# AttoSQL

This is an ongoing effort to implement a tiny relational database in Rust.

## Getting Started

To build,

```
$ cargo build
```

and test,

```
$ cargo test
```