extern crate attosql_commons;

pub mod accessor;
pub mod btree;
pub mod database;
pub mod page;
pub mod search;
pub mod schema;
pub mod sort;
pub mod trans;

use std::cell::RefCell;

use bytes::Bytes;
use tempfile::NamedTempFile;
use tonic::{transport::Server, Request, Response, Status, Code};

use attosql_commons::attosql::database_server::{Database, DatabaseServer};
use attosql_commons::attosql::{
  self, query, FullScan, RangeScan, CreateTableRequest, CreateTableResponse, InsertRequest, InsertResponse, RunRequest, RunResponse,
  BeginRequest, BeginResponse, CommitRequest, CommitResponse, RollbackRequest, RollbackResponse,
};
use crate::accessor::FileAccessor;
use crate::btree::BTreeRoot;
use crate::schema::{Query, Table, TableId, Value};

pub struct DatabaseImpl {
  db: RefCell<database::Database<FileAccessor>>,
}

unsafe impl Send for DatabaseImpl {}
unsafe impl Sync for DatabaseImpl {}

impl DatabaseImpl {
  fn new(db: database::Database<FileAccessor>) -> DatabaseImpl {
    DatabaseImpl { db: RefCell::new(db) }
  }

  fn convert_query(&self, query: Option<attosql::Query>) -> Result<Query, Status> {
    match query {
      Some(attosql::Query { query: Some(query::Query::FullScan(FullScan { table_id } )) } ) => {
        Ok(Query::FullScan { table_id: table_id.into() } )
      },
      Some(attosql::Query { query: Some(query::Query::RangeScan(RangeScan { beg, end, table_id } )) } ) => {
        let table_id = table_id.into();
        let ty = self.db.borrow().fetch_primary_column_type(table_id)?;
        let beg = Value::from_in_value(beg.unwrap(), ty);
        let end = Value::from_in_value(end.unwrap(), ty);
        Ok(Query::RangeScan { table_id, beg, end })
      },
      _ => Err(Status::new(Code::Unimplemented, "unimplemented")),
    }
  }
}

#[tonic::async_trait]
impl Database for DatabaseImpl {
  async fn run(&self, request: Request<RunRequest>) -> Result<Response<RunResponse>, Status> {
    let db = self.db.borrow();
    let pipe = db.plan(self.convert_query(request.into_inner().query)?, None)?;
    let res = RunResponse { rows: pipe.into() };
    Ok(Response::new(res))
  }

  async fn insert(&self, request: Request<InsertRequest>) -> Result<Response<InsertResponse>, Status> {
    let request = request.into_inner();
    let table_id: TableId = request.table_id.into();
    let schema = self.db.borrow().fetch_schema(table_id)?;
    let key: Value = Value::from_in_value(request.values[0].clone(), schema.types[0]);
    let values: Vec<Value> = request.values[1..].iter()
      .zip(schema.types[1..].iter())
      .map(|(v, ty)| Value::from_in_value(v.clone(), ty.clone()))
      .collect();
    let key: Bytes = schema.get_key_bytes(key);
    let values: Bytes = schema.get_value_bytes(values);
    self.db.borrow_mut().insert(table_id, &key, &values)?;
    Ok(Response::new(InsertResponse{}))
  }

  async fn create_table(&self, request: Request<CreateTableRequest>) -> Result<Response<CreateTableResponse>, Status> {
    let table = Table::new(request.into_inner().columns.into_iter().map(|col| col.into()).collect());
    let table_id = self.db.borrow_mut().create_table(&table)?;
    let res = CreateTableResponse { table_id: table_id.into() };
    Ok(Response::new(res))
  }

  async fn begin(&self, request: Request<BeginRequest>) -> Result<Response<BeginResponse>, Status> {
    let trans_id = self.db.borrow_mut().begin()?;
    Ok(Response::new(BeginResponse{ trans_id: trans_id.into() }))
  }

  async fn commit(&self, request: Request<CommitRequest>) -> Result<Response<CommitResponse>, Status> {
    self.db.borrow_mut().commit(&request.into_inner().trans_id.into())?;
    Ok(Response::new(CommitResponse{}))
  }

  async fn rollback(&self, request: Request<RollbackRequest>) -> Result<Response<RollbackResponse>, Status> {
    self.db.borrow_mut().rollback(&request.into_inner().trans_id.into());
    Ok(Response::new(RollbackResponse{}))
  }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
  println!("Start main");
  let addr = "[::1]:5000".parse()?;

  let (_, path) = NamedTempFile::new()?.into_parts();
  let vendor = FileAccessor::new(path)?;
  let root = BTreeRoot::new(vendor)?;
  let db = database::Database::new(root)?;
  let server = DatabaseImpl::new(db);

  Server::builder()
    .add_service(DatabaseServer::new(server))
    .serve(addr)
    .await?;

  Ok(())
}