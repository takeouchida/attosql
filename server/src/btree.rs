use std::borrow::{Borrow, BorrowMut};
use std::cmp::Ordering;
use std::collections::BTreeMap;
use std::convert::TryInto;
use std::mem::size_of;
use std::io::Result;
use std::vec::IntoIter;

use bytes::Bytes;

use attosql_commons::iter::MultiHashMap;
use crate::page::*;
use crate::schema::{BinType, ColumnId, IndexId, Table, TableColumnId, TableId, Type, Value};

fn lt_typed<K, const N: usize>(a: &[u8], b: &[u8]) -> bool where K: From<[u8; N]> + Clone + Ord {
  let a: [u8; N] = a.try_into().unwrap();
  let b: [u8; N] = b.try_into().unwrap();
  Into::<K>::into(a).cmp(&Into::<K>::into(b.clone())) == Ordering::Less
}

fn get<'p, P>(vendor: &'p P, key: &[u8], id: PageId, lt: &dyn Fn(&[u8], &[u8]) -> bool) -> Result<Option<Bytes>>
  where
    P: PageVendor,
{
  if vendor.is_branch(id)? {
    let node: Branch = vendor.fetch(id)?;
    let pos = node.find_upper_bound(key, lt);
    let id = node.value(pos).clone();
    get(vendor, key, id.into(), lt)
  } else if vendor.is_leaf(id)? {
    let node: Leaf = vendor.fetch(id)?;
    let pos = node.find_lower_bound(key, lt);
    if pos < node.header().len() && node.key(pos) == key {
      Ok(Some(node.value(pos)))
    } else {
      Ok(None)
    }
  } else {
    panic!("get: the page is neither lead or branch: {:?}", id)
  }
}

fn get_typed<'p, P, K, V, const N: usize, const M: usize>(
  vendor: &'p P,
  id: PageId,
  key: &K,
) -> Result<Option<V>>
  where
    P: PageVendor,
    [u8; N]: From<K>,
    [u8; M]: From<V>,
    K: From<[u8; N]> + Clone + Ord,
    V: From<[u8; M]>,
{
  let result = get(vendor, &Into::<[u8; N]>::into(key.clone()), id, &lt_typed::<K, N>)?;
  Ok(result.map(|x| TryInto::<[u8; M]>::try_into(&x[..]).unwrap().into()))
}

fn get_iter<'p, P>(
  vendor: &'p P, key: Bytes, id: PageId, lt: &dyn Fn(&[u8], &[u8]) -> bool
) -> Result<IntoIter<Vec<Bytes>>>
  where
    P: PageVendor,
{
  let result = get(vendor, &key[..], id, lt)?;
  let iter = match result {
    None => Vec::new().into_iter(),
    Some(x) => vec![vec![key, x]].into_iter(),
  };
  Ok(iter)
}

fn get_iter_typed<'a, 'p, P, K, V, const N: usize, const M: usize>(
  vendor: &'p P,
  id: PageId,
  key: &K,
) -> Result<IntoIter<Vec<Bytes>>>
  where
    P: PageVendor,
    [u8; N]: From<K>,
    [u8; M]: From<V>,
    K: From<[u8; N]> + Clone,
    V: From<[u8; M]>,
    K: Ord,
{
  let result = get(vendor, &Into::<[u8; N]>::into(key.clone()), id, &lt_typed::<K, N>)?;
  let iter = match result {
    None => Vec::new().into_iter(),
    Some(x) => vec![vec![Bytes::copy_from_slice(&Into::<[u8; N]>::into(key.clone())), x]].into_iter(),
  };
  Ok(iter)
}

fn first_page_id<'p, P>(vendor: &'p P, id: PageId) -> Result<PageId> where P: PageVendor,
{
  if vendor.is_branch(id)? {
    let node: Branch = vendor.fetch(id)?;
    first_page_id(vendor, node.value(0).into())
  } else if vendor.is_leaf(id)? {
    Ok(id)
  } else {
    panic!("first_page_id: the page is neither lead or branch: {:?}", id)
  }
}

fn batch_get<P>(
  vendor: &mut P,
  keys: Vec<Bytes>,
  id: PageId,
  lt: &dyn Fn(&[u8], &[u8]) -> bool
) -> Result<Vec<(Bytes, Bytes)>> where P: PageVendor {
  if vendor.is_branch(id)? {
    let node: Branch = vendor.fetch(id)?;
    let mut result = Vec::<(Bytes, Bytes)>::new();
    let pos_key_pairs: Vec<_> = keys.iter().map(|k| node.find_lower_bound(k, lt)).zip(keys.iter()).collect();
    let map: MultiHashMap<_, _> = pos_key_pairs.into_iter().collect();
    for (k, vs) in map {
      let id = node.value(k).clone();
      let keys: Vec<Bytes> = vs.into_iter().map(|v| v.clone()).collect();
      let key_value_pairs = batch_get(vendor, keys, id.into(), lt)?;
      for (k, v) in key_value_pairs {
        result.push((k, v));
      }
    }
    Ok(result)
  } else if vendor.is_leaf(id)? {
    let node: Leaf = vendor.fetch(id)?;
    let mut result = Vec::<(Bytes, Bytes)>::new();
    for k in keys {
      let pos = node.find_lower_bound(&k, lt);
      if pos < node.header().len() && node.key(pos) == k {
        result.push((k, node.value(pos)));
      }
    }
    Ok(result)
  } else {
    panic!("batch_get: the page is neither lead or branch: {:?}", id)
  }
}

fn insert<'p, P>(
  mut vendor: &'p mut P,
  mut root: PageId,
  key: &[u8],
  value: &[u8],
  lt: &dyn Fn(&[u8], &[u8]) -> bool,
) -> Result<PageId>
  where
    P: PageVendor
{
  let is_full = vendor.is_full(root)?;
  if is_full {
    let mut child: BranchMut = vendor.copy(root)?;
    let key_size = child.header_mut().key_size();
    let mut parent: BranchMut = vendor.new_page(key_size, size_of::<PageId>());
    parent.push_typed(root.into());
    root = vendor.save(parent)?;
    root = split_child(vendor, root, 0)?;
    // root = id;
    // vendor = v;
  }
  insert_nonfull(vendor, root, key, value, lt)
}

fn insert_typed<'p, P, K, V, const N: usize, const M: usize>(
  vendor: &'p mut P,
  root: PageId,
  key: &K,
  value: &V,
) -> Result<PageId>
  where
    P: PageVendor,
    [u8; N]: From<K>,
    [u8; M]: From<V>,
    K: From<[u8; N]> + Clone + Ord,
    V: Clone,
{
  insert(
    vendor,
    root,
    &Into::<[u8; N]>::into(key.clone()),
    &Into::<[u8; M]>::into(value.clone()),
    &|a: &[u8], b: &[u8]| {
      let a: [u8; N] = a.try_into().unwrap();
      let b: [u8; N] = b.try_into().unwrap();
      Into::<K>::into(a).cmp(&Into::<K>::into(b.clone())) == Ordering::Less
    })
}

fn insert_nonfull<'p, P>(
  mut vendor: &'p mut P,
  mut parent_id: PageId,
  key: &[u8],
  value: &[u8],
  lt: &dyn Fn(&[u8], &[u8]) -> bool,
) -> Result<PageId>
  where
    P: PageVendor
{
  if vendor.is_branch(parent_id)? {
    let mut branch: Branch = vendor.fetch(parent_id)?;
    let pos = branch.find_upper_bound(&key, lt);
    if vendor.is_full(branch.value(pos).into())? {
      parent_id = split_child(vendor, parent_id, pos)?;
      // parent_id = id;
      // vendor = v;
      branch = vendor.fetch(parent_id)?;
    }
    let pos = branch.find_upper_bound(&key, lt);
    let child_id = branch.value(pos).into();
    let new_child_id = insert_nonfull(vendor, child_id, key, value, lt)?;
    // vendor = v;
    let mut branch: BranchMut = vendor.copy(parent_id.into())?;
    branch.set_value_typed(pos, new_child_id);
    vendor.save(branch)
    // Ok((id, vendor))
  } else if vendor.is_leaf(parent_id)? {
    let leaf: Leaf = vendor.fetch(parent_id)?;
    debug_assert!(!leaf.header().is_full());
    let pos = leaf.find_upper_bound(&key, lt);
    let mut leaf: LeafMut = vendor.copy(parent_id)?;
    leaf.insert(&key, &value, pos);
    vendor.save(leaf)
  } else {
    panic!("BTree::insert_nonnull: the page is neither lead or branch: {:?}", parent_id);
  }
}

fn split_child<'p, P>(mut vendor: &'p mut P, parent_id: PageId, pos: usize) -> Result<PageId> where P: PageVendor {
  let parent: Branch = vendor.fetch(parent_id)?;
  let child_id1 = parent.value(pos).into();
  if vendor.is_branch(child_id1)? {
    let mut child1: BranchMut = vendor.copy(child_id1)?;
    let mut child2: BranchMut = vendor.new_page(child1.header_mut().key_size(), size_of::<PageId>());
    let pivot = child1.split(&mut child2);
    let child_id2 = vendor.save(child2)?;
    let child_id1 = vendor.save(child1)?;
    let child_id2: [u8; 6] = child_id2.into();
    let mut parent: BranchMut = vendor.copy(parent_id)?;
    parent.set_value_typed(pos, child_id1);
    parent.insert(pos, &pivot, &child_id2);
    vendor.save(parent)
    // Ok((id, vendor))
  } else if vendor.is_leaf(child_id1)? {
    let mut child1: LeafMut = vendor.copy(child_id1)?;
    let mut child2: LeafMut = vendor.new_page(child1.header_mut().key_size(), child1.header_mut().value_size());
    let pivot = child1.split(&mut child2);
    let child_id2 = vendor.save(child2)?;
    let child_id1 = vendor.save(child1)?;
    let child_id2: [u8; 6] = child_id2.into();
    let mut parent: BranchMut = vendor.copy(parent_id)?;
    parent.set_value_typed(pos, child_id1);
    parent.insert(pos, &pivot, &child_id2);
    vendor.save(parent)
    // Ok((id, vendor))
  } else {
    panic!("BTreeMut::split_child: The page is neither branch nor leaf: {:?}", child_id1)
  }
}

struct BatchInsert<'i, 'l, 'p, P> {
  vendor: &'p mut P,
  root: PageId,
  iter: &'i mut dyn Iterator<Item=(Bytes, Bytes)>,
  lt: &'l dyn Fn(&[u8], &[u8]) -> bool,
  next: Option<(Bytes, Bytes)>,
}

#[derive(Eq, PartialEq)]
enum LastAction { Unknown, Up, Down, Stay }

impl<'i, 'l, 'p, P> BatchInsert<'i, 'l, 'p, P> where P: PageVendor {
  fn new(
    vendor: &'p mut P,
    root: PageId,
    iter: &'i mut dyn Iterator<Item=(Bytes, Bytes)>,
    lt: &'l dyn Fn(&[u8], &[u8]) -> bool,
  ) -> BatchInsert<'i, 'l, 'p, P> {
    BatchInsert { vendor, root, iter, lt, next: None }
  }

  fn split_child(&mut self, parent: &mut BranchMut, pos: usize) -> Result<()> {
    let child_id1 = parent.value_typed(pos);
    let (pivot, child_id1, child_id2) = if self.vendor.is_branch(child_id1)? {
      let mut child1: BranchMut = self.vendor.copy(child_id1)?;
      let mut child2: BranchMut = self.vendor.new_page(child1.header().key_size(), size_of::<PageId>());
      let pivot = child1.split(&mut child2);
      (pivot, self.vendor.save(child1)?,  self.vendor.save(child2)?)
    } else if self.vendor.is_leaf(child_id1)? {
      let mut child1: LeafMut = self.vendor.copy(child_id1)?;
      let mut child2: LeafMut = self.vendor.new_page(child1.header().key_size(), child1.header().value_size());
      let pivot = child1.split(&mut child2);
      (pivot, self.vendor.save(child1)?,  self.vendor.save(child2)?)
    } else {
      panic!("BTreeMut::split_child: The page is neither branch nor leaf: {:?}", child_id1)
    };
    let child_id2: [u8; 6] = child_id2.into();
    parent.set_value_typed(pos, child_id1);
    parent.insert(pos, &pivot, &child_id2);
    Ok(())
  }

  fn nonfull(&mut self, mut id: PageId) -> Result<PageId> {
    let mut stack = Vec::<(PageId, usize, BranchMut, Option<Bytes>)>::new();
    let mut leaf: Option<LeafMut> = None;
    let mut action: LastAction = LastAction::Unknown;

    loop {
      if self.next.is_none() {
        self.next = self.iter.next()
      }
      if self.next.is_none() {
        // The items in the iterator are exhausted.
        if leaf.is_some() {
          if leaf.as_mut().unwrap().is_updated() {
            id = self.vendor.save(leaf.unwrap())?;
          }
        }
        // Saves all the nodes left and exit the loop.
        while let Some((_, pos, mut node, _)) = stack.pop() {
          node.set_value_typed(pos, id.clone());
          id = self.vendor.save(node)?;
        }
        return Ok(id)
      }
      if stack.is_empty() && action == LastAction::Up {
        break
      }
      let (key, value) = self.next.as_ref().unwrap().clone();
      if self.vendor.is_branch(id)? {
        // Determines if it should leave the node and go up, or stay here and go down.
        let leave = if let Some((_, _, _, Some(right))) = stack.last() {
          !(*self.lt)(&key, right)
        } else {
          false
        };
        if leave {
          // If go up, pop item from stack, and save the node.
          let (_, pos, mut node, _) = stack.pop().unwrap();
          node.set_value_typed(pos, id);
          id = self.vendor.save(node)?;
          action = LastAction::Up;
          continue
        }
        let mut node: BranchMut = self.vendor.copy(id)?;
        let pos = node.find_upper_bound(&key, self.lt);
        let child_id = node.value_typed(pos);
        // If full, splits the child node.
        if self.vendor.is_full(child_id)? {
          self.split_child(&mut node, pos)?;
        }
        // If goes down, the item is pushed into stack.
        let pos = node.find_upper_bound(&key, self.lt);
        let right = if pos + 1 < node.header().len() {
          Some(Bytes::copy_from_slice(node.raw_key(pos + 1)))
        } else {
          None
        };
        id = node.value_typed(pos);
        stack.push((id, pos, node, right));
        action = LastAction::Down;
      } else if self.vendor.is_leaf(id)? {
        if leaf.is_none() {
          leaf = Some(self.vendor.copy(id)?);
        }
        // Determines if it should leave the node and go up, or stay here and go down.
        if let Some((_, pos, branch, Some(right))) = stack.last_mut() {
          if !(*self.lt)(&key, right) {
            // If go up, pop item from stack, and save the node.
            id = self.vendor.save(leaf.unwrap())?;
            leaf = None;
            branch.set_value_typed(*pos, id);
            stack.pop();
            action = LastAction::Up;
            continue
          }
        }
        // If full, splits the child node.
        let l = leaf.as_mut().unwrap();
        if l.header().is_full() {
          let mut branch: BranchMut = self.vendor.new_page(l.header().key_size(), l.header().value_size());
          id = self.vendor.save(leaf.unwrap())?;
          leaf = None;
          branch.push_typed(id);
          self.split_child(&mut branch, 0)?;
          id = self.vendor.save(branch)?;
          action = LastAction::Stay;
          continue
        }
        // Inserts an entry and take the next entry.
        let pos = l.find_upper_bound(&key, self.lt);
        l.insert(&key, &value, pos);
        self.next = None;
        action = LastAction::Stay;
      } else {
        panic!("BTreeMut::nonfull: The page is neither branch nor leaf: {:?}", id)
      }
    }
    Ok(id)
  }

  fn run(&mut self) -> Result<PageId> {
    if self.vendor.is_full(self.root)? {
      let child: BranchMut = self.vendor.copy(self.root)?;
      let mut parent: BranchMut = self.vendor.new_page(child.header().key_size(), size_of::<PageId>());
      parent.push_typed(self.root.clone());
      self.split_child(&mut parent, 0)?;
      self.root = self.vendor.save(parent)?;
    }
    self.next = self.iter.next();
    match self.next {
      None    => Ok(self.root),
      Some(_) => self.nonfull(self.root),
    }
  }
}

fn batch_insert<'i, 'l, 'p, P>(
  vendor: &'p mut P,
  root: PageId,
  iter: &'i mut dyn Iterator<Item=(Bytes, Bytes)>, // The keys must be sorted in ascending order.
  lt: &'l dyn Fn(&[u8], &[u8]) -> bool,
) -> Result<PageId>
  where
    P: PageVendor,
{
  BatchInsert::new(vendor, root, iter, lt).run()
}

fn update<'p, P>(
  mut vendor: &'p mut P,
  key: &[u8],
  value: &[u8],
  id: PageId,
  lt: &dyn Fn(&[u8], &[u8]) -> bool
) -> Result<Option<PageId>>
  where
    P: PageVendor
{
  if vendor.is_branch(id)? {
    let branch: Branch = vendor.fetch(id)?;
    let pos = branch.find_upper_bound(&key, lt);
    let child_id = branch.value(pos).into();
    update(vendor, key, value, child_id, lt)
  } else if vendor.is_leaf(id)? {
    let leaf: Leaf = vendor.fetch(id)?;
    let pos = leaf.find_lower_bound(&key, lt);
    let key_exists = pos < leaf.header().len() && key == leaf.key(pos);
    if key_exists {
      let mut leaf: LeafMut = vendor.copy(id)?;
      leaf.set_value(pos, &value);
      let result = vendor.save(leaf)?;
      Ok(Some(result))
    } else {
      Ok(None)
    }
  } else {
    panic!("BTree::update_internal: the page is neither lead or branch: {:?}", id)
  }
}

fn update_typed<'p, P, K, V, const N: usize, const M: usize>(
  vendor: &'p mut P,
  key: &K,
  value: &V,
  id: PageId,
) -> Result<Option<PageId>>
  where
    P: PageVendor,
    [u8; N]: From<K>,
    [u8; M]: From<V>,
    K: From<[u8; N]> + Clone + Ord,
    V: Clone,
{
  update(
    vendor,
    &Into::<[u8; N]>::into(key.clone()),
    &Into::<[u8; M]>::into(value.clone()),
    id,
    &|a: &[u8], b: &[u8]| {
      let a: [u8; N] = a.try_into().unwrap();
      let b: [u8; N] = b.try_into().unwrap();
      Into::<K>::into(a).cmp(&Into::<K>::into(b.clone())) == Ordering::Less
    })
}

fn batch_update<P>(
  mut vendor: &mut P,
  params: Vec<(Bytes, Bytes)>, // Should be sorted by key in ascending order.
  root: PageId,
  lt: &dyn Fn(&[u8], &[u8]) -> bool
) -> Result<Option<PageId>>
  where
    P: PageVendor
{
  let mut id = root;
  let mut pages: BTreeMap<PageId, Vec<(usize, PageId)>> = BTreeMap::new();
  let mut stack: Vec<(PageId, usize)> = Vec::new();
  let mut i: usize = 0;
  let mut leaf: Option<LeafMut> = None;
  while i < params.len() {
    let (key, value) = params.get(i).unwrap();
    if vendor.is_branch(id)? {
      let node: Branch = vendor.fetch(id)?;
      let pos = node.find_upper_bound(&key, lt);
      if pos == node.len() && !stack.is_empty() && stack.last().unwrap().0 == id {
        // This node was already visited and the pos is greater than the right-most key, which means the upper layer needs to be seen.
        stack.pop();
        if pages.contains_key(&id) {
          // Updates PageIds that need to be updated here.
          let mut node: BranchMut = vendor.copy(id)?;
          for (pos, id) in pages.get(&id).unwrap() {
            node.set_value_typed(*pos, id.clone());
          }
          id = vendor.save(node)?;
          pages.remove(&id);
          // Remembers the id to be updated in the upper layer.
          let (parent_id, parent_pos) = stack.last().unwrap();
          if pages.contains_key(parent_id) {
            pages.get_mut(parent_id).unwrap().push((*parent_pos, id));
          } else {
            pages.insert(*parent_id, vec![(*parent_pos, id)]);
          }
        }
      } else {
        // This node was not yet visited or the pos is less than or equal to the right-most key, which means the lower layer needs to be seen.
        if !stack.is_empty() && stack.last().unwrap().0 == id {
          stack.last_mut().unwrap().1 = pos;
        } else {
          stack.push((id, pos));
        }
        id = node.value(pos).into();
      }
    } else if vendor.is_leaf(id)? {
      let node: Leaf = vendor.fetch(id)?;
      let pos = node.find_lower_bound(&key, lt);
      if pos < node.len() && node.key(pos) == key {
        // Updates the value of the key.
        if leaf.is_none() {
          leaf = Some(vendor.copy(id)?);
        }
        leaf.as_mut().unwrap().set_value(pos, &value);
      } else if pos == node.len() {
        // Saves the leaf if there is an update.
        if leaf.is_some() {
          id = vendor.save(leaf.unwrap())?;
        } else if !stack.is_empty() {
          id = stack.last().unwrap().0;
        }
        leaf = None;
        stack.pop();
      }
      i += 1;
    } else {
      panic!("BTree::batch_update: the page is neither lead or branch: {:?}", id)
    }
  }
  if id == root {
    Ok(Some(id))
  } else {
    Ok(None)
  }
}

fn search_for_barch_update<P>(
  vendor: &mut P,
  key: Bytes,
  id: PageId,
  stack: &mut Vec<(usize, PageId)>,
  lt: &dyn Fn(&[u8], &[u8]) -> bool
) -> Result<Option<(usize, PageId)>>
  where
    P: PageVendor
{
  if vendor.is_branch(id)? {
    let node: Branch = vendor.fetch(id)?;
    let pos = node.find_lower_bound(&key, lt);
    let id = node.value(pos).into();
    search_for_barch_update(vendor, key, id, stack, lt)
  } else if vendor.is_branch(id)? {
    let node: Leaf = vendor.fetch(id)?;
    let pos = node.find_lower_bound(&key, lt);
    if pos < node.header().len() && node.key(pos) == key {
      Ok(Some((pos, id)))
    } else {
      Ok(None)
    }
  } else {
    panic!("BTree::search_for_batch_update: the page is neither lead or branch: {:?}", id)
  }
}

pub struct RangeScan<'f, 'p, P>
  where
    P: PageVendor
{
  vendor: &'p P,
  path: Vec<(PageId, usize)>,
  leaf: Option<Leaf>,
  to: Bytes,
  lt: &'f dyn Fn(&[u8], &[u8]) -> bool,
}

pub fn range_scan<'f, 'p, P>(
  vendor: &'p P,
  root: PageId,
  from: &Bytes,
  to: &Bytes,
  lt: &'f dyn Fn(&[u8], &[u8]) -> bool,
) -> Result<RangeScan<'f, 'p, P>>
  where
    P: PageVendor,
{
  range_scan_internal(vendor, Vec::new(), root, from, to, lt)
}

fn range_scan_internal<'f, 'p, P>(
  vendor: &'p P,
  mut path: Vec<(PageId, usize)>,
  id: PageId,
  from: &Bytes,
  to: &Bytes,
  lt: &'f dyn Fn(&[u8], &[u8]) -> bool,
) -> Result<RangeScan<'f, 'p, P>>
  where
    P: PageVendor,
{
  if vendor.is_branch(id)? {
    let node: Branch = vendor.fetch(id)?;
    let pos = node.find_upper_bound(&from, lt);
    path.push((id, pos));
    range_scan_internal(vendor, path, node.value(pos).into(), from, to, lt)
  } else if vendor.is_leaf(id)? {
    let node: Leaf = vendor.fetch(id)?;
    let pos = node.find_lower_bound(&from, lt);
    path.push((id, pos));
    Ok(RangeScan { vendor, path, leaf: None, to: to.clone(), lt })
  } else {
    panic!("range_scan_internal: the page is neither lead or branch: {:?}", id)
  }
}

impl<'f, 'p, P> Iterator for RangeScan<'f, 'p, P> where P: PageVendor, {
  type Item = (Bytes, Bytes);

  #[inline]
  fn next(&mut self) -> Option<Self::Item> {
    match self.run() {
      Ok(result) => result,
      // TODO Report the error
      Err(_) => None,
    }
  }
}

impl<'f, 'p, P> RangeScan<'f, 'p, P> where P: PageVendor {

  #[inline]
  fn run(&mut self) -> Result<Option<(Bytes, Bytes)>> {
    loop {
      if self.path.is_empty() {
        return Ok(None)
      }
      let (id, pos) = self.path.last().unwrap().clone();
      let id = id.clone();
      if self.vendor.is_branch(id)? {
        if pos <= self.vendor.fetch_len(id)? {
          let node: Branch = self.vendor.fetch(id)?;
          let id = node.value(pos).into();
          self.path.push((id, 0));
        } else {
          self.path.pop();
          if let Some((_, pos)) = self.path.last_mut() {
            *pos += 1;
          }
        }
      } else if self.vendor.is_leaf(id)? {
        let node: Leaf = self.vendor.fetch(id)?;
        if pos < node.header().len() {
          let key = node.key(pos);
          return if (*self.lt)(&key, &self.to) {
            match self.path.last_mut() {
              None => Ok(None),
              Some((_, p)) => {
                *p += 1;
                Ok(Some((node.key(pos), node.value(pos))))
              },
            }
          } else {
            Ok(None)
          }
        } else {
          self.path.pop();
          if let Some((_, pos)) = self.path.last_mut() {
            *pos += 1;
          }
        }
      }
    }
  }
}

pub struct FullScan<'p, P> where P: PageVendor {
  vendor: &'p P,
  path: Vec<(PageId, usize)>,
}

pub fn full_scan<'p, P>(vendor: &'p P, root: PageId) -> Result<FullScan<P>> where P: PageVendor {
  full_scan_internal(vendor, Vec::new(), root)
}

fn full_scan_internal<'p, 'q, P>(vendor: &'p P, mut path: Vec<(PageId, usize)>, id: PageId) -> Result<FullScan<'p, P>>
  where
    'q: 'p,
    P: PageVendor,
{
  if vendor.is_branch(id)? {
    path.push((id, 0));
    let node: Branch = vendor.fetch(id)?;
    let id = node.value(0).into();
    full_scan_internal(vendor, path, id)
  } else if vendor.is_leaf(id)? {
    path.push((id, 0));
    Ok(FullScan { vendor, path })
  } else {
    panic!("full_scan_internal: the page is neither lead or branch: {:?}", id)
  }
}

impl<'p, P> Iterator for FullScan<'p, P> where P: PageVendor, {
  type Item = (Bytes, Bytes);

  #[inline]
  fn next(&mut self) -> Option<Self::Item> {
    match self.run() {
      Ok(result) => result,
      // TODO Propagate the error
      Err(err) => {
        panic!("{}", err);
      },
    }
  }
}

impl<'p, P> FullScan<'p, P> where P: PageVendor {
  #[inline]
  fn run(&mut self) -> Result<Option<(Bytes, Bytes)>> {
    loop {
      if self.path.is_empty() {
        return Ok(None)
      }
      let (id, pos) = self.path.last().unwrap().clone();
      if self.vendor.is_branch(id)? {
        let node: Branch = self.vendor.fetch(id)?;
        if pos <= node.header().len() {
          let id = node.value(pos).into();
          self.path.push((id, 0));
        } else {
          self.path.pop();
          if let Some((_, p)) = self.path.last_mut() {
            *p += 1
          } else {
            return Ok(None)
          }
        }
      } else if self.vendor.is_leaf(id)? {
        let node: Leaf = self.vendor.fetch(id)?;
        if pos < node.header().len() {
          if let Some((_, pos)) = self.path.last_mut() {
            *pos += 1
          }
          return Ok(Some((node.key(pos), node.value(pos))))
        } else {
          self.path.pop();
          if let Some((_, pos)) = self.path.last_mut() {
            *pos += 1;
          } else {
            return Ok(None)
          }
        }
      }
    }
  }
}

pub struct SubMutator<'r, P: PageVendor> {
  page_id: PageId,
  root: &'r mut BTreeRoot<P>,
}

impl<'m, 'r, P: PageVendor> SubMutator<'r, P> {
  pub fn fetch_primary_column_type(&self, table_id: TableId) -> Result<Type> {
    let table_column_id = TableColumnId::new(table_id, ColumnId::zero());
    Ok(get_typed(&self.root.vendor, self.root.columns_root()?, &table_column_id)?.unwrap())
  }

  pub fn get_typed<K, V, const N: usize, const M: usize>(&self, key: &K) -> Result<Option<V>>
    where
      [u8; N]: From<K>,
      [u8; M]: From<V>,
      K: From<[u8; N]> + Clone + Ord,
      V: From<[u8; M]>,
  {
    get_typed(&self.root.vendor, self.page_id, key)
  }

  pub fn batch_insert<'i, 'l>(
    &mut self,
    iter: &'i mut dyn Iterator<Item=(Bytes, Bytes)>, // The keys must be sorted in ascending order.
    lt: &'l dyn Fn(&[u8], &[u8]) -> bool,
  ) -> Result<PageId> {
    BatchInsert::new(&mut self.root.vendor, self.page_id, iter, lt).run()
  }

  pub fn update_typed<'p, K, V, const N: usize, const M: usize>(
    &mut self,
    key: &K,
    value: &V,
  ) -> Result<()>
    where
      [u8; N]: From<K>,
      [u8; M]: From<V>,
      K: From<[u8; N]> + Clone + Ord,
      V: Clone,
  {
    let o = update_typed(&mut self.root.vendor, key, value, self.page_id)?;
    if let Some(id) = o {
      self.page_id = id
    }
    Ok(())
  }

  pub fn insert(
    &mut self,
    mut root: PageId,
    key: &[u8],
    value: &[u8],
    lt: &dyn Fn(&[u8], &[u8]) -> bool,
  ) -> Result<PageId> {
    insert(&mut self.root.vendor, root, key, value, lt)
  }

  pub fn update(
    &mut self,
    key: &[u8],
    value: &[u8],
    id: PageId,
    lt: &dyn Fn(&[u8], &[u8]) -> bool
  ) -> Result<Option<PageId>> {
    update(&mut self.root.vendor, key, value, id, lt)
  }

  pub fn insert_typed<K, V, const N: usize, const M: usize>(
    &mut self,
    key: &K,
    value: &V,
  ) -> Result<PageId>
    where
      P: PageVendor,
      [u8; N]: From<K>,
      [u8; M]: From<V>,
      K: From<[u8; N]> + Clone + Ord,
      V: Clone,
  {
    insert(
      &mut self.root.vendor,
      self.page_id,
      &Into::<[u8; N]>::into(key.clone()),
      &Into::<[u8; M]>::into(value.clone()),
      &|a: &[u8], b: &[u8]| {
        let a: [u8; N] = a.try_into().unwrap();
        let b: [u8; N] = b.try_into().unwrap();
        Into::<K>::into(a).cmp(&Into::<K>::into(b.clone())) == Ordering::Less
      })
  }
}

pub struct Mutator<'m, 'r, P: PageVendor> {
  root: &'r mut BTreeRoot<P>,
  meta: &'m mut MetadataMut,
}

impl<'m, 'r, P> Mutator<'m, 'r, P>
  where
    P: PageVendor,
{
  pub fn root(&'r mut self) -> &'r mut BTreeRoot<P> {
    self.root
  }

  pub fn meta(&mut self) -> &mut MetadataMut {
    &mut self.meta
  }

  pub fn next_table_id(&mut self) -> TableId {
    let body: &mut MetadataBody = self.meta.borrow_mut();
    body.next_table_id().into()
  }

  pub fn next_index_id(&mut self) -> IndexId {
    let body: &mut MetadataBody = self.meta.borrow_mut();
    body.next_index_id().into()
  }

  pub fn data<'s, 't, F>(&'s mut self, mut action: F) -> Result<()>
    where
      F: FnMut(&mut SubMutator<'t, P>) -> Result<()>,
      'r: 's,
      's: 't,
  {
    let meta: &mut MetadataBody = self.meta.borrow_mut();
    let mut sub = SubMutator { page_id: meta.data_root(), root: self.root };
    action(&mut sub)?;
    meta.set_data_root(sub.page_id);
    Ok(())
  }

  pub fn index<'s, 't, F>(&'s mut self, mut action: F) -> Result<()>
    where
      F: FnMut(&mut SubMutator<'t, P>) -> Result<()>,
      'r: 's,
      's: 't,
  {
    let meta: &mut MetadataBody = self.meta.borrow_mut();
    let mut sub = SubMutator { page_id: meta.indices_root(), root: self.root };
    action(&mut sub)?;
    meta.set_indices_root(sub.page_id);
    Ok(())
  }

  pub fn column<'s, 't, F>(&'s mut self, mut action: F) -> Result<()>
    where
      F: FnMut(&mut SubMutator<'t, P>) -> Result<PageId>,
      'r: 's,
      's: 't,
  {
    let meta: &mut MetadataBody = self.meta.borrow_mut();
    let mut sub = SubMutator { page_id: meta.columns_root(), root: self.root };
    action(&mut sub)?;
    meta.set_columns_root(sub.page_id);
    Ok(())
  }

  pub fn column_index<'s, 't, F>(&'s mut self, mut action: F) -> Result<()>
    where
      F: FnMut(&mut SubMutator<'t, P>) -> Result<()>,
      'r: 's,
      's: 't,
  {
    let meta: &mut MetadataBody = self.meta.borrow_mut();
    let mut sub = SubMutator { page_id: meta.index_columns_root(), root: self.root };
    action(&mut sub)?;
    meta.set_index_columns_root(sub.page_id);
    Ok(())
  }

  pub fn create_table(&mut self, key_size: usize, value_size: usize) -> Result<TableId> {
    let table_id = self.next_table_id();
    let node: LeafMut = self.root.vendor.borrow_mut().new_page(key_size, value_size);
    let page_id = self.root.vendor.borrow_mut().save(node)?;
    let meta: &mut MetadataBody = self.meta.borrow_mut();
    let new_data_root = insert_typed(self.root.vendor.borrow_mut(), meta.data_root(), &table_id, &page_id)?.into();
    meta.set_data_root(new_data_root);
    Ok(table_id)
  }

  pub fn create_index(&mut self, key_size: usize, value_size: usize) -> Result<IndexId> {
    let index_id = self.next_index_id();
    let node: LeafMut = self.root.vendor.borrow_mut().new_page(key_size, value_size);
    let index_page_id = self.root.vendor.borrow_mut().save(node)?;
    let meta: &mut MetadataBody = self.meta.borrow_mut();
    let new_index_data_id = insert_typed(self.root.vendor.borrow_mut(), meta.indices_root(), &index_id, &index_page_id)?.into();
    meta.set_indices_root(new_index_data_id);
    Ok(index_id)
  }

  pub fn batch_insert<'i, 'l>(
    &mut self,
    root: PageId,
    iter: &'i mut dyn Iterator<Item=(Bytes, Bytes)>, // The keys must be sorted in ascending order.
    lt: &'l dyn Fn(&[u8], &[u8]) -> bool,
  ) -> Result<PageId> {
    BatchInsert::new(&mut self.root.vendor, root, iter, lt).run()
  }

  pub fn insert_typed<K, V, const N: usize, const M: usize>(
    &mut self,
    root: PageId,
    key: &K,
    value: &V,
  ) -> Result<PageId>
    where
      P: PageVendor,
      [u8; N]: From<K>,
      [u8; M]: From<V>,
      K: From<[u8; N]> + Clone + Ord,
      V: Clone,
  {
    insert(
      &mut self.root.vendor,
      root,
      &Into::<[u8; N]>::into(key.clone()),
      &Into::<[u8; M]>::into(value.clone()),
      &|a: &[u8], b: &[u8]| {
        let a: [u8; N] = a.try_into().unwrap();
        let b: [u8; N] = b.try_into().unwrap();
        Into::<K>::into(a).cmp(&Into::<K>::into(b.clone())) == Ordering::Less
      })
  }

  pub fn insert(
    &mut self,
    mut root: PageId,
    key: &[u8],
    value: &[u8],
    lt: &dyn Fn(&[u8], &[u8]) -> bool,
  ) -> Result<PageId> {
    insert(&mut self.root.vendor, root, key, value, lt)
  }

  pub fn update(
    &mut self,
    key: &[u8],
    value: &[u8],
    id: PageId,
    lt: &dyn Fn(&[u8], &[u8]) -> bool
  ) -> Result<Option<PageId>> {
    update(&mut self.root.vendor, key, value, id, lt)
  }

  pub fn update_typed<'p, K, V, const N: usize, const M: usize>(
    &mut self,
    key: &K,
    value: &V,
    id: PageId,
  ) -> Result<Option<PageId>>
    where
      [u8; N]: From<K>,
      [u8; M]: From<V>,
      K: From<[u8; N]> + Clone + Ord,
      V: Clone,
  {
    update_typed(&mut self.root.vendor, key, value, id)
  }
}

pub struct BTreeRoot<P: PageVendor> {
  vendor: P,
}

// - Database Page List
//   - id: 0, name: metadata,      content: [ data_root_page_id, columns_root_page_id, index_root_page_id, index_columns_page_id ]
//   - id: 1, name: data,          content: { key: TableId, value: PageId }
//   - id: 2, name: columns,       content: { key: TableColumnId, value: Type }
//   - id: 3, name: index,         content: { key: TableId, value: PageId }
//   - id: 4, name: index_columns, content: { key: TableColumnId, value: Type }

impl<P: PageVendor> BTreeRoot<P> {
  pub fn new(mut vendor: P) -> Result<BTreeRoot<P>> {
    let data = LeafMut::new(size_of::<TableId>(), size_of::<PageId>());
    let columns = LeafMut::new(size_of::<TableColumnId>(), size_of::<BinType>());
    let indices = LeafMut::new(size_of::<TableId>(), size_of::<PageId>());
    let index_columns = LeafMut::new(size_of::<TableColumnId>(), size_of::<BinType>());
    let data_id = vendor.save(data)?;
    let columns_id = vendor.save(columns)?;
    let indices_id = vendor.save(indices)?;
    let index_columns_id = vendor.save(index_columns)?;
    let mut root: MetadataMut = vendor.copy_meta()?;
    let body_mut: &mut MetadataBody = root.borrow_mut();
    body_mut.set_data_root(data_id);
    body_mut.set_columns_root(columns_id);
    body_mut.set_indices_root(indices_id);
    body_mut.set_index_columns_root(index_columns_id);
    vendor.save_meta(root)?;
    // let vendor = RefCell::new(vendor);
    Ok(BTreeRoot { vendor })
  }

  pub fn data_root(&self) -> Result<PageId> {
    let meta_node = self.vendor.borrow().fetch_meta()?;
    let meta: &MetadataBody = meta_node.borrow();
    Ok(meta.data_root())
  }

  pub fn columns_root(&self) -> Result<PageId> {
    let meta_node = self.vendor.borrow().fetch_meta()?;
    let meta: &MetadataBody = meta_node.borrow();
    Ok(meta.columns_root())
  }

  pub fn indices_root(&self) -> Result<PageId> {
    let meta_node = self.vendor.borrow().fetch_meta()?;
    let meta: &MetadataBody = meta_node.borrow();
    Ok(meta.indices_root())
  }

  pub fn fetch_types_helper(&self, table_id: TableId) -> Result<Vec<Vec<Type>>> {
    let types = self.fetch_types(table_id)?;
    Ok(vec![vec![types[0]], types[1..].to_vec()])
  }

  pub fn fetch_schema(&self, table_id: TableId) -> Result<Table> {
    Ok(Table::new(self.fetch_types(table_id)?))
  }

  pub fn fetch_primary_column_type(&self, table_id: TableId) -> Result<Type> {
    let table_column_id = TableColumnId::new(table_id, ColumnId::zero());
    Ok(get_typed(self.vendor.borrow(), self.columns_root()?, &table_column_id)?.unwrap())
  }

  pub fn fetch_types(&self, table_id: TableId) -> Result<Vec<Type>> {
    let beg = Value::TableColumnId(TableColumnId::new(table_id, ColumnId::zero()));
    let end = Value::TableColumnId(TableColumnId::new(table_id.succ(), ColumnId::zero()));
    let iter = range_scan(&self.vendor, self.columns_root()?, &beg.into(), &end.into(), Type::TableColumnId.into())?;
    Ok(iter.map(|(_, v)| v.into()).collect())
  }

  pub fn fetch_table_ids(&self) -> Result<Vec<TableId>> {
    let iter = full_scan(&self.vendor, self.data_root()?)?;
    Ok(iter.map(|(k, _)| k.into()).collect())
  }

  pub fn get_typed<K, V, const N: usize, const M: usize>(&self, id: PageId, key: &K) -> Result<Option<V>>
    where
      [u8; N]: From<K>,
      [u8; M]: From<V>,
      K: From<[u8; N]> + Clone + Ord,
      V: From<[u8; M]>,
  {
    get_typed(self.vendor.borrow(), id, key)
  }

  pub fn range_scan<'f, 'p>(
    &'p self,
    root: PageId,
    from: &Bytes,
    to: &Bytes,
    lt: &'f dyn Fn(&[u8], &[u8]) -> bool,
  ) -> Result<RangeScan<'f, 'p, P>>
  {
    range_scan(&self.vendor, root, from, to, lt)
  }

  pub fn full_scan(&self, root: PageId) -> Result<FullScan<P>> {
    full_scan(&self.vendor, root)
  }

  pub fn first_page_id(&self, id: PageId) -> Result<PageId> {
    first_page_id(&self.vendor, id)
  }

  pub fn get_iter(
    &self, key: Bytes, id: PageId, lt: &dyn Fn(&[u8], &[u8]) -> bool
  ) -> Result<IntoIter<Vec<Bytes>>>
  {
    get_iter(&self.vendor, key, id, lt)
  }

  pub fn insert(
    &mut self,
    root: PageId,
    key: &[u8],
    value: &[u8],
    lt: &dyn Fn(&[u8], &[u8]) -> bool,
  ) -> Result<PageId> {
    insert(&mut self.vendor, root, key, value, lt)
  }

  pub fn update_typed<K, V, const N: usize, const M: usize>(
    &mut self,
    key: &K,
    value: &V,
    id: PageId,
  ) -> Result<Option<PageId>>
    where
      [u8; N]: From<K>,
      [u8; M]: From<V>,
      K: From<[u8; N]> + Clone + Ord,
      V: Clone,
  {
    update_typed(&mut self.vendor, key, value, id)
  }

  pub fn mutate<F, V>(&mut self, mut action: F) -> Result<V>
    where
      F: FnMut(&mut Mutator<P>) -> Result<V>,
  {
    let result = {
      let mut meta = self.vendor.copy_meta()?;
      let mut mutator = Mutator { root: self, meta: &mut meta };
      let result = action(&mut mutator)?;
      self.vendor.save_meta(meta)?;
      result
    };
    Ok(result)
  }
}

#[cfg(test)]
mod tests {
  use std::collections::BTreeMap;
  use std::convert::TryInto;
  use std::mem::size_of;

  use bytes::{Bytes, BytesMut, BufMut};
  use rand::prelude::*;
  use rand_pcg::Pcg64;

  use crate::page::DummyPageVendor;
  use super::*;

  fn lt_u64(lhs: &[u8], rhs: &[u8]) -> bool {
    return u64::from_le_bytes(lhs.try_into().unwrap()) < u64::from_le_bytes(rhs.try_into().unwrap())
  }

  fn to_u64_le(value: u64) -> Bytes {
    let mut bytes = BytesMut::with_capacity(size_of::<u64>());
    bytes.put_u64_le(value);
    bytes.freeze()
  }

  #[test]
  fn range_scan_1() {
    let mut vendor = DummyPageVendor::new();
    let node: LeafMut = vendor.borrow().new_page(8, 8);
    let id = vendor.borrow_mut().save(node).unwrap();
    let from = to_u64_le(0);
    let to = to_u64_le(1);
    assert_eq!(0, range_scan(&vendor, id, &from, &to, &lt_u64).unwrap().count());
  }

  #[test]
  fn range_scan_2() {
    let mut vendor = DummyPageVendor::new();
    let leaf: LeafMut = vendor.borrow().new_page(8, 8);
    let id = vendor.borrow_mut().save(leaf).unwrap();
    let k1 = to_u64_le(0);
    let k2 = to_u64_le(1);
    let k3 = to_u64_le(2);
    let k4 = to_u64_le(3);
    let v1 = to_u64_le(1);
    let id = insert(&mut vendor, id, &k2, &v1, &lt_u64).unwrap();
    let leaf: Leaf = vendor.borrow_mut().fetch(id).unwrap();

    assert_eq!(1, leaf.header().len());

    let result: BTreeMap<_, _> = range_scan(vendor.borrow(), id, &k2, &k3, &lt_u64).unwrap().collect();

    assert_eq!(1, result.len());
    assert_eq!(Some(&v1), result.get(&k2));

    assert_eq!(0, range_scan(vendor.borrow(), id, &k1, &k2, &lt_u64).unwrap().count());
    assert_eq!(0, range_scan(vendor.borrow(), id, &k3, &k4, &lt_u64).unwrap().count());
  }

  #[test]
  fn range_scan_3() {
    let mut vendor = DummyPageVendor::new();
    let leaf: LeafMut = vendor.borrow_mut().new_page(8, 8);
    let mut id = vendor.borrow_mut().save(leaf).unwrap();
    let k0 = to_u64_le(0);
    let k1 = to_u64_le(1);
    let k2 = to_u64_le(2);
    let k3 = to_u64_le(3);
    let k4 = to_u64_le(4);
    let v1 = to_u64_le(1);
    let v2 = to_u64_le(2);
    id = insert(&mut vendor, id, &k1, &v1, &lt_u64).unwrap();
    id = insert(&mut vendor, id, &k2, &v2, &lt_u64).unwrap();

    let result: BTreeMap<_, _> = range_scan(vendor.borrow(), id, &k1, &k3, &lt_u64).unwrap().collect();

    assert_eq!(2, result.len());
    assert_eq!(Some(&v1), result.get(&k1));
    assert_eq!(Some(&v2), result.get(&k2));

    assert_eq!(0, range_scan(vendor.borrow(), id, &k0, &k1, &lt_u64).unwrap().count());
    assert_eq!(0, range_scan(vendor.borrow(), id, &k3, &k4, &lt_u64).unwrap().count());

    let result: BTreeMap<_, _> = range_scan(vendor.borrow(), id, &k0, &k2, &lt_u64).unwrap().collect();

    assert_eq!(1, result.len());
    assert_eq!(Some(&v1), result.get(&k1));
  }

  #[test]
  fn range_scan_4() {
    let mut vendor = DummyPageVendor::new();
    let node: LeafMut = vendor.borrow_mut().new_page(8, 8);
    let mut id = vendor.borrow_mut().save(node).unwrap();

    for i in 0..256 {
      let v = u64::to_le_bytes(i);
      id = insert(&mut vendor, id, &v, &v, &lt_u64).unwrap();
    }

    let beg = to_u64_le(0);
    let end = to_u64_le(256);

    assert_eq!(256, range_scan(&vendor, id, &beg, &end, &lt_u64).unwrap().count());
  }

  #[test]
  fn range_scan_5() {
    let count: usize = 100000;
    let mut vendor = DummyPageVendor::new();
    let node: LeafMut = vendor.borrow_mut().new_page(8, 8);
    let mut id = vendor.borrow_mut().save(node).unwrap();

    for i in 0..count {
      let v = to_u64_le(i as u64);
      id = insert(&mut vendor, id, &v, &v, &lt_u64).unwrap();
    }

    let beg = to_u64_le(0);
    let end = to_u64_le(count as u64);

    let result: BTreeMap<_, _> = range_scan(&vendor, id, &beg, &end, &lt_u64).unwrap().collect();

    assert_eq!(count, result.len());
    for i in 0..count {
      let v = to_u64_le(i as u64);
      assert_eq!(Some(&v), result.get(&v));
    }
  }

  #[test]
  fn range_scan_6() {
    use rand::prelude::*;

    let count = 100000;
    let mut rng = rand::thread_rng();
    let mut nums: Vec<u64> = (0..count).collect();
    nums.shuffle(&mut rng);

    let mut vendor = DummyPageVendor::new();
    let mut id = {
      let node: LeafMut = vendor.borrow_mut().new_page(8, 8);
      vendor.borrow_mut().save(node).unwrap()
    };

    for i in &nums {
      let v = to_u64_le(*i);
      id = insert(&mut vendor, id, &v, &v, &lt_u64).unwrap();
    }

    let beg = to_u64_le(0);
    let end = to_u64_le(count);
    let result: BTreeMap<_, _> = range_scan(&vendor, id, &beg, &end, &lt_u64).unwrap().collect();

    for i in 0..count {
      let v = to_u64_le(i);
      assert_eq!(Some(&v), result.get(&v));
    }
  }

  #[test]
  fn full_scan_1() {
    let mut vendor = DummyPageVendor::new();
    let leaf: LeafMut = vendor.borrow_mut().new_page(8, 8);
    let id = vendor.borrow_mut().save(leaf).unwrap();

    assert_eq!(0, full_scan(&vendor, id).unwrap().count());
  }

  #[test]
  fn full_scan_2() {
    let mut vendor = DummyPageVendor::new();
    let leaf: LeafMut = vendor.borrow_mut().new_page(8, 8);
    let mut id = vendor.borrow_mut().save(leaf).unwrap();
    let k1 = to_u64_le(0);
    let v1 = to_u64_le(0);

    id = insert(&mut vendor, id, &k1, &v1, &lt_u64).unwrap();

    assert_eq!(1, full_scan(&vendor, id).unwrap().count());

    let result: BTreeMap<_, _> = full_scan(&vendor, id).unwrap().collect();

    assert_eq!(1, result.len());
    assert_eq!(Some(&k1), result.get(&k1));
  }

  #[test]
  fn full_scan_3() {
    let mut vendor = DummyPageVendor::new();
    let leaf: LeafMut = vendor.borrow_mut().new_page(8, 8);
    let mut id = vendor.borrow_mut().save(leaf).unwrap();
    let k1 = to_u64_le(1);
    let k2 = to_u64_le(2);

    id = insert(&mut vendor, id, &k1, &k1, &lt_u64).unwrap();
    id = insert(&mut vendor, id, &k2, &k2, &lt_u64).unwrap();

    let result: BTreeMap<_, _> = full_scan(&vendor, id).unwrap().collect();

    assert_eq!(2, result.len());
    assert_eq!(Some(&k1), result.get(&k1));
    assert_eq!(Some(&k2), result.get(&k2));
  }

  #[test]
  fn full_scan_4() {
    let mut vendor = DummyPageVendor::new();
    let leaf: LeafMut = vendor.borrow_mut().new_page(8, 8);
    let mut id = vendor.save(leaf).unwrap();
    let total = 256 as usize;

    for i in 0..total {
      let v = to_u64_le(i as u64);
      id = insert(&mut vendor, id, &v, &v, &lt_u64).unwrap();
    }

    let result: BTreeMap<_, _> = full_scan(&vendor, id).unwrap().collect();

    assert_eq!(total, result.len());
    for i in 0..total {
      let v = to_u64_le(i as u64);
      assert_eq!(Some(&v), result.get(&v));
    }
  }

  #[test]
  fn batch_insert_empty() {
    let mut vendor = DummyPageVendor::new();
    let leaf: LeafMut = vendor.borrow_mut().new_page(8, 8);
    let id = vendor.borrow_mut().save(leaf).unwrap();
    let data = Vec::new();
    let mut iter = data.into_iter();
    let id = batch_insert(&mut vendor, id, &mut iter, &lt_u64).unwrap();

    assert_eq!(0, full_scan(vendor.borrow(), id).unwrap().count())
  }

  #[test]
  fn batch_insert_1_elem() -> Result<()> {
    let mut vendor = DummyPageVendor::new();
    let leaf: LeafMut = vendor.borrow_mut().new_page(8, 8);
    let id = vendor.borrow_mut().save(leaf)?;
    let k = to_u64_le(1);
    let v = to_u64_le(41);
    let data = vec![(k.clone(), v.clone())];
    let mut iter = data.into_iter();
    let id = batch_insert(&mut vendor, id, &mut iter, &lt_u64)?;

    assert_eq!(1, full_scan(vendor.borrow(), id)?.count());

    let result: BTreeMap<Bytes, Bytes> = full_scan(vendor.borrow(), id)?.collect();

    assert_eq!(Some(&v), result.get(&k));
    Ok(())
  }

  #[test]
  fn batch_insert_10_elems() -> Result<()> {
    let mut vendor = DummyPageVendor::new();
    let leaf: LeafMut = vendor.borrow_mut().new_page(8, 8);
    let id = vendor.borrow_mut().save(leaf)?;
    let data: Vec<_> = (0..10).map(|i| (to_u64_le(i), to_u64_le(i + 10))).collect();
    let mut iter = data.into_iter();
    let id = batch_insert(&mut vendor, id, &mut iter, &lt_u64)?;

    assert_eq!(10, full_scan(&vendor, id)?.count());

    let result: BTreeMap<Bytes, Bytes> = full_scan(&vendor, id)?.collect();

    for i in 0..10 {
      let k = to_u64_le(i);
      let v = to_u64_le(i + 10);
      assert_eq!(Some(&v), result.get(&k));
    }
    Ok(())
  }

  #[test]
  fn batch_insert_100k_elems() -> Result<()> {
    let count = 100000;
    let mut vendor = DummyPageVendor::new();
    let leaf: LeafMut = vendor.borrow_mut().new_page(8, 8);
    let id = vendor.borrow_mut().save(leaf)?;
    let data: Vec<_> = (0..count).map(|i| (to_u64_le(i), to_u64_le(i + 10))).collect();
    let mut iter = data.into_iter();
    let id = batch_insert(&mut vendor, id, &mut iter, &lt_u64)?;
    let result: BTreeMap<Bytes, Bytes> = full_scan(vendor.borrow(), id)?.collect();

    assert_eq!(count as usize, result.len());
    for i in 0..count {
      assert_eq!(Some(&to_u64_le(i + 10)), result.get(&to_u64_le(i)));
    }
    Ok(())
  }

  #[test]
  fn batch_insert_1k_lots_of_100_elems() -> Result<()> {
    let unit = 100;
    let count = 1000;
    let mut vendor = DummyPageVendor::new();
    let leaf: LeafMut = vendor.new_page(8, 8);
    let mut id = vendor.save(leaf)?;

    let mut rng = Pcg64::seed_from_u64(1);
    let mut lots: Vec<u64> = (0..count).collect();
    lots.shuffle(&mut rng);

    for i in lots {
      let entries: Vec<_> = (0..unit).map(|j| (to_u64_le(i * count + j), to_u64_le(i * count + j + 500))).collect();
      let mut iter = entries.into_iter();
      id = batch_insert(&mut vendor, id, &mut iter, &lt_u64).unwrap();
      let beg = Bytes::copy_from_slice(&to_u64_le(i * count));
      let end = Bytes::copy_from_slice(&to_u64_le((i + 1) * count));
      let result: BTreeMap<Bytes, Bytes> = range_scan(&vendor, id, &beg, &end, &lt_u64)?.collect();

      for j in 0..unit {
        assert_eq!(Some(&to_u64_le(i * count + j + 500)), result.get(&to_u64_le(i * count + j)))
      }
    }

    let c = full_scan(&vendor, id)?.count();
    assert_eq!(c as u64, unit * count);
    Ok(())
  }
}