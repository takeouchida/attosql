use std::collections::BTreeMap;
use std::io::{Result, ErrorKind};

use bytes::Bytes;

use attosql_commons::attosql::Row;
use attosql_commons::iter::{Cross, GroupBy, HashInnerJoin, MergeInnerJoin};
use crate::btree;
use crate::page::{PageId, PageVendor};
use crate::schema::{ColumnId, Cond, IndexId, IndexColumnId, Query, Table, TableColumnId, TableId, Type, to_value};
use crate::trans::{TransId, TransactionScheduler};

pub struct Pipe<'i> {
  pub iter: Box<dyn Iterator<Item = Vec<Bytes>> + 'i>,
  types: Vec<Vec<Type>>,
}

impl<'i> From<Pipe<'i>> for Vec<Row> {
  fn from(pipe: Pipe<'i>) -> Vec<Row> {
    let types = pipe.types;
    pipe.iter.map(|bss| {
      let mut columns = vec![];
      for i in 0..types.len() {
        let bs = &bss[i];
        let ts = &types[i];
        let mut beg = 0;
        for j in 0..ts.len() {
          let t = &ts[j];
          let size = t.size();
          let end = beg + size;
          let b = bs.slice(beg..end);
          beg = end;
          columns.push(to_value(b, t.clone()).into());
        }
      }
      Row { columns }
    }).collect()
  }
}

impl<'i> Pipe<'i> {
  fn new(iter: Box<dyn Iterator<Item = Vec<Bytes>> + 'i>, types: Vec<Type>) -> Result<Pipe<'i>> {
    Ok(Pipe { iter, types: vec![types] })
  }

  fn from_t_us(
    iter: Box<dyn Iterator<Item = (Bytes, Vec<Bytes>)> + 'i>,
    ty1: Type,
    ty2: Vec<Type>
  ) -> Result<Pipe<'i>> {
    let iter = Box::new(iter.map(|(x, ys)| {
      let mut zs = Vec::<Bytes>::with_capacity(1 + ys.len());
      zs.push(x);
      zs.extend_from_slice(&ys);
      zs
    }));
    let types = vec![vec![ty1], ty2];
    Ok(Pipe { iter, types })
  }

  fn from_new_t_us_vs(
    iter: Box<dyn Iterator<Item = (Bytes, Vec<Bytes>, Vec<Bytes>)> + 'i>,
    ty1: Vec<Type>,
    ty2: Vec<Vec<Type>>,
    ty3: Vec<Vec<Type>>
  ) -> Result<Pipe<'i>> {
    let iter = Box::new(iter.map(|(x, ys, zs)| {
      let mut vs = Vec::with_capacity(1 + ys.len() + zs.len());
      vs.push(x);
      vs.extend_from_slice(&ys);
      vs.extend_from_slice(&zs);
      vs
    }));
    let mut types = Vec::new();
    types.push(ty1);
    types.extend_from_slice(&ty2);
    types.extend_from_slice(&ty3);
    Ok(Pipe { iter, types })
  }

  fn from_new_t_u_v(
    iter: Box<dyn Iterator<Item = (Bytes, Bytes, Bytes)> + 'i>,
    ty1: Vec<Type>,
    ty2: Vec<Vec<Type>>,
    ty3: Vec<Vec<Type>>,
  ) -> Result<Pipe<'i>> {
    let iter = Box::new(iter.map(|(x, y, z)| vec![x, y, z]));
    let mut types = Vec::new();
    types.push(ty1);
    types.extend_from_slice(&ty2);
    types.extend_from_slice(&ty3);
    Ok(Pipe { iter, types })
  }

  fn merge_inner_join<'j, 'k>(pipe_left: Pipe<'j>, pipe_right: Pipe<'k>) -> Result<Pipe<'i>> where 'j: 'i, 'k: 'i {
    debug_assert!(1 < pipe_left.types.len());
    debug_assert!(1 < pipe_right.types.len());
    debug_assert!(pipe_left.types[0] == pipe_right.types[0]);

    let iter_left = Box::new(GroupBy::new(Box::new(pipe_left.iter.map(|vs| (vs[0].clone(), vs[1..].to_vec())))));
    let iter_right = Box::new(GroupBy::new(Box::new(pipe_right.iter.map(|vs| (vs[0].clone(), vs[1..].to_vec())))));
    let iter = Box::new(Cross::new(Box::new(MergeInnerJoin::new(iter_left, iter_right))));

    Pipe::from_new_t_us_vs(iter, pipe_left.types[0].clone(), pipe_left.types[1..].to_vec(), pipe_right.types[1..].to_vec())
  }

  fn hash_inner_join<'j, 'k>(
    pipe_left: Pipe<'j>,
    pipe_right: Pipe<'k>,
    slot_left: usize,
    column_left: usize,
    slot_right: usize,
    column_slot: usize
  ) -> Result<Pipe<'i>> where 'j: 'i, 'k: 'i {
    debug_assert!(slot_left < pipe_left.types.len());
    debug_assert!(column_left < pipe_left.types[slot_left].len());
    debug_assert!(slot_right < pipe_left.types.len());
    debug_assert!(column_slot < pipe_left.types[slot_right].len());

    let beg_left: usize = pipe_left.types[slot_left][0..column_left].iter().map(|ty| ty.size()).sum();
    let end_left: usize = beg_left + pipe_left.types[slot_left][column_left].size();
    let beg_right: usize = pipe_right.types[slot_right][0..column_slot].iter().map(|ty| ty.size()).sum();
    let end_right: usize = beg_right + pipe_right.types[slot_right][column_slot].size();
    let iter_left = Box::new(pipe_left.iter.map(move |vs| (vs[slot_left].slice(beg_left..end_left), vs)));
    let iter_right = Box::new(pipe_right.iter.map(move |vs| (vs[slot_right].slice(beg_right..end_right), vs)));
    let iter = Box::new(Cross::new(Box::new(HashInnerJoin::new(iter_left, iter_right))));

    Pipe::from_new_t_u_v(iter, pipe_left.types[0].clone(), pipe_left.types[1..].to_vec(), pipe_right.types[1..].to_vec())
  }
}

pub struct Database<P> where P: PageVendor, {
  root: btree::BTreeRoot<P>,
  scheduler: TransactionScheduler,
  trans: BTreeMap<TransId, PageId>,
}

unsafe impl<P> Sync for Database<P> where P: PageVendor {}

impl<P: PageVendor> Database<P> {
  pub fn new(root: btree::BTreeRoot<P>) -> Result<Database<P>> {
    let scheduler = TransactionScheduler::new();
    let trans = BTreeMap::new();
    Ok(Database { root, scheduler, trans })
  }

  pub fn fetch_primary_column_type(&self, table_id: TableId) -> Result<Type> {
    self.root.fetch_primary_column_type(table_id)
  }

  pub fn fetch_schema(&self, table_id: TableId) -> Result<Table> {
    self.root.fetch_schema(table_id)
  }

  pub fn fetch_table_ids(&self) -> Result<Vec<TableId>> {
    self.root.fetch_table_ids()
  }

  pub fn begin(&mut self) -> Result<TransId> {
    let data_root_id = self.root.data_root()?;
    let trans_id = self.scheduler.begin();
    self.trans.insert(trans_id.clone(), data_root_id);
    Ok(trans_id)
  }

  pub fn commit(&mut self, trans_id: &TransId) -> Result<bool> {
    let schedule = self.scheduler.schedule_to_commit(trans_id);
    match schedule {
      None => {
        self.scheduler.rollback(trans_id);
        self.trans.remove(trans_id);
        Ok(false)
      },
      Some(es) => {
        self.root.mutate(|m| {
          m.data(|s| {
            for e in &es {
              let ty = s.fetch_primary_column_type(e.table_id)?;
              let mut page_id = s.get_typed(&e.table_id)?.unwrap();
              let opt_page_id = s.update(&e.key, &e.value, page_id, ty.into())?;
              match opt_page_id {
                None => {
                  page_id = s.insert(page_id, &e.key, &e.value, ty.into())?;
                },
                Some(id) => {
                  page_id = id;
                }
              }
              s.update_typed(&e.table_id, &page_id)?;
            }
            Ok(())
          })
        })?;
        self.scheduler.commit(trans_id);
        self.trans.remove(trans_id);
        Ok(true)
      }
    }
  }

  pub fn rollback(&mut self, trans_id: &TransId) {
    self.scheduler.rollback(trans_id);
    self.trans.remove(trans_id);
  }

  pub fn create_table(&mut self, table: &Table) -> Result<TableId> {
    debug_assert!(1 < table.types.len());

    self.root.mutate(|m| {
      // Creates a first blank data page of the table and the PageId to save.
      let table_id = m.create_table(table.key_size(), table.value_size())?;

      // Saves the columns of the new table.
      m.column(|s| {
        let mut column_iter = table.types.iter().enumerate().map(|(i, ty)| {
          let column_id: ColumnId = (i as u16).into();
          let key: Bytes = TableColumnId::new(table_id, column_id).into();
          let ty: Bytes = (*ty).into();
          (key, ty)
        });
        s.batch_insert(&mut column_iter, Type::TableColumnId.into())
      })?;

      Ok(table_id)
    })
  }

  pub fn drop_table(&mut self, _table: &Table) {
  }

  pub fn create_index(&mut self, table_id: TableId, pos: usize) -> Result<IndexId> {
    let table = self.root.fetch_schema(table_id)?;
    let key_size = table.get_type()[pos].size();
    let value_size = table.key_size();
    let lt: &dyn Fn(&[u8], &[u8]) -> bool = table.get_type()[pos].into();
    let values: Vec<_> = self.plan(Query::FullScan { table_id }, None)?.iter.collect();

    self.root.mutate(|m| {
      // Creates a new index brank node.
      let index_page_id = m.create_index(key_size, value_size)?;

      // Scans all rows in the given table.
      let mut pairs = values.iter().map(|vs| {
        let v: Bytes = table.get_value(vs[1].clone(), pos).into();
        (vs[0].clone(), v)
      });
      m.index(|s| {
        s.batch_insert(&mut pairs, &lt)?;
        Ok(())
      })?;

      // Saves the index column PageId.
      m.column_index(|s| {
        let index_column_id = IndexColumnId::new(index_page_id, pos.into());
        s.insert_typed(&index_column_id, &index_page_id);
        Ok(())
      })?;

      Ok(index_page_id)
    })
  }

  pub fn drop_index(&mut self, _index_id: IndexId) {
  }

  pub fn add_column(&mut self, _id: TableId, _ty: Type) -> ColumnId {
    todo!()
  }

  pub fn drop_column(&mut self, _table_id: TableId, _column_id: ColumnId) {
  }

  pub fn insert(&mut self, table_id: TableId, key: &[u8], value: &[u8]) -> Result<()> {
    let ty = self.root.fetch_primary_column_type(table_id)?;
    self.root.mutate(|m| {
      m.data(|s| {
        let data_page_id: PageId = s.get_typed(&table_id)?.unwrap();
        let new_data_page_id = s.insert(data_page_id, key, value, ty.into())?;
        s.update_typed(&table_id, &new_data_page_id)
      })
    })?;
    Ok(())
  }

  pub fn insert_trans(&mut self, trans_id: &TransId, table_id: &TableId, key: &[u8], value: &[u8]) -> Result<()> {
    debug_assert!(self.trans.contains_key(trans_id));
    let ty = self.root.fetch_primary_column_type(table_id.clone())?;
    let data_root_id = self.trans[trans_id];
    let page_id = self.root.get_typed(data_root_id, table_id)?.unwrap();
    let page_id = self.root.insert(page_id, key, value, ty.into())?;
    let data_root_id: PageId = self.root.update_typed(table_id, &page_id, data_root_id)?.unwrap();
    let k = Bytes::copy_from_slice(key);
    let v = Bytes::copy_from_slice(value);
    self.scheduler.write(trans_id, table_id, &k, &v);
    self.trans.insert(trans_id.clone(), data_root_id);
    Ok(())
  }

  // The keys must be sorted in ascending order.
  pub fn batch_insert(&mut self, table_id: TableId, params: &mut dyn Iterator<Item=(Bytes, Bytes)>) -> Result<()> {
    let ty = self.root.fetch_primary_column_type(table_id)?;
    self.root.mutate(|m| {
      m.data(|s| {
        let page_id: PageId = s.get_typed(&table_id)?.unwrap();
        let new_table_page_id = s.batch_insert(params, ty.into())?;
        s.update_typed(&table_id, &new_table_page_id)
      })
    })
  }

  pub fn update(&mut self, _id: TableId, _columns: Vec<ColumnId>, _values: Vec<&[u8]>, _cond: Cond) -> usize {
    todo!()
  }

  pub fn batch_update(&mut self, _id: TableId, _columns: Vec<ColumnId>, _values: Vec<Vec<&[u8]>>, _cond: Cond) -> usize {
    todo!()
  }

  pub fn delete(&mut self, _id: TableId, _cond: Cond) -> usize {
    todo!()
  }

  pub fn plan<'q, 'd>(&'d self, query: Query, trans_id: Option<TransId>) -> Result<Pipe<'q>> where 'd: 'q {
    let data_root_id = match trans_id {
      None => self.root.data_root()?,
      Some(id) => {
        match self.trans.get(&id) {
          None => return Err(ErrorKind::NotFound.into()),
          Some(pid) => pid.clone(),
        }
      }
    };
    match query {
      Query::FullScan{table_id} => {
        let table_data_page_id = self.root.get_typed(data_root_id, &table_id)?.unwrap();
        let iter = Box::new(self.root.full_scan(table_data_page_id)?.map(|(k, v)| vec![k, v]));
        let types = self.root.fetch_types_helper(table_id)?;
        Ok(Pipe { iter, types })
      },
      Query::RangeScan{beg, end, table_id} => {
        let ty = self.root.fetch_primary_column_type(table_id)?;
        debug_assert!(ty.matches(&beg));
        debug_assert!(ty.matches(&end));
        let table_data_page_id = self.root.get_typed(data_root_id, &table_id)?.unwrap();
        let iter = Box::new(self.root.range_scan(table_data_page_id, &beg.into(), &end.into(), ty.into())?.map(|(k, v)| vec![k, v]));
        let types = self.root.fetch_types_helper(table_id)?;
        Ok(Pipe { iter, types })
      },
      Query::IndexRangeScan{beg: _, end: _, table_id, column} => {
        let table_column_id = TableColumnId::new(table_id, column.into());
        let _index_page_id: PageId = self.root.get_typed(self.root.indices_root()?, &table_column_id)?.unwrap();
        let table_data_page_id = self.root.get_typed(data_root_id, &table_id)?.unwrap();
        let page_id: PageId = self.root.first_page_id(table_data_page_id)?;
        let _iter = Box::new(self.root.full_scan(page_id)?.map(|(k, v)| vec![k, v]));
        todo!()
      },
      Query::Filter{source: _, filter: _} => {
        todo!()
      },
      Query::MergeJoin{left, right} => {
        let left = self.plan(*left, trans_id)?;
        let right = self.plan(*right, trans_id)?;
        Pipe::merge_inner_join(left, right)
      },
      Query::HashJoin{left, right, left_slot, left_column, right_slot, right_column} => {
        let left = self.plan(*left, trans_id)?;
        let right = self.plan(*right, trans_id)?;
        Pipe::hash_inner_join(left, right, left_slot, left_column, right_slot, right_column)
      },
      Query::GroupBy{source: _, by: _} => {
        todo!()
      },
      Query::Select{source: _, columns: _} => {
        todo!()
      },
      Query::FetchByPK{table_id, key} => {
        let ty = self.root.fetch_primary_column_type(table_id)?;
        debug_assert!(ty.matches(&key));
        let table_data_page_id = self.root.get_typed(data_root_id, &table_id)?.unwrap();
        let iter = Box::new(self.root.get_iter(key.into(), table_data_page_id, ty.into())?);
        let types = vec![vec![ty]];
        Ok(Pipe { iter, types })
      },
    }
  }
}

#[cfg(test)]
mod tests {
  use bytes::{Bytes, BufMut, BytesMut};
  use std::mem::size_of;
  use crate::page::DummyPageVendor;
  use crate::schema::Value;
  use super::*;

  fn to_u32_le(value: u32) -> Bytes {
    let mut bytes = BytesMut::with_capacity(size_of::<u32>());
    bytes.put_u32_le(value);
    bytes.freeze()
  }

  fn to_i32_le(value: i32) -> Bytes {
    let mut bytes = BytesMut::with_capacity(size_of::<i32>());
    bytes.put_i32_le(value);
    bytes.freeze()
  }

  fn to_char(s: &str, len: usize) -> Bytes {
    let n = s.len();
    assert!(n <= len);
    let mut bytes = BytesMut::with_capacity(len + 1);
    bytes.put_u8(n as u8);
    bytes.put(s.as_bytes());
    (0..(len - n)).for_each(|_| bytes.put_u8(0));
    bytes.freeze()
  }

  // #[test]
  fn empty_db() -> Result<()>{
    let mut db = Database::new(btree::BTreeRoot::new(DummyPageVendor::new())?)?;
    let types = vec![Type::I32, Type::Char(10)];
    let table = Table::new(types);
    let table_id = db.create_table(&table)?;
    let query = Query::FullScan { table_id };
    let iter = db.plan(query, None)?;

    assert_eq!(0, iter.iter.count());
    Ok(())
  }

  // #[test]
  fn insert_2elems() -> Result<()> {
    let mut db = Database::new(btree::BTreeRoot::new(DummyPageVendor::new())?)?;
    let types = vec![Type::I32, Type::Char(10)];
    let table = Table::new(types);
    let table_id = db.create_table(&table)?;
    let k1 = to_u32_le(41);
    let k2 = to_u32_le(42);
    let v1 = to_char("hello", 10);
    let v2 = to_char("world", 10);
    db.insert(table_id, &k1, &v1)?;
    db.insert(table_id, &k2, &v2)?;
    let plan = Query::FullScan { table_id };
    let mut iter = db.plan(plan, None)?;

    assert_eq!(Some(vec![k1, v1]), iter.iter.next());
    assert_eq!(Some(vec![k2, v2]), iter.iter.next());
    assert_eq!(None, iter.iter.next());
    Ok(())
  }

  // #[test]
  fn insert_2elems_range_scan() -> Result<()> {
    let mut db = Database::new(btree::BTreeRoot::new(DummyPageVendor::new())?)?;
    let types = vec![Type::I32, Type::Char(10)];
    let table = Table::new(types);
    let table_id = db.create_table(&table)?;
    let k1 = to_i32_le(41);
    let k2 = to_i32_le(42);
    let v1 = to_char("hello", 10);
    let v2 = to_char("world", 10);
    db.insert(table_id, &k1, &v1)?;
    db.insert(table_id, &k2, &v2)?;
    let plan = Query::RangeScan { beg: Value::I32(41), end: Value::I32(42), table_id };
    let mut iter = db.plan(plan, None)?;

    assert_eq!(Some(vec![k1, v1]), iter.iter.next());
    assert_eq!(None, iter.iter.next());
    Ok(())
  }

  // #[test]
  fn insert_2elems_and_fetch_by_pk() -> Result<()> {
    let mut db = Database::new(btree::BTreeRoot::new(DummyPageVendor::new())?)?;
    let types = vec![Type::I32, Type::Char(10)];
    let table = Table::new(types);
    let table_id = db.create_table(&table)?;
    let k1 = to_i32_le(41);
    let k2 = to_i32_le(42);
    let v1 = to_char("hello", 10);
    let v2 = to_char("world", 10);
    db.insert(table_id, &k1, &v1)?;
    db.insert(table_id, &k2, &v2)?;

    let query = Query::FetchByPK { table_id, key: Value::I32(41) };
    let mut iter = db.plan(query, None)?;

    assert_eq!(Some(vec![k1, v1]), iter.iter.next());
    assert_eq!(None, iter.iter.next());

    let query = Query::FetchByPK { table_id, key: Value::I32(42) };
    let mut iter = db.plan(query, None).unwrap();

    assert_eq!(Some(vec![k2, v2]), iter.iter.next());
    assert_eq!(None, iter.iter.next());
    Ok(())
  }

  // #[test]
  fn begin_insert_commit() -> Result<()> {
    let mut db = Database::new(btree::BTreeRoot::new(DummyPageVendor::new())?)?;
    let types = vec![Type::I32, Type::Char(10)];
    let table = Table::new(types);
    let table_id = db.create_table(&table)?;
    let k1 = to_i32_le(41);
    let k2 = to_i32_le(42);
    let v1 = to_char("hello", 10);
    let v2 = to_char("world", 10);

    // v1 := k1 in t1
    let trans_id1 = db.begin()?;
    db.insert_trans(&trans_id1, &table_id, &k1, &v1)?;

    {
      let query = Query::FullScan { table_id };
      let mut iter = db.plan(query, None)?;

      // Nothing inserted yet.
      assert_eq!(None, iter.iter.next());
    }

    // v2 := k2 in t2
    let trans_id2 = db.begin().unwrap();
    db.insert_trans(&trans_id2, &table_id, &k2, &v2)?;

    {
      let query = Query::FullScan { table_id };
      let mut iter = db.plan(query, None).unwrap();

      // Nothing inserted yet.
      assert_eq!(None, iter.iter.next());
    }

    {
      let query = Query::FullScan { table_id };
      let mut iter = db.plan(query, Some(trans_id1))?;

      // (k1, v1) is inserted in t1.
      assert_eq!(Some(vec![k1.clone(), v1.clone()]), iter.iter.next());
      assert_eq!(None, iter.iter.next());
    }

    {
      let query = Query::FullScan { table_id };
      let mut iter = db.plan(query, Some(trans_id2))?;

      // (k2, v2) is inserted in t2.
      assert_eq!(Some(vec![k2.clone(), v2.clone()]), iter.iter.next());
      assert_eq!(None, iter.iter.next());
    }

    db.commit(&trans_id1)?;

    {
      let query = Query::FullScan { table_id };
      let mut iter = db.plan(query, None)?;

      // (k1, v1) is inserted.
      assert_eq!(Some(vec![k1.clone(), v1.clone()]), iter.iter.next());
      assert_eq!(None, iter.iter.next());
    }

    db.commit(&trans_id2)?;

    {
      let query = Query::FullScan { table_id };
      let mut iter = db.plan(query, None)?;

      // (k1, v1), (k2, v2) is inserted.
      assert_eq!(Some(vec![k1, v1]), iter.iter.next());
      assert_eq!(Some(vec![k2, v2]), iter.iter.next());
      assert_eq!(None, iter.iter.next());
    }
    Ok(())
  }
}
