use std::cmp::Ordering;
use std::convert::TryInto;
use std::mem::size_of;

use bytes::{Bytes, BytesMut, Buf, BufMut};

use attosql_commons::conv::deref_helper;
use attosql_commons::attosql;

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum Type {
  Unknown,
  U8,
  U16,
  I32,
  U32,
  I64,
  U64,
  Date,     // days from 2000-01-01, 3-byte
  DateTime, // msecs from 2000-01-01, 6-byte
  Char(usize),
  Type,
  TableColumnId,
  TableId,
  IndexId,
}

impl From<[u8; 2]> for Type    { fn from(bytes: [u8; 2]) -> Type { let ty: BinType = bytes.into(); ty.into() } }
impl From<Type>    for [u8; 2] { fn from(ty: Type) -> [u8; 2]    { let bin: BinType = ty.into(); bin.into() } }
impl From<Bytes>   for Type    { fn from(value: Bytes) -> Type   { let ty = BinType(value[..].try_into().unwrap()); ty.into() } }

impl From<Type> for Bytes {
  fn from(ty: Type) -> Bytes {
    let bin: BinType = ty.into();
    let bytes: [u8; 2] = bin.into();
    Bytes::copy_from_slice(&bytes)
  }
}

impl From<attosql::ColumnSpec> for Type {
  fn from(spec: attosql::ColumnSpec) -> Type {
    match spec.r#type() {
      attosql::Type::U8   => Type::U8,
      attosql::Type::U16  => Type::U16,
      attosql::Type::I32  => Type::I32,
      attosql::Type::U32  => Type::U32,
      attosql::Type::U64  => Type::U64,
      attosql::Type::Char => Type::Char(spec.len as usize),
      _                   => unimplemented!(),
    }
  }
}

impl Type {
  pub fn size(&self) -> usize {
    match self {
      Type::Unknown  => 0,
      Type::U8       => size_of::<u8>(),
      Type::U16      => size_of::<u16>(),
      Type::I32      => size_of::<i32>(),
      Type::U32      => size_of::<u32>(),
      Type::I64      => size_of::<i64>(),
      Type::U64      => size_of::<u64>(),
      Type::Date     => 3,
      Type::DateTime => 6,
      Type::Char(n)  => {
        debug_assert!(*n < 256);
        *n + 1
      }
      Type::Type          => 2,
      Type::TableColumnId => size_of::<TableColumnId>(),
      Type::TableId       => size_of::<TableId>(),
      Type::IndexId       => size_of::<IndexId>(),
    }
  }

  pub fn matches(&self, value: &Value) -> bool {
    match self {
      Type::U8            => if let Value::U8(_) = value { true } else { false }
      Type::U16           => if let Value::U16(_) = value { true } else { false }
      Type::I32           => if let Value::I32(_) = value { true } else { false }
      Type::U32           => if let Value::U32(_) = value { true } else { false }
      Type::U64           => if let Value::U64(_) = value { true } else { false }
      Type::Date          => if let Value::Date(_) = value { true } else { false }
      Type::DateTime      => if let Value::DateTime(_) = value { true } else { false }
      Type::Char(_)       => if let Value::Char(_) = value { true } else { false }
      Type::Type          => if let Value::Type(_) = value { true } else { false }
      Type::TableColumnId => if let Value::TableColumnId(_) = value { true } else { false }
      Type::TableId       => if let Value::TableId(_) = value { true } else { false }
      _                   => false
    }
  }
}

impl<'a> From<Type> for &'a dyn Fn(&[u8], &[u8]) -> bool {
  fn from(ty: Type) -> &'a dyn Fn(&[u8], &[u8]) -> bool {
    match ty {
      Type::U8  => &|x, y| u8::from_le_bytes(x.try_into().unwrap()) < u8::from_le_bytes(y.try_into().unwrap()),
      Type::U16 => &|x, y| u16::from_le_bytes(x.try_into().unwrap()) < u16::from_le_bytes(y.try_into().unwrap()),
      Type::I32 => &|x, y| i32::from_le_bytes(x.try_into().unwrap()) < i32::from_le_bytes(y.try_into().unwrap()),
      Type::U32 => &|x, y| u32::from_le_bytes(x.try_into().unwrap()) < u32::from_le_bytes(y.try_into().unwrap()),
      Type::U64 => &|x, y| u64::from_le_bytes(x.try_into().unwrap()) < u64::from_le_bytes(y.try_into().unwrap()),
      Type::Char(_) => &|x, y| {
        let end_x = x[0] as usize + 1;
        let end_y = y[0] as usize + 1;
        x[1..end_x] < y[1..end_y]
      },
      Type::TableColumnId => &|x, y| {
        let x: &TableColumnId = unsafe { deref_helper(x) };
        let y: &TableColumnId = unsafe { deref_helper(y) };
        if x.0.0 == y.0.0 {
          x.1.0 < y.1.0
        } else {
          x.0.0 < y.0.0
        }
      },
      Type::TableId => &|x, y| {
        let x: &TableId = unsafe { deref_helper(x) };
        let y: &TableId = unsafe { deref_helper(y) };
        x.0 < y.0
      },
      Type::IndexId => &|x, y| {
        let x: &IndexId = unsafe { deref_helper(x) };
        let y: &IndexId = unsafe { deref_helper(y) };
        x.0 < y.0
      },
      _ => unimplemented!(),
    }
  }
}

#[derive(Copy, Clone, Debug, Eq, Ord, PartialEq, PartialOrd)]
#[repr(packed)]
pub struct BinType([u8; 2]);

impl From<[u8; 2]> for BinType { fn from(value: [u8; 2]) -> BinType { BinType(value) } }
impl From<BinType> for [u8; 2] { fn from(value: BinType) -> [u8; 2] { value.0 } }

impl From<Type> for BinType {
  fn from(ty: Type) -> BinType {
    match ty {
      Type::Unknown  => BinType([0, 0]),
      Type::U8       => BinType([1, 0]),
      Type::U16      => BinType([2, 0]),
      Type::I32      => BinType([3, 0]),
      Type::U32      => BinType([4, 0]),
      Type::I64      => BinType([5, 0]),
      Type::U64      => BinType([6, 0]),
      Type::Date     => BinType([7, 0]),
      Type::DateTime => BinType([8, 0]),
      Type::Char(n)  => {
        debug_assert!(n < 256);
        BinType([9, n as u8])
      }
      Type::Type          => BinType([10, 0]),
      Type::TableColumnId => BinType([11, 0]),
      Type::TableId       => BinType([12, 0]),
      Type::IndexId       => BinType([13, 0]),
    }
  }
}

impl From<BinType> for Type {
  fn from(bytes: BinType) -> Type {
    match bytes.0 {
      [0, _]  => Type::Unknown,
      [1, _]  => Type::U8,
      [2, _]  => Type::U16,
      [3, _]  => Type::I32,
      [4, _]  => Type::U32,
      [5, _]  => Type::I64,
      [6, _]  => Type::U64,
      [7, _]  => Type::Date,
      [8, _]  => Type::DateTime,
      [9, n]  => Type::Char(n as usize),
      [10, _]  => Type::Type,
      [11, _] => Type::TableColumnId,
      [12, _] => Type::TableId,
      [13, _] => Type::IndexId,
      _ => panic!("From<BinType>::from: unsupported bytes: {:?}", bytes),
    }
  }
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
#[repr(packed)]
pub struct ColumnId([u8; 2]);

impl From<[u8; 2]>  for ColumnId { #[inline] fn from(value: [u8; 2])  -> ColumnId { ColumnId(value) } }
impl From<u16>      for ColumnId { #[inline] fn from(value: u16)      -> ColumnId { ColumnId(u16::to_le_bytes(value)) } }
impl From<usize>    for ColumnId { #[inline] fn from(value: usize)    -> ColumnId { ColumnId(u16::to_le_bytes(value.try_into().unwrap())) } }
impl From<ColumnId> for [u8; 2]  { #[inline] fn from(value: ColumnId) -> [u8; 2]  { value.0 } }
impl From<ColumnId> for u16      { #[inline] fn from(value: ColumnId) -> u16      { u16::from_le_bytes(value.0) } }
impl From<ColumnId> for usize    { #[inline] fn from(value: ColumnId) -> usize    { let u: u16 = value.into(); u as usize } }

impl Ord for ColumnId {
  fn cmp(&self, other: &Self) -> Ordering {
    let x: u16 = (*self).into();
    let y: u16 = (*other).into();
    x.cmp(&y)
  }
}

impl PartialOrd for ColumnId {
  fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
    let x: u16 = (*self).into();
    let y: u16 = (*other).into();
    Some(x.cmp(&y))
  }
}

impl ColumnId {
  pub fn zero() -> ColumnId {
    ColumnId(u16::to_le_bytes(0))
  }
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
#[repr(packed)]
pub struct IndexId([u8; 2]);

impl From<[u8; 2]>  for IndexId { #[inline] fn from(value: [u8; 2])  -> IndexId { IndexId(value) } }
impl From<u16>      for IndexId { #[inline] fn from(value: u16)      -> IndexId { IndexId(u16::to_le_bytes(value)) } }
impl From<IndexId> for [u8; 2]  { #[inline] fn from(value: IndexId) -> [u8; 2]  { value.0 } }
impl From<IndexId> for u16      { #[inline] fn from(value: IndexId) -> u16      { u16::from_le_bytes(value.0) } }

impl Ord for IndexId {
  fn cmp(&self, other: &Self) -> Ordering {
    let x: u16 = (*self).into();
    let y: u16 = (*other).into();
    x.cmp(&y)
  }
}

impl PartialOrd for IndexId {
  fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
    let x: u16 = (*self).into();
    let y: u16 = (*other).into();
    Some(x.cmp(&y))
  }
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
#[repr(packed)]
pub struct TableId([u8; 2]);

impl From<[u8; 2]>  for TableId { #[inline] fn from(value: [u8; 2])  -> TableId { TableId(value) } }
impl From<u16>      for TableId { #[inline] fn from(value: u16)      -> TableId { TableId(u16::to_le_bytes(value)) } }
impl From<u32>      for TableId { #[inline] fn from(value: u32)      -> TableId { TableId(u16::to_le_bytes(value as u16)) } }
impl From<Bytes>    for TableId { #[inline] fn from(value: Bytes)    -> TableId { TableId((&value[..]).try_into().unwrap()) } }
impl From<TableId> for [u8; 2]  { #[inline] fn from(value: TableId) -> [u8; 2]  { value.0 } }
impl From<TableId> for u16      { #[inline] fn from(value: TableId) -> u16      { u16::from_le_bytes(value.0) } }
impl From<TableId> for u32      { #[inline] fn from(value: TableId) -> u32      { u16::from_le_bytes(value.0) as u32 } }

impl From<TableId> for Bytes {
  #[inline]
  fn from(value: TableId) -> Bytes {
    let mut bytes = BytesMut::new();
    bytes.copy_from_slice(&Into::<[u8; 2]>::into(value));
    bytes.freeze()
  }
}

impl Ord for TableId {
  fn cmp(&self, other: &Self) -> Ordering {
    let x: u16 = (*self).into();
    let y: u16 = (*other).into();
    x.cmp(&y)
  }
}

impl PartialOrd for TableId {
  fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
    let x: u16 = (*self).into();
    let y: u16 = (*other).into();
    Some(x.cmp(&y))
  }
}

impl TableId {
  #[inline]
  pub fn zero() -> TableId {
    TableId(u16::to_le_bytes(0))
  }

  #[inline]
  pub fn succ(&self) -> TableId {
    let value = u16::from_le_bytes(self.0);
    TableId(u16::to_le_bytes(value + 1))
  }
}

#[derive(Copy, Clone, Debug, Eq, Ord, PartialEq, PartialOrd)]
#[repr(packed)]
pub struct TableColumnId(TableId, ColumnId);

impl TableColumnId {
  pub fn new(table_id: TableId, column_id: ColumnId) -> TableColumnId {
    TableColumnId(table_id, column_id)
  }
}

impl From<TableColumnId> for [u8; 4] {
  fn from(id: TableColumnId) -> Self {
    let mut bytes = [0 as u8; 4];
    bytes[0..2].copy_from_slice(&id.0.0);
    bytes[2..4].copy_from_slice(&id.1.0);
    bytes
  }
}

impl From<[u8; 4]> for TableColumnId {
  fn from(id: [u8; 4]) -> Self {
    let mut table_id = [0 as u8; 2];
    let mut column_id = [0 as u8; 2];
    table_id.copy_from_slice(&id[0..2]);
    column_id.copy_from_slice(&id[2..4]);
    TableColumnId(table_id.into(), column_id.into())
  }
}

impl From<TableColumnId> for Bytes {
  fn from(id: TableColumnId) -> Self {
    let bytes: [u8; 4] = id.into();
    Bytes::copy_from_slice(&bytes)
  }
}

impl From<Bytes> for TableColumnId {
  fn from(bytes: Bytes) -> Self {
    let bytes: [u8; 4] = (&bytes[..]).try_into().unwrap();
    bytes.into()
  }
}

#[derive(Copy, Clone, Debug, Eq, Ord, PartialEq, PartialOrd)]
#[repr(packed)]
pub struct IndexColumnId(IndexId, ColumnId);

impl IndexColumnId {
  pub fn new(index_id: IndexId, column_id: ColumnId) -> IndexColumnId {
    IndexColumnId(index_id, column_id)
  }
}

impl From<IndexColumnId> for [u8; 4] {
  fn from(id: IndexColumnId) -> Self {
    let mut bytes = [0 as u8; 4];
    bytes[0..2].copy_from_slice(&id.0.0);
    bytes[2..4].copy_from_slice(&id.1.0);
    bytes
  }
}

impl From<[u8; 4]> for IndexColumnId {
  fn from(bytes: [u8; 4]) -> Self {
    let mut index_id = [0u8; 2];
    let mut column_id = [0u8; 2];
    index_id.copy_from_slice(&bytes[0..2]);
    column_id.copy_from_slice(&bytes[2..4]);
    IndexColumnId(IndexId(index_id), ColumnId(column_id))
  }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Value {
  U8(u8),
  U16(u16),
  I32(i32),
  U32(u32),
  I64(i64),
  U64(u64),
  Date(i32),
  DateTime(i64),
  Char(Bytes),
  Type(Type),
  TableColumnId(TableColumnId),
  TableId(TableId),
}

impl From<Value> for Bytes {
  fn from(bytes: Value) -> Bytes {
    let mut vec = vec![];
    match bytes {
      Value::U8(n)  => vec.put_u8(n),
      Value::U16(n) => vec.put_u16_le(n),
      Value::I32(n) => vec.put_i32_le(n),
      Value::U32(n) => vec.put_u32_le(n),
      Value::U64(n) => vec.put_u64_le(n),
      Value::Date(n) => vec.put_i32_le(n),
      Value::DateTime(n) => vec.put_i64_le(n),
      Value::Char(bs) => {
        debug_assert!(bs.len() < 256);
        vec.put_u8(bs.len() as u8);
        vec.put(bs);
      },
      Value::TableColumnId(TableColumnId(TableId(t), ColumnId(c))) => {
        vec.put(&t[..]);
        vec.put(&c[..]);
      }
      _ => unimplemented!(),
    }
    Bytes::copy_from_slice(&vec)
  }
}

impl From<Value> for attosql::OutValue {
  fn from(value: Value) -> attosql::OutValue {
    match value {
      Value::U8(n) => attosql::OutValue { out_value: Some(attosql::out_value::OutValue::U8(n as u32)) },
      Value::U16(n) => attosql::OutValue { out_value: Some(attosql::out_value::OutValue::U16(n as u32)) },
      Value::I32(n) => attosql::OutValue { out_value: Some(attosql::out_value::OutValue::I32(n)) },
      Value::U32(n) => attosql::OutValue { out_value: Some(attosql::out_value::OutValue::U32(n)) },
      Value::I64(n) => attosql::OutValue { out_value: Some(attosql::out_value::OutValue::I64(n)) },
      Value::U64(n) => attosql::OutValue { out_value: Some(attosql::out_value::OutValue::U64(n)) },
      Value::Char(n) => attosql::OutValue { out_value: Some(attosql::out_value::OutValue::Str(String::from_utf8_lossy(&n).to_owned().to_string())) },
      _ => unimplemented!(),
    }
  }
}

impl From<Value> for Type {
  fn from(value: Value) -> Type {
    match value {
      Value::U8(_)    => Type::U8,
      Value::U16(_)   => Type::U16,
      Value::I32(_)   => Type::I32,
      Value::U32(_)   => Type::U32,
      Value::U64(_)   => Type::U64,
      Value::Char(bs) => Type::Char(bs.len()),
      _               => unimplemented!(),
    }
  }
}

impl Value {
  fn from_bytes(mut bytes: Bytes, ty: Type) -> Value {
    match ty {
      Type::U8      => Value::U8(bytes.get_u8()),
      Type::U16     => Value::U16(bytes.get_u16_le()),
      Type::I32     => Value::I32(bytes.get_i32_le()),
      Type::U32     => Value::U32(bytes.get_u32_le()),
      Type::U64     => Value::U64(bytes.get_u64_le()),
      Type::Char(n) => Value::Char(bytes.slice(1..(n as usize + 1))),
      _             => unimplemented!(),
    }
  }

  pub fn from_in_value(value: attosql::InValue, ty: Type) -> Value {
    let val = value.in_value.clone();
    match (value.in_value, ty) {
      (Some(attosql::in_value::InValue::Number(n)), Type::U8) => Value::U8(n as u8),
      (Some(attosql::in_value::InValue::Number(n)), Type::U16) => Value::U16(n as u16),
      (Some(attosql::in_value::InValue::Number(n)), Type::I32) => Value::I32(n as i32),
      (Some(attosql::in_value::InValue::Number(n)), Type::U32) => Value::U32(n as u32),
      (Some(attosql::in_value::InValue::Number(n)), Type::I64) => Value::I64(n as i64),
      (Some(attosql::in_value::InValue::Number(n)), Type::U64) => Value::U64(n as u64),
      (Some(attosql::in_value::InValue::Str(s)), Type::Char(n)) => { Value::Char(Bytes::copy_from_slice(s.as_bytes())) },
      _ => panic!("Not implemented: {:?} {:?}", val, ty),
    }
  }

  fn to_bytes(self, ty: &Type) -> Bytes {
    self.to_bytes_mut(ty).freeze()
  }

  fn to_bytes_mut(self, ty: &Type) -> BytesMut {
    let mut vec = BytesMut::new();
    match &self {
      Value::U8(n)  => { debug_assert_eq!(ty, &Type::U8); vec.put_u8(*n) },
      Value::U16(n) => { debug_assert_eq!(ty, &Type::U16); vec.put_u16_le(*n) },
      Value::I32(n) => { debug_assert_eq!(ty, &Type::I32); vec.put_i32_le(*n) },
      Value::U32(n) => { debug_assert_eq!(ty, &Type::U32); vec.put_u32_le(*n) },
      Value::U64(n) => { debug_assert_eq!(ty, &Type::U64); vec.put_u64_le(*n) },
      Value::Date(n) => { debug_assert_eq!(ty, &Type::Date); vec.put_i32_le(*n) },
      Value::DateTime(n) => { debug_assert_eq!(ty, &Type::DateTime); vec.put_i64_le(*n) },
      Value::Char(bs) => {
        if let Type::Char(n) = ty {
          debug_assert!(bs.len() <= *n);
          vec.put_u8(*n as u8);
          vec.put(bs.clone());
          if bs.len() < *n {
            for _ in 0..(n - bs.len()) {
              vec.put_u8(0)
            }
          }
        } else {
          panic!("Type mismatch, {:?} {:?}", self, ty);
        }
      },
      Value::TableColumnId(TableColumnId(TableId(t), ColumnId(c))) => {
        vec.put(&t[..]);
        vec.put(&c[..]);
      },
      _ => unimplemented!(),
    }
    vec
  }

  fn default(ty: Type) -> Value {
    match ty {
      Type::U8      => Value::U8(0),
      Type::U16     => Value::U16(0),
      Type::I32     => Value::I32(0),
      Type::U32     => Value::U32(0),
      Type::U64     => Value::U64(0),
      Type::Char(n) => {
        let mut value = BytesMut::with_capacity(n);
        unsafe {
          value.set_len(n);
          value.fill(0);
        }
        Value::Char(value.freeze())
      },
      _ => unimplemented!(),
    }
  }
}

pub struct RowScanner<'a> {
  bytes: Bytes,
  pos: usize,
  types: &'a [Type],
}

impl<'a> RowScanner<'a> {
  fn new(bytes: Bytes, types: &'a [Type]) -> RowScanner<'a> {
    RowScanner { bytes, pos: 0, types }
  }
}

impl<'a> Iterator for RowScanner<'a> {
  type Item = Value;

  fn next(&mut self) -> Option<Self::Item> {
    if self.pos < self.types.len() {
      let ty = self.types[self.pos];
      let bytes = self.bytes.split_to(ty.size());
      let value = to_value(bytes, ty);
      self.pos += 1;
      Some(value)
    } else {
      None
    }
  }
}

pub struct Selector<'i> {
  _types: Vec<Vec<Type>>,
  _offsets: Vec<Vec<usize>>,
  _columns: Vec<(usize, usize)>,
  _iter: Box<dyn Iterator<Item=Vec<Bytes>> + 'i>,
}

// impl<'i> Selector<'i> {
//   pub fn new(types: Vec<Vec<Type>>, columns: Vec<(usize, usize)>, iter: Box<dyn Iterator<Item=Vec<Bytes>> + 'i>) -> Selector<'i> {
//     debug_assert!(Self::all_columns_are_in_bounds(columns, types));
//     let mut offsets = Vec::new();
//     for ts in types {
//       let mut ofs = vec![0; ts.len() + 1];
//       for i in 1..types.len() {
//         ofs[i] = ofs[i - 1] + ts[i].size();
//       }
//       offsets.push(ofs);
//     }
//     Selector { types, offsets, columns, iter }
//   }

//   fn all_columns_are_in_bounds(columns: Vec<(usize, usize)>, types: Vec<Vec<Type>>) -> bool {
//     columns.iter().find(|(x, y)| x < &types.len() && y < &types[*x].len()).is_none()
//   }
// }

// impl<'i> Iterator for Selector<'i> {
//   type Item = Vec<Bytes>;

//   fn next(&mut self) -> Option<Self::Item> {
//     match self.iter.next() {
//       None => None,
//       Some(v) => {
//         let mut result = Vec::new();
//         todo!()
//       }
//     }
//     todo!()
//   }
// }

// fn select_from_bytes(bytes: Bytes, types: Vec<Type>, pos: usize) -> Bytes {
//   debug_assert_eq!(bytes.len(), types.iter().map(|t| t.size()).sum());
// }

#[derive(Debug)]
pub enum Term {
  Value(Value),
  Column(TableColumnId),
}

#[derive(Debug)]
pub enum Cond {
  Equal(Term, Term),
  LessThan(Term, Term),
  LessEqual(Term, Term),
  GreaterThan(Term, Term),
  GreaterEqual(Term, Term),
  Range(Term, Term, Term),
  RangeIncl(Term, Term, Term),
  Any,
}

#[derive(Debug)]
pub enum BExp {
  And(Box<BExp>, Box<BExp>),
  Or(Box<BExp>, Box<BExp>),
  Not(Box<BExp>),
  Cond(Box<Cond>),
}

#[derive(Debug)]
pub enum Query {
  FullScan {
    table_id: TableId,
  },
  RangeScan{
    beg: Value,
    end: Value,
    table_id: TableId,
  },
  IndexRangeScan {
    beg: Value,
    end: Value,
    table_id: TableId,
    column: usize,
  },
  Filter {
    source: Box<Query>,
    filter: Box<BExp>,
  },
  MergeJoin {
    left: Box<Query>,
    right: Box<Query>,
  },
  HashJoin {
    left: Box<Query>,
    right: Box<Query>,
    left_slot: usize,
    left_column: usize,
    right_slot: usize,
    right_column: usize,
  },
  GroupBy {
    source: Box<Query>,
    by: usize,
  },
  Select {
    source: Box<Query>,
    columns: Vec<(usize, usize)>,
  },
  FetchByPK {
    table_id: TableId,
    key: Value,
  },
}

#[derive(Debug)]
pub struct Table {
  // TODO Hide this field.
  pub types: Vec<Type>,
  offsets: Vec<usize>,
}

pub fn to_value(mut bytes: Bytes, ty: Type) -> Value {
  match ty {
    Type::U8  => Value::U8(bytes.get_u8()),
    Type::U16 => Value::U16(bytes.get_u16_le()),
    Type::I32 => Value::I32(bytes.get_i32_le()),
    Type::U32 => Value::U32(bytes.get_u32_le()),
    Type::U64 => Value::U64(bytes.get_u64_le()),
    Type::Char(n) => {
      debug_assert!(n < 256);
      let len = u8::from_le_bytes((&bytes.split_to(1)[..]).try_into().unwrap()) as usize;
      Value::Char(bytes.split_to(len))
    },
    Type::Type          => Value::Type(bytes.split_to(2).into()),
    Type::TableColumnId => Value::TableColumnId(bytes.split_to(4).into()),
    Type::TableId       => Value::TableId(bytes.get_u16_le().into()),
    _ => unimplemented!(),
  }
}

impl Table {
  pub fn new(types: Vec<Type>) -> Table {
    debug_assert!(1 < types.len());
    let mut offsets = vec![0; types.len()];
    for i in 1..types.len() {
      offsets[i] = offsets[i - 1] + types[i].size();
    }
    Table { types, offsets }
  }

  pub fn key_size(&self) -> usize {
    self.types[0].size()
  }

  pub fn value_size(&self) -> usize {
    self.offsets[self.offsets.len() - 1]
  }

  pub fn get_type(&self) -> &[Type] {
    &self.types
  }

  pub fn len(&self) -> usize {
    self.types.len()
  }

  pub fn get_value(&self, values: Bytes, pos: usize) -> Value {
    debug_assert!(0 < pos);
    debug_assert!(pos < self.types.len());
    let ty = self.types[pos];
    let bs = values.slice(self.offsets[pos - 1]..self.offsets[pos]);
    to_value(bs, ty)
  }

  pub fn get_key_bytes(&self, value: Value) -> Bytes {
    value.to_bytes(&self.types[0])
  }

  pub fn get_value_bytes(&self, value: Vec<Value>) -> Bytes {
    debug_assert_eq!(value.len() + 1, self.types.len());
    let size = self.value_size();
    let mut bytes = BytesMut::with_capacity(size);
    for i in 0..value.len() {
      let bs = &value[i].clone().to_bytes_mut(&self.types[i + 1]);
      bytes.put(&bs[..]);
    }
    bytes.freeze()
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn empty_row_scanner() {
    let bytes = Bytes::new();
    assert_eq!(0, RowScanner::new(bytes, &[]).count());
  }

  #[test]
  fn row_scanner_1_column() {
    let bytes = Bytes::copy_from_slice(&[4, 3, 2, 1]);
    let types = &[Type::U32];
    assert_eq!(vec![Value::U32(0x01020304)], RowScanner::new(bytes, types).collect::<Vec<_>>());
  }

  #[test]
  fn row_scanner_columns() {
    let bytes = Bytes::copy_from_slice(&[10, 1, 0, 2, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 5, 72, 101, 108, 108, 111 ]);
    let types = &[Type::U8, Type::U16, Type::I32, Type::U64, Type::Char(5)];
    let expected = vec![Value::U8(10), Value::U16(1), Value::I32(2), Value::U64(3), Value::Char(Bytes::from_static("Hello".as_bytes()))];
    assert_eq!(expected, RowScanner::new(bytes, types).collect::<Vec<_>>());
  }

  #[test]
  fn table_get_value() {
    let types = vec![Type::U8, Type::U16, Type::I32, Type::U64, Type::Char(5)];
    let table = Table::new(types);
    let bytes = Bytes::copy_from_slice(&[1, 0, 2, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 5, 72, 101, 108, 108, 111 ]);
    let expected = vec![Value::U16(1), Value::I32(2), Value::U64(3), Value::Char(Bytes::from_static("Hello".as_bytes()))];
    for i in 1..expected.len() {
      assert_eq!(expected[i - 1], table.get_value(bytes.clone(), i))
    }
  }
}