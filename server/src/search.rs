pub fn lower_bound(arr: &[u8], size: usize, len: usize, key: &[u8], lt: impl Fn(&[u8], &[u8]) -> bool) -> usize
{
  debug_assert_eq!(key.len(), size);
  debug_assert!(size * len == arr.len());
  let mut lo = 0;                 
  let mut hi = len;
  while hi - lo > 0 {                                             
    let p = (hi + lo) / 2;                                                                  
    let v = &arr[(p * size)..((p + 1) * size)];
    if lt(v, key) {                                               
      lo = p + 1;                                                                           
    } else {              
      hi = p;                     
    }                        
  }                
  return lo;    
}

pub fn upper_bound(arr: &[u8], size: usize, len: usize, key: &[u8], lt: impl Fn(&[u8], &[u8]) -> bool) -> usize
{
  debug_assert_eq!(key.len(), size);
  debug_assert!(size * len == arr.len());
  let mut lo = 0;                                                     
  let mut hi = len;
  while hi - lo > 0 {
    let p = (hi + lo) / 2;                                    
    let v = &arr[(p * size)..((p + 1) * size)];
    if lt(key, v) {
      hi = p;        
    } else {              
      lo = p + 1;                 
    }                        
  }                                                                      
  return lo;
}

#[cfg(test)]
mod tests {
  use super::*;
  use bytes::{Buf, BufMut};

  fn lt_i32(mut lhs: &[u8], mut rhs: &[u8]) -> bool {
    lhs.get_i32_le() < rhs.get_i32_le()
  }

  #[test]                   
  fn test_lower_bound_1() {
    let arr = [];
    let key = u32::to_le_bytes(1);
    let i = lower_bound(&arr, 4, 0, &key, lt_i32);
    assert_eq!(i, 0);
  }

  #[test]
  fn test_lower_bound_2() {
    let arr = u32::to_le_bytes(1);
    let key = u32::to_le_bytes(0);
    let i = lower_bound(&arr, 4, 1, &key, lt_i32);
    assert_eq!(i, 0);
  }

  #[test]
  fn test_lower_bound_3() {
    let mut arr = vec![];
    let mut key = vec![];
    arr.put_i32_le(1);
    key.put_i32_le(1);
    let i = lower_bound(&arr, 4, 1, &key, lt_i32);
    assert_eq!(i, 0);
  }

  #[test]
  fn test_lower_bound_4() {
    let mut arr = vec![];
    let mut key = vec![];
    arr.put_i32_le(1);
    key.put_i32_le(2);
    let i = lower_bound(&arr, 4, 1, &key, lt_i32);
    assert_eq!(i, 1);
  }

  #[test]
  fn test_lower_bound_5() {
    let mut arr = vec![];
    let mut key = vec![];
    (1..=3).for_each(|x| arr.put_i32_le(x));
    key.put_i32_le(0);
    let i = lower_bound(&arr, 4, 3, &key, lt_i32);
    assert_eq!(i, 0);
  }

  #[test]
  fn test_lower_bound_6() {
    let mut arr = vec![];
    let mut key = vec![];
    (0..2).for_each(|x| arr.put_i32_le(x));
    key.put_i32_le(0);
    let i = lower_bound(&arr, 4, 2, &key, lt_i32);
    assert_eq!(i, 0);
  }

  #[test]
  fn test_lower_bound_7() {
    let mut arr = vec![];
    let mut key = vec![];
    (0..2).for_each(|x| arr.put_i32_le(x));
    key.put_i32_le(1);
    let i = lower_bound(&arr, 4, 2, &key, lt_i32);
    assert_eq!(i, 1);
  }

  #[test]
  fn test_lower_bound_8() { 
    let mut arr = vec![];
    let mut key = vec![];
    (0..2).for_each(|x| arr.put_i32_le(x));
    key.put_i32_le(2);
    let i = lower_bound(&arr, 4, 2, &key, lt_i32);
    assert_eq!(i, 2);
  }

  #[test]
  fn test_lower_bound_9() {
    let mut arr = vec![];
    let mut key = vec![];
    [1; 2].iter().for_each(|&x| arr.put_i32_le(x));
    key.put_i32_le(0);
    let i = lower_bound(&arr, 4, 2, &key, lt_i32);
    assert_eq!(i, 0);
  }

  #[test]
  fn test_lower_bound_10() {
    let mut arr = vec![];
    let mut key = vec![];
    [1; 2].iter().for_each(|&x| arr.put_i32_le(x));
    key.put_i32_le(1);
    let i = lower_bound(&arr, 4, 2, &key, lt_i32);
    assert_eq!(i, 0);
  }

  #[test]
  fn test_lower_bound_11() {
    let mut arr = vec![];
    let mut key = vec![];
    [1; 2].iter().for_each(|&x| arr.put_i32_le(x));
    key.put_i32_le(2);
    let i = lower_bound(&arr, 4, 2, &key, lt_i32);
    assert_eq!(i, 2);
  }

  #[test]
  fn test_lower_bound_12() {
    let mut arr = vec![];
    let mut key = vec![];
    (0..3).for_each(|x| arr.put_i32_le(x));
    key.put_i32_le(-1);
    let i = lower_bound(&arr, 4, 3, &key, lt_i32);
    assert_eq!(i, 0);
  }

  #[test]
  fn test_lower_bound_13() {
    let mut arr = vec![];
    let mut key = vec![];
    (0..3).for_each(|x| arr.put_i32_le(x));
    key.put_i32_le(0);
    let i = lower_bound(&arr, 4, 3, &key, lt_i32);
    assert_eq!(i, 0);
  }

  #[test]
  fn test_lower_bound_14() {
    let mut arr = vec![];
    let mut key = vec![];
    (0..3).for_each(|x| arr.put_i32_le(x));
    key.put_i32_le(1);
    let i = lower_bound(&arr, 4, 3, &key, lt_i32);
    assert_eq!(i, 1);
  }

  #[test]
  fn test_lower_bound_15() {
    let mut arr = vec![];
    let mut key = vec![];
    (0..3).for_each(|x| arr.put_i32_le(x));
    key.put_i32_le(2);
    let i = lower_bound(&arr, 4, 3, &key, lt_i32);
    assert_eq!(i, 2);
  }

  #[test]
  fn test_lower_bound_16() {
    let mut arr = vec![];
    let mut key = vec![];
    (0..3).for_each(|x| arr.put_i32_le(x));
    key.put_i32_le(3);
    let i = lower_bound(&arr, 4, 3, &key, lt_i32);
    assert_eq!(i, 3);
  }

  #[test]
  fn test_lower_bound_17() {
    let mut arr = vec![];
    let mut key = vec![];
    [0, 1, 1, 2].iter().for_each(|&x| arr.put_i32_le(x));
    key.put_i32_le(1);
    let i = lower_bound(&arr, 4, 4, &key, lt_i32);
    assert_eq!(i, 1);
  }

  #[test]
  fn test_lower_bound_19() {
    let mut arr = vec![];
    let mut key = vec![];
    [0, 1, 1, 2].iter().for_each(|&x| arr.put_i32_le(x));
    key.put_i32_le(2);
    let i = lower_bound(&arr, 4, 4, &key, lt_i32);
    assert_eq!(i, 3);
  }

  #[test]
  fn test_lower_bound_20() {
    let mut arr = vec![];
    let mut key = vec![];
    [0, 1, 1, 2].iter().for_each(|&x| arr.put_i32_le(x));
    key.put_i32_le(3);
    let i = lower_bound(&arr, 4, 4, &key, lt_i32);
    assert_eq!(i, 4);
  }

  #[test]
  fn test_lower_bound_21() {
    let mut arr = vec![];
    let mut key = vec![];
    [0, 1, 1, 2].iter().for_each(|&x| arr.put_i32_le(x));
    key.put_i32_le(0);
    let i = lower_bound(&arr, 4, 4, &key, lt_i32);
    assert_eq!(i, 0);
  }

  #[test]
  fn test_lower_bound_22() {
    let mut arr = vec![];
    let mut key = vec![];
    [0, 0, 1, 1, 2].iter().for_each(|&x| arr.put_i32_le(x));
    key.put_i32_le(1);
    let i = lower_bound(&arr, 4, 5, &key, lt_i32);
    assert_eq!(i, 2);
  }

  #[test]
  fn test_lower_bound_23() {
    let mut arr = vec![];
    let mut key = vec![];
    [0, 0, 1, 1, 2].iter().for_each(|&x| arr.put_i32_le(x));
    key.put_i32_le(4);
    let i = lower_bound(&arr, 4, 5, &key, lt_i32);
    assert_eq!(i, 5);
  }


  #[test]
  fn test_lower_bound_24() {
    let mut arr = vec![];
    let mut key = vec![];
    [0, 0, 1, 2, 2].iter().for_each(|&x| arr.put_i32_le(x));
    key.put_i32_le(2);
    let i = lower_bound(&arr, 4, 5, &key, lt_i32);
    assert_eq!(i, 3);
  }

  #[test]
  fn test_lower_bound_25() {
    let mut arr = vec![];
    let mut key = vec![];
    [0, 0, 1, 2, 2].iter().for_each(|&x| arr.put_i32_le(x));
    key.put_i32_le(4);
    let i = lower_bound(&arr, 4, 5, &key, lt_i32);
    assert_eq!(i, 5);
  }

  #[test]
  fn test_upper_bound_1() {
    let arr = vec![];
    let mut key = vec![];
    key.put_i32_le(1);
    let i = upper_bound(&arr, 4, 0, &key, lt_i32);
    assert_eq!(i, 0);
  }

  #[test]
  fn test_upper_bound_2() {
    let mut arr = vec![];
    let mut key = vec![];
    arr.put_i32_le(1);
    key.put_i32_le(0);
    let i = upper_bound(&arr, 4, 1, &key, lt_i32);
    assert_eq!(i, 0); 
  }                    
              
  #[test]                                                                                                       
  fn test_upper_bound_3() {
    let mut arr = vec![];
    let mut key = vec![];
    arr.put_i32_le(1);
    key.put_i32_le(1);
    let i = upper_bound(&arr, 4, 1, &key, lt_i32);
    assert_eq!(i, 1);
  }

  #[test]
  fn test_upper_bound_4() {
    let mut arr = vec![];
    let mut key = vec![];
    arr.put_i32_le(1);
    key.put_i32_le(2);
    let i = upper_bound(&arr, 4, 1, &key, lt_i32);
    assert_eq!(i, 1);
  }

  #[test]
  fn test_upper_bound_5() {
    let mut arr = vec![];
    let mut key = vec![];
    (0..2).for_each(|x| arr.put_i32_le(x));
    key.put_i32_le(-1);
    let i = upper_bound(&arr, 4, 2, &key, lt_i32);
    assert_eq!(i, 0);
  }

  #[test]
  fn test_upper_bound_6() {
    let mut arr = vec![];
    let mut key = vec![];
    (0..2).for_each(|x| arr.put_i32_le(x));
    key.put_i32_le(0);
    let i = upper_bound(&arr, 4, 2, &key, lt_i32);
    assert_eq!(i, 1);
  }

  #[test]
  fn test_upper_bound_7() {
    let mut arr = vec![];
    let mut key = vec![];
    (0..2).for_each(|x| arr.put_i32_le(x));
    key.put_i32_le(1);
    let i = upper_bound(&arr, 4, 2, &key, lt_i32);
    assert_eq!(i, 2);                                   
  }   
                  
  #[test]             
  fn test_upper_bound_8() {  
    let mut arr = vec![];
    let mut key = vec![];
    (0..2).for_each(|x| arr.put_i32_le(x));
    key.put_i32_le(2);
    let i = upper_bound(&arr, 4, 2, &key, lt_i32);
    assert_eq!(i, 2);
  }

  #[test]                            
  fn test_upper_bound_9() {     
    let mut arr = vec![];
    let mut key = vec![];
    [1; 2].iter().for_each(|&x| arr.put_i32_le(x));
    key.put_i32_le(0);
    let i = upper_bound(&arr, 4, 2, &key, lt_i32);
    assert_eq!(i, 0);       
  }             
              
  #[test]
  fn test_upper_bound_10() {
    let mut arr = vec![];
    let mut key = vec![];
    [1; 2].iter().for_each(|&x| arr.put_i32_le(x));
    key.put_i32_le(1);
    let i = upper_bound(&arr, 4, 2, &key, lt_i32);
    assert_eq!(i, 2);
  }

  #[test]
  fn test_upper_bound_11() {
    let mut arr = vec![];
    let mut key = vec![];
    [1; 2].iter().for_each(|&x| arr.put_i32_le(x));
    key.put_i32_le(2);
    let i = upper_bound(&arr, 4, 2, &key, lt_i32);
    assert_eq!(i, 2);
  }

  #[test]
  fn test_upper_bound_12() {
    let mut arr = vec![];
    let mut key = vec![];
    (0..3).for_each(|x| arr.put_i32_le(x));
    key.put_i32_le(-1);
    let i = upper_bound(&arr, 4, 3, &key, lt_i32);
    assert_eq!(i, 0);
  }

  #[test]
  fn test_upper_bound_13() {
    let mut arr = vec![];
    let mut key = vec![];
    (0..3).for_each(|x| arr.put_i32_le(x));
    key.put_i32_le(0);
    let i = upper_bound(&arr, 4, 3, &key, lt_i32);
    assert_eq!(i, 1);
  }

  #[test]
  fn test_upper_bound_14() {
    let mut arr = vec![];
    let mut key = vec![];
    (0..3).for_each(|x| arr.put_i32_le(x));
    key.put_i32_le(1);
    let i = upper_bound(&arr, 4, 3, &key, lt_i32);
    assert_eq!(i, 2);
  }

  #[test]
  fn test_upper_bound_15() {
    let mut arr = vec![];
    let mut key = vec![];
    (0..3).for_each(|x| arr.put_i32_le(x));
    key.put_i32_le(2);
    let i = upper_bound(&arr, 4, 3, &key, lt_i32);
    assert_eq!(i, 3);
  }

  #[test]
  fn test_upper_bound_16() {
    let mut arr = vec![];
    let mut key = vec![];
    (0..3).for_each(|x| arr.put_i32_le(x));
    key.put_i32_le(3);
    let i = upper_bound(&arr, 4, 3, &key, lt_i32);
    assert_eq!(i, 3);
  }

  #[test]
  fn test_upper_bound_17() {
    let mut arr = vec![];
    let mut key = vec![];
    [0, 1, 1, 2].iter().for_each(|&x| arr.put_i32_le(x));
    key.put_i32_le(3);
    let i = upper_bound(&arr, 4, 4, &key, lt_i32);
    assert_eq!(i, 4);
  }

  #[test]
  fn test_upper_bound_19() {
    let mut arr = vec![];
    let mut key = vec![];
    [0, 1, 1, 2].iter().for_each(|&x| arr.put_i32_le(x));
    key.put_i32_le(2);
    let i = upper_bound(&arr, 4, 4, &key, lt_i32);
    assert_eq!(i, 4);
  }

  #[test]
  fn test_upper_bound_20() {
    let mut arr = vec![];
    let mut key = vec![];
    [0, 1, 1, 2].iter().for_each(|&x| arr.put_i32_le(x));
    key.put_i32_le(3);
    let i = upper_bound(&arr, 4, 4, &key, lt_i32);
    assert_eq!(i, 4);
  }

  #[test]
  fn test_upper_bound_21() {
    let mut arr = vec![];
    let mut key = vec![];
    [0, 0, 1, 1, 2].iter().for_each(|&x| arr.put_i32_le(x));
    key.put_i32_le(0);
    let i = upper_bound(&arr, 4, 5, &key, lt_i32);
    assert_eq!(i, 2);
  }

  #[test]
  fn test_upper_bound_22() {
    let mut arr = vec![];
    let mut key = vec![];
    [0, 0, 1, 1, 2].iter().for_each(|&x| arr.put_i32_le(x));
    key.put_i32_le(1);
    let i = upper_bound(&arr, 4, 5, &key, lt_i32);
    assert_eq!(i, 4);
  }

  #[test]
  fn test_upper_bound_23() {
    let mut arr = vec![];
    let mut key = vec![];
    [0, 0, 1, 1, 2].iter().for_each(|&x| arr.put_i32_le(x));
    key.put_i32_le(4);
    let i = upper_bound(&arr, 4, 5, &key, lt_i32);
    assert_eq!(i, 5);
  }


  #[test]
  fn test_upper_bound_24() {
    let mut arr = vec![];
    let mut key = vec![];
    [0, 0, 1, 2, 2].iter().for_each(|&x| arr.put_i32_le(x));
    key.put_i32_le(2);
    let i = upper_bound(&arr, 4, 5, &key, lt_i32);
    assert_eq!(i, 5);
  }

  #[test]
  fn test_upper_bound_25() {
    let mut arr = vec![];
    let mut key = vec![];
    [0, 0, 1, 2, 2].iter().for_each(|&x| arr.put_i32_le(x));
    key.put_i32_le(4);
    let i = upper_bound(&arr, 4, 5, &key, lt_i32);
    assert_eq!(i, 5);
  }
}