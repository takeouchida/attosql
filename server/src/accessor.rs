use std::cell::Cell;
use std::fs::{File, OpenOptions};
use std::io::{prelude::*, SeekFrom, Result};
use std::path::Path;

use bytes::BytesMut;

use attosql_commons::consts::PAGE_SIZE;
use attosql_commons::conv::deref_helper;
use crate::page::{BranchHeader, LeafHeader, Header, NodeKind, Node, NodeMut, Metadata, MetadataMut, PageId, PageVendor};

pub struct FileAccessor {
  file: Cell<File>,
  next_id: u64,
}

impl FileAccessor {
  pub fn new<P>(path: P) -> Result<FileAccessor> where P: AsRef<Path> {
    let file = OpenOptions::new().read(true).write(true).create(true).open(path)?;
    let heap_size = file.metadata()?.len();
    let file = Cell::new(file);
    let next_id = heap_size / PAGE_SIZE as u64;
    Ok(Self{file, next_id})
  }

  fn fetch_bytes_mut(&self, id: PageId) -> Result<BytesMut> {
    let pos: u64 = id.into();
    let offset = PAGE_SIZE as u64 * pos;
    let mut bytes = BytesMut::with_capacity(PAGE_SIZE);
    unsafe {
      bytes.set_len(PAGE_SIZE);
      self.file_mut().seek(SeekFrom::Start(offset))?;
      self.file_mut().read_exact(&mut bytes[..])?;
    }
    Ok(bytes)
  }

  fn save_slice(&mut self, bytes: &mut [u8]) -> Result<()> {
    unsafe {
      self.file_mut().seek(SeekFrom::Start(PAGE_SIZE as u64 * self.next_id))?;
      self.file_mut().write_all(bytes)
    }
  }

  fn save_root_slice(&mut self, bytes: &mut [u8]) -> Result<()> {
    unsafe {
      self.file_mut().seek(SeekFrom::Start(0))?;
      self.file_mut().write_all(bytes)
    }
  }

  fn save_bytes(&mut self, mut bytes: BytesMut) -> Result<()>  {
    self.save_slice(&mut bytes[..])
  }

  #[inline]
  unsafe fn file_mut(&self) -> &mut File {
    &mut *(self.file.as_ptr())
  }

  pub fn sync(&mut self) -> Result<()> {
    unsafe {
      self.file_mut().flush()?;
      self.file_mut().sync_all()
    }
  }
}

impl PageVendor for FileAccessor {
  fn fetch<T, N>(&self, id: PageId) -> Result<N>
    where
      T: Header,
      N: Node<T>
  {
    Ok(self.fetch_bytes_mut(id)?.freeze().into())
  }

  fn save<T, N>(&mut self, node: N) -> Result<PageId>
    where
      T: Header,
      N: NodeMut<T>
  {
    self.save_slice(&mut node.into())?;
    let id = self.next_id;
    self.next_id += 1;
    Ok(id.into())
  }

  fn copy<T, N>(&self, id: PageId) -> Result<N>
    where
      T: Header,
      N: NodeMut<T>
  {
    Ok(self.fetch_bytes_mut(id)?.into())
  }

  fn fetch_header(&self, id: PageId) -> Result<&dyn Header> {
    let bytes = self.fetch_bytes_mut(id)?;
    match &bytes[0].into() {
      NodeKind::Branch => unsafe { Ok(deref_helper(&bytes[..]) as &BranchHeader) },
      NodeKind::Leaf   => unsafe { Ok(deref_helper(&bytes[..]) as &LeafHeader) },
      _                => panic!("FileAccessor::fetch_header(), unsupported nodekind: {}", &bytes[0]),
    }
  }

  fn fetch_key_size(&self, id: PageId) -> Result<usize> {
    let header = self.fetch_header(id)?;
    Ok(header.key_size())
  }

  fn fetch_len(&self, id: PageId) -> Result<usize> {
    let header = self.fetch_header(id)?;
    Ok(header.len())
  }

  fn is_full(&self, id: PageId) -> Result<bool> {
    let header = self.fetch_header(id)?;
    Ok(header.len() == header.capacity())
  }

  fn is_branch(&self, id: PageId) -> Result<bool> {
    let bytes = self.fetch_bytes_mut(id)?;
    let kind: &NodeKind = &bytes[0].into();
    Ok(kind == &NodeKind::Branch)
  }

  fn is_leaf(&self, id: PageId) -> Result<bool> {
    let bytes = self.fetch_bytes_mut(id)?;
    let kind: &NodeKind = &bytes[0].into();
    Ok(kind == &NodeKind::Leaf)
  }

  fn fetch_meta(&self) -> Result<Metadata> {
    Ok(self.fetch_bytes_mut(0u64.into())?.freeze().into())
  }

  fn copy_meta(&self) -> Result<MetadataMut> {
    Ok(self.fetch_bytes_mut(0u64.into())?.into())
  }

  fn save_meta(&mut self, meta: MetadataMut) -> Result<()> {
    let mut bytes: BytesMut = meta.into();
    self.save_root_slice(&mut bytes[..])
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use tempfile::NamedTempFile;
  use crate::page::{BasicNode, Leaf, LeafMut};

  #[test]
  fn accessor_empty_pages() {
    // read and write 2 pages
    let (_, path) = NamedTempFile::new().unwrap().into_parts();
    let mut accessor = FileAccessor::new(path).unwrap();
    let p1: LeafMut = accessor.new_page(1, 2);
    let p2: LeafMut = accessor.new_page(4, 8);

    let id1 = accessor.save(p1.clone()).unwrap();
    let id2 = accessor.save(p2.clone()).unwrap();
    let p3: Leaf = accessor.fetch(id1).unwrap();

    assert_eq!(p1.header(), p3.header());

    let p4: Leaf = accessor.fetch(id2).unwrap();

    assert_eq!(p2.header(), p4.header());

    let p5: LeafMut = accessor.copy(id1).unwrap();

    assert_eq!(p1.header(), p5.header());

    let p6: LeafMut = accessor.copy(id2).unwrap();

    assert_eq!(p2.header(), p6.header());
  }

  #[test]
  fn accessor_read_and_write_page() -> Result<()> {
    // read and write 1 page
    let (_, path) = NamedTempFile::new()?.into_parts();
    let mut accessor = FileAccessor::new(path)?;
    let mut p1: LeafMut = accessor.new_page(4, 8);

    let k1 = u32::to_le_bytes(1);
    let k2 = u32::to_le_bytes(2);
    let k3 = u32::to_le_bytes(3);
    let k4 = u32::to_le_bytes(4);
    let v1 = u64::to_le_bytes(41);
    let v2 = u64::to_le_bytes(42);
    let v3 = u64::to_le_bytes(43);
    let v4 = u64::to_le_bytes(44);

    p1.insert(&k1, &v1, 0);
    p1.insert(&k2, &v2, 1);

    let id1 = accessor.save(p1.clone())?;
    let p2: Leaf = accessor.fetch(id1)?;

    assert_eq!(p1.header(), p2.header());
    assert_eq!(k1, &p2.key(0)[..]);
    assert_eq!(k2, &p2.key(1)[..]);
    assert_eq!(v1, &p2.value(0)[..]);
    assert_eq!(v2, &p2.value(1)[..]);

    let mut node: LeafMut = accessor.copy(id1)?;
    node.insert(&k3, &v3, 2);
    node.insert(&k4, &v4, 3);
    let id2 = accessor.save(node)?;
    let node: Leaf = accessor.fetch(id2)?;

    assert_eq!(k1, &node.key(0)[..]);
    assert_eq!(k2, &node.key(1)[..]);
    assert_eq!(k3, &node.key(2)[..]);
    assert_eq!(k4, &node.key(3)[..]);
    assert_eq!(v1, &node.value(0)[..]);
    assert_eq!(v2, &node.value(1)[..]);
    assert_eq!(v3, &node.value(2)[..]);
    assert_eq!(v4, &node.value(3)[..]);

    let node: Leaf = accessor.fetch(id1)?;

    assert_eq!(k1, &node.key(0)[..]);
    assert_eq!(k2, &node.key(1)[..]);
    Ok(())
  }
}