use std::borrow::{Borrow, BorrowMut};
use std::convert::{From, TryInto};
use std::fmt::Debug;
use std::io::Result;
use std::mem::size_of;
use std::ops::Deref;

use bytes::{Bytes, BytesMut};

use attosql_commons::consts::PAGE_SIZE;
use attosql_commons::conv::{deref_helper, deref_mut_helper, cast_to_byte_slice, cast_to_byte_slice_mut};
use crate::search::{lower_bound, upper_bound};

#[derive(Copy, Clone, Eq, PartialEq, Debug, Ord, PartialOrd)]
#[repr(packed)]
pub struct PageId([u8; 6]);

impl From<[u8; 6]> for PageId  { #[inline] fn from(bytes: [u8; 6])-> PageId  { PageId(bytes) } }
impl From<PageId>  for [u8; 6] { #[inline] fn from(id: PageId)    -> [u8; 6] { id.0 } }

impl From<u64> for PageId {
  #[inline]
  fn from(id: u64) -> PageId {
    let mut value = [0u8; 6];
    unsafe { value.copy_from_slice(&cast_to_byte_slice(&id)[..6]) }
    PageId(value)
  }
}

impl From<usize> for PageId {
  #[inline]
  fn from(size: usize) -> PageId {
    let mut value = [0u8; 6];
    unsafe { value.copy_from_slice(&cast_to_byte_slice(&size)[..6]) }
    PageId(value)
  }
}

impl From<PageId> for u64 {
  #[inline]
  fn from(id: PageId) -> u64 {
    let mut value = 0u64;
    unsafe { cast_to_byte_slice_mut(&mut value)[..6].copy_from_slice(&id.0) }
    value
  }
}

impl From<PageId> for usize {
  #[inline]
  fn from(id: PageId) -> usize {
    let mut value = 0usize;
    unsafe { cast_to_byte_slice_mut(&mut value)[..6].copy_from_slice(&id.0) }
    value
  }
}

impl From<Bytes> for PageId {
  #[inline]
  fn from(bytes: Bytes) -> PageId {
    debug_assert!(6 <= bytes.len());
    let mut value = [0u8; 6];
    value.copy_from_slice(&bytes[0..6]);
    PageId(value)
  }
}

impl From<PageId> for Bytes {
  #[inline]
  fn from(id: PageId) -> Bytes {
    Bytes::copy_from_slice(&id.0)
  }
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub enum NodeKind {
  Unknown = 0,
  Branch = 1,
  Leaf = 2,
}

impl From<NodeKind> for u8 {
  #[inline]
  fn from(kind: NodeKind) -> u8 {
    kind as u8
  }
}

impl From<u8> for NodeKind {
  #[inline]
  fn from(byte: u8) -> NodeKind {
    match byte {
      0 => NodeKind::Unknown,
      1 => NodeKind::Branch,
      2 => NodeKind::Leaf,
      _ => panic!("Undefined NodeKind {}", byte),
    }
  }
}

pub trait Header {
  fn kind(&self) -> NodeKind;
  fn key_size(&self) -> usize;
  fn value_size(&self) -> usize;
  fn len(&self) -> usize;
  fn capacity(&self) -> usize;
  fn value_capacity(&self) -> usize;
  fn kind_const(&self) -> NodeKind;
  fn set_kind(&mut self, kind: u8);
  fn set_key_size(&mut self, size: usize);
  fn set_value_size(&mut self, size: usize);
  fn set_len(&mut self, len: usize);
  fn set_capacity(&mut self, capacity: usize);

  #[inline] fn is_full(&self)   -> bool { self.len() == self.capacity() }
  #[inline] fn is_empty(&self)  -> bool { self.len() == 0 }
  #[inline] fn is_leaf(&self)   -> bool { self.kind() == NodeKind::Leaf }
  #[inline] fn is_branch(&self) -> bool { self.kind() == NodeKind::Branch }
}

#[derive(Debug, PartialEq)]
#[repr(C)]
pub struct BranchHeader {
  kind: u8,
  size: u8,
  // represents the count of keys, The count of values is count + 1.
  len: u16,
  // represents the capacity of keys The capacity of values is capacity + 1.
  capacity: u16,
  _reserved: [u8; 2],
}

impl Header for BranchHeader {
  #[inline] fn kind(&self)           -> NodeKind { self.kind.into() }
  #[inline] fn key_size(&self)       -> usize    { self.size as usize }
  #[inline] fn value_size(&self)     -> usize    { size_of::<PageId>() }
  #[inline] fn len(&self)            -> usize    { self.len as usize }
  #[inline] fn capacity(&self)       -> usize    { self.capacity as usize }
  #[inline] fn value_capacity(&self) -> usize    { self.capacity as usize + 1 }
  #[inline] fn kind_const(&self)     -> NodeKind { NodeKind::Branch }

  #[inline] fn set_kind(&mut self, kind: u8)            { self.kind = kind as u8; }
  #[inline] fn set_key_size(&mut self, size: usize)     { self.size = size as u8; }
  #[inline] fn set_value_size(&mut self, _: usize)      {}
  #[inline] fn set_len(&mut self, len: usize)           { self.len = len as u16; }
  #[inline] fn set_capacity(&mut self, capacity: usize) { self.capacity = capacity as u16; }
}

#[derive(Debug, PartialEq)]
#[repr(C)]
pub struct LeafHeader {
  kind: u8,
  key_size: u8,
  value_size: u8,
  // represents the count of both keys and values.
  len: u16,
  capacity: u16,
  _reserved: u8,
}

impl<'a> From<Bytes> for &'a dyn Header {
  fn from(bytes: Bytes) -> &'a dyn Header {
    if bytes[0] == NodeKind::Leaf.into() {
      unsafe { deref_helper::<LeafHeader>(&bytes) }
    } else if bytes[0] == NodeKind::Branch.into() {
      unsafe { deref_helper::<BranchHeader>(&bytes) }
    } else {
      panic!("From::from(): The kind is neither leaf nor branch: {}", bytes[0])
    }
  }
}

impl Header for LeafHeader {
  #[inline] fn kind(&self)                   -> NodeKind { self.kind.into() }
  #[inline] fn key_size(&self)               -> usize    { self.key_size as usize }
  #[inline] fn value_size(&self)             -> usize    { self.value_size as usize }
  #[inline] fn len(&self)                    -> usize    { self.len as usize }
  #[inline] fn capacity(&self)               -> usize    { self.capacity as usize }
  #[inline] fn value_capacity(&self)         -> usize    { self.capacity as usize }
  #[inline] fn kind_const(&self)             -> NodeKind { NodeKind::Leaf }

  #[inline] fn set_kind(&mut self, kind: u8)            { self.kind = kind as u8; }
  #[inline] fn set_key_size(&mut self, size: usize)     { self.key_size = size as u8; }
  #[inline] fn set_value_size(&mut self, size: usize)   { self.value_size = size as u8; }
  #[inline] fn set_len(&mut self, len: usize)           { self.len = len as u16; }
  #[inline] fn set_capacity(&mut self, capacity: usize) { self.capacity = capacity as u16; }
}

pub trait BasicNode<T: Header>: Borrow<[u8]> + BorrowMut<[u8]> {
  #[inline]
  fn header(&self) -> &T {
    unsafe { deref_helper(self.borrow()) }
  }

  #[inline]
  fn header_mut(&mut self) -> &mut T {
    unsafe { deref_mut_helper(self.borrow_mut()) }
  }

  #[inline]
  fn body(&self) -> &[u8] {
    let beg = size_of::<LeafHeader>();
    &self.borrow()[beg..]
  }

  #[inline]
  fn raw_keys(&self) -> &[u8] {
    let beg = size_of::<T>();
    let end = beg + self.header().key_size() * self.header().capacity();
    &self.borrow()[beg..end]
  }

  #[inline]
  fn raw_key_range(&self, beg: usize, end: usize) -> &[u8] {
    debug_assert!(beg <= end);
    debug_assert!(end <= self.header().capacity());
    let size = self.header().key_size();
    let beg = size_of::<T>() + beg * size;
    let end = size_of::<T>() + end * size;
    &self.borrow()[beg..end]
  }

  #[inline]
  fn raw_key(&self, pos: usize) -> &[u8] {
    debug_assert!(pos < self.header().len());
    let beg = size_of::<T>() + self.header().key_size() * pos;
    let end = beg + self.header().key_size();
    &self.borrow()[beg..end]
  }

  #[inline]
  fn key_typed<K, const N: usize>(&self, pos: usize) -> K where K: From<[u8; N]> {
    TryInto::<[u8; N]>::try_into(self.raw_key(pos)).unwrap().into()
  }

  #[inline]
  fn raw_values(&self) -> &[u8] {
    let beg = size_of::<T>() + self.header().key_size() * self.header().capacity();
    let end = beg + self.header().value_size() * self.header().capacity();
    &self.borrow()[beg..end]
  }

  #[inline]
  fn raw_value_range(&self, beg: usize, end: usize) -> &[u8] {
    debug_assert!(beg <= end);
    debug_assert!(end <= self.header().value_capacity());
    let offset = size_of::<T>() + self.header().key_size() * self.header().capacity();
    let beg = offset + beg * self.header().value_size();
    let end = offset + end * self.header().value_size();
    &self.borrow()[beg..end]
  }

  #[inline]
  fn raw_value(&self, pos: usize) -> &[u8] {
    debug_assert!(pos <= self.header().len());
    let beg = size_of::<T>() + self.header().key_size() * self.header().capacity() + self.header().value_size() * pos;
    let end = beg + self.header().value_size();
    &self.borrow()[beg..end]
  }

  #[inline]
  fn value_typed<K, const N: usize>(&self, pos: usize) -> K where K: From<[u8; N]> {
    TryInto::<[u8; N]>::try_into(self.raw_value(pos)).unwrap().into()
  }

  #[inline]
  fn set_key(&mut self, pos: usize, values: &[u8]) {
    self.set_key_slice(pos, pos + 1, values)
  }

  #[inline]
  fn set_key_typed<K, const N: usize>(&mut self, pos: usize, key: K) where K: Into<[u8; N]> {
    self.set_key(pos, &key.into())
  }

  #[inline]
  fn set_value(&mut self, pos: usize, values: &[u8]) {
    debug_assert!(pos <= self.header().len());
    self.set_value_slice(pos, pos + 1, values)
  }

  #[inline]
  fn set_value_typed<V, const N: usize>(&mut self, pos: usize, values: V) where V: Into<[u8; N]> {
    self.set_value(pos, &values.into())
  }

  #[inline]
  fn set_key_slice(&mut self, beg: usize, end: usize, values: &[u8]) {
    self.key_slice_mut(beg, end).copy_from_slice(&values)
  }

  #[inline]
  fn set_value_slice(&mut self, beg: usize, end: usize, values: &[u8]) {
    self.value_slice_mut(beg, end).copy_from_slice(&values)
  }

  #[inline]
  fn raw_keys_mut(&mut self) -> &mut [u8] {
    let beg = size_of::<T>();
    let end = beg + self.header().key_size() * self.header().capacity();
    &mut self.borrow_mut()[beg..end]
  }

  #[inline]
  fn raw_values_mut(&mut self) -> &mut [u8] {
    let beg = size_of::<T>() + self.header().key_size() * self.header().capacity();
    let end = beg + self.header().value_size() * self.header().value_capacity();
    &mut self.borrow_mut()[beg..end]
  }

  #[inline]
  fn key_slice_mut(&mut self, beg: usize, end: usize) -> &mut [u8] {
    debug_assert!(beg <= end);
    debug_assert!(end <= self.header().capacity());
    let size = self.header().key_size();
    let beg = size_of::<T>() + beg * size;
    let end = size_of::<T>() + end * size;
    &mut self.borrow_mut()[beg..end]
  }

  #[inline]
  fn value_slice_mut(&mut self, beg: usize, end: usize) -> &mut [u8] {
    debug_assert!(beg <= end);
    debug_assert!(end <= self.header().value_capacity());
    let offset = size_of::<T>() + self.header().key_size() * self.header().capacity();
    let beg = offset + beg * self.header().value_size();
    let end = offset + end * self.header().value_size();
    &mut self.borrow_mut()[beg..end]
  }

  #[inline]
  fn copy_within_keys(&mut self, beg: usize, end: usize, pos: usize) {
    debug_assert!(pos < self.header().capacity());
    debug_assert!(beg <= end);
    let size = self.header().key_size();
    let beg = size_of::<T>() + beg * size;
    let end = size_of::<T>() + end * size;
    let pos = size_of::<T>() + pos * size;
    self.borrow_mut().copy_within(beg..end, pos);
  }

  #[inline]
  fn copy_within_values(&mut self, beg: usize, end: usize, pos: usize) {
    debug_assert!(pos <= self.header().capacity());
    debug_assert!(beg <= end);
    let offset = size_of::<T>() + self.header().key_size() * self.header().capacity();
    let size = self.header().value_size();
    let beg = offset + beg * size;
    let end = offset + end * size;
    let pos = offset + pos * size;
    self.borrow_mut().copy_within(beg..end, pos);
  }

  #[inline]
  fn init_header(&mut self, kind: u8, key_size: usize, value_size: usize) {
    let header_size = size_of::<T>();
    let is_branch = kind == NodeKind::Branch.into();
    debug_assert!(value_size < 256);
    debug_assert!(key_size < 256);
    let t = if is_branch {
      (PAGE_SIZE - header_size + key_size) / (2 * (key_size + value_size))
    } else {
      ((PAGE_SIZE - header_size) / (key_size + value_size) + 1) / 2
    };
    self.header_mut().set_kind(kind);
    self.header_mut().set_key_size(key_size);
    self.header_mut().set_value_size(value_size);
    self.header_mut().set_len(0);
    self.header_mut().set_capacity(2 * t - 1);
  }

  #[inline]
  fn find_upper_bound(&self, key: &[u8], lt: impl Fn(&[u8], &[u8]) -> bool) -> usize {
    let size = self.header().key_size();
    let len = self.header().len();
    upper_bound(&self.raw_key_range(0, len), size, len, key, lt)
  }

  #[inline]
  fn find_lower_bound(&self, key: &[u8], lt: impl Fn(&[u8], &[u8]) -> bool) -> usize {
    let size = self.header().key_size();
    let len = self.header().len();
    lower_bound(&self.raw_key_range(0, len), size, len, key, lt)
  }
}

pub trait Node<T: Header>: BasicNode<T> + From<Bytes> + Into<Bytes> {
  fn bytes(&self) -> &Bytes;

  #[inline]
  fn key(&self, pos: usize) -> Bytes {
    debug_assert!(pos < self.header().len());
    let beg = size_of::<T>() + self.header().key_size() * pos;
    let end = beg + self.header().key_size();
    self.bytes().slice(beg..end)
  }

  #[inline]
  fn value(&self, pos: usize) -> Bytes {
    debug_assert!((self.header().is_branch() && pos <= self.header().len()) || (!self.header().is_branch() && pos < self.header().len()));
    let beg = size_of::<T>() + self.header().key_size() * self.header().capacity() + pos * self.header().value_size();
    let end = beg + self.header().value_size();
    self.bytes().slice(beg..end)
  }

  #[inline]
  fn key_range(&self, beg: usize, end: usize) -> Bytes {
    debug_assert!(beg <= end);
    debug_assert!(end <= self.header().capacity());
    let beg = size_of::<T>() + beg * self.header().key_size();
    let end = size_of::<T>() + end * self.header().key_size();
    self.bytes().slice(beg..end)
  }

  #[inline]
  fn value_range(&self, beg: usize, end: usize) -> Bytes {
    debug_assert!(beg <= end);
    debug_assert!(end <= self.header().value_capacity());
    let offset = size_of::<T>() + self.header().key_size() * self.header().capacity();
    let beg = offset + beg * self.header().value_size();
    let end = offset + end * self.header().value_size();
    self.bytes().slice(beg..end)
  }
}

pub trait NodeMut<T: Header>: BasicNode<T> + From<BytesMut> + Into<BytesMut> {
  fn is_updated(&self) -> bool;
  fn set_updated(&mut self, updated: bool);

  fn new(key_size: usize, value_size: usize) -> Self {
    let mut bytes = BytesMut::with_capacity(PAGE_SIZE);
    unsafe { bytes.set_len(PAGE_SIZE); }
    bytes.fill(0 as u8);
    let mut node: Self = bytes.into();
    node.init_header(node.header().kind_const().into(), key_size, value_size);
    node
  }
}

#[derive(Clone)]
pub struct Leaf(Bytes);

#[derive(Clone)]
pub struct LeafMut(BytesMut, bool);

#[derive(Clone)]
pub struct Branch(Bytes);

#[derive(Clone)]
pub struct BranchMut(BytesMut, bool);

impl From<Bytes> for Leaf     { #[inline] fn from(bytes: Bytes) -> Self { Leaf(bytes) } }
impl From<Bytes> for Branch   { #[inline] fn from(bytes: Bytes) -> Self { Branch(bytes) } }

impl From<Leaf>     for Bytes { #[inline] fn from(node: Leaf)     -> Bytes { node.0 } }
impl From<Branch>   for Bytes { #[inline] fn from(node: Branch)   -> Bytes { node.0 } }

impl From<BytesMut> for LeafMut     { #[inline] fn from(bytes: BytesMut) -> Self { LeafMut(bytes, false) } }
impl From<BytesMut> for BranchMut   { #[inline] fn from(bytes: BytesMut) -> Self { BranchMut(bytes, false) } }

impl From<LeafMut>     for BytesMut { #[inline] fn from(node: LeafMut)     -> BytesMut { node.0 }}
impl From<BranchMut>   for BytesMut { #[inline] fn from(node: BranchMut)   -> BytesMut { node.0 }}

impl Borrow<[u8]> for Leaf      { #[inline] fn borrow(&self) -> &[u8] { &self.0 } }
impl Borrow<[u8]> for Branch    { #[inline] fn borrow(&self) -> &[u8] { &self.0 } }
impl Borrow<[u8]> for LeafMut   { #[inline] fn borrow(&self) -> &[u8] { &self.0 } }
impl Borrow<[u8]> for BranchMut { #[inline] fn borrow(&self) -> &[u8] { &self.0 } }

impl BorrowMut<[u8]> for Leaf      { #[inline] fn borrow_mut(&mut self) -> &mut [u8] { unimplemented!() } }
impl BorrowMut<[u8]> for Branch    { #[inline] fn borrow_mut(&mut self) -> &mut [u8] { unimplemented!() } }
impl BorrowMut<[u8]> for LeafMut   { #[inline] fn borrow_mut(&mut self) -> &mut [u8] { &mut self.0 } }
impl BorrowMut<[u8]> for BranchMut { #[inline] fn borrow_mut(&mut self) -> &mut [u8] { &mut self.0 } }

impl Borrow<Bytes> for Leaf        { #[inline] fn borrow(&self) -> &Bytes { &self.0 } }
impl Borrow<Bytes> for Branch      { #[inline] fn borrow(&self) -> &Bytes { &self.0 } }

impl Borrow<BytesMut> for LeafMut     { #[inline] fn borrow(&self) -> &BytesMut { &self.0 } }
impl Borrow<BytesMut> for BranchMut   { #[inline] fn borrow(&self) -> &BytesMut { &self.0 } }

impl BorrowMut<BytesMut>  for LeafMut     { #[inline] fn borrow_mut(&mut self) -> &mut BytesMut { &mut self.0 } }
impl BorrowMut<BytesMut>  for BranchMut   { #[inline] fn borrow_mut(&mut self) -> &mut BytesMut { &mut self.0 } }

impl Deref for Leaf     { type Target = Bytes; #[inline] fn deref(&self) -> &Self::Target { &self.0 } }
impl Deref for Branch   { type Target = Bytes; #[inline] fn deref(&self) -> &Self::Target { &self.0 } }

impl BasicNode<LeafHeader>   for Leaf      {}
impl BasicNode<BranchHeader> for Branch    {}
impl BasicNode<LeafHeader>   for LeafMut   {}
impl BasicNode<BranchHeader> for BranchMut {}

impl Node<LeafHeader>   for Leaf     { #[inline] fn bytes(&self) -> &Bytes { &self.0 } }
impl Node<BranchHeader> for Branch   { #[inline] fn bytes(&self) -> &Bytes { &self.0 } }

impl NodeMut<LeafHeader> for LeafMut {
  fn is_updated(&self) -> bool {
    self.1
  }

  fn set_updated(&mut self, updated: bool) {
    self.1 = updated
  }
}

impl NodeMut<BranchHeader> for BranchMut {
  fn is_updated(&self) -> bool {
    self.1
  }

  fn set_updated(&mut self, updated: bool) {
    self.1 = updated
  }
}

impl LeafMut {
  pub fn insert(&mut self, key: &[u8], value: &[u8], pos: usize) {
    debug_assert!(!self.header().is_full());
    debug_assert!(pos <= self.header().len());
    debug_assert_eq!(self.header().key_size(), key.len());
    debug_assert_eq!(self.header().value_size(), value.len());
    let len = self.header().len();
    if pos < len {
      self.copy_within_keys(pos, len, pos + 1);
      self.copy_within_values(pos, len, pos + 1);
    }
    self.header_mut().set_len(len + 1);
    self.set_key(pos, key);
    self.set_value(pos, value);
    self.set_updated(true);
  }

  pub fn split(&mut self, other: &mut Self) -> Bytes {
    debug_assert!(self.header().is_full());
    debug_assert_eq!(self.header().key_size(), other.header().key_size());
    debug_assert_eq!(self.header().capacity(), other.header().capacity());
    let t = (self.header().capacity() + 1) / 2;
    let capacity = self.header().capacity();
    other.set_key_slice(0, t, &self.key_slice_mut(t - 1, capacity));
    other.set_value_slice(0, t, &self.value_slice_mut(t - 1, capacity));
    other.header_mut().set_len(t);
    other.set_updated(true);
    let key = self.raw_key(t - 1);
    let key = Bytes::copy_from_slice(key);
    self.header_mut().set_len(t - 1);
    self.set_updated(true);
    key
  }
}

impl BranchMut {
  pub fn push(&mut self, value: &[u8]) {
    debug_assert_eq!(0, self.header().len());
    debug_assert_eq!(self.header().value_size(), value.len());
    let end = self.header().value_size();
    (&mut self.raw_values_mut())[..end].copy_from_slice(value);
    self.set_updated(true);
  }

  pub fn push_typed(&mut self, value: PageId) {
    self.push(&Into::<[u8; 6]>::into(value))
  }

  pub fn insert(&mut self, pos: usize, key: &[u8], value: &[u8]) {
    debug_assert!(!self.header().is_full());
    debug_assert!(pos <= self.header().len());
    debug_assert_eq!(self.header().key_size(), key.len());
    debug_assert_eq!(self.header().value_size(), value.len());
    let len = self.header().len();
    if pos < len {
      self.copy_within_keys(pos, len, pos + 1);
      self.copy_within_values(pos + 1, len + 1, pos + 2);
    }
    self.header_mut().set_len(len + 1);
    self.set_key(pos, key);
    self.set_value(pos + 1, value);
    self.set_updated(true);
  }

  pub fn insert_typed<K, const N: usize>(&mut self, pos: usize, key: K, value: PageId)
    where
      K: Into<[u8; N]>,
  {
    self.insert(pos, &key.into(), &Into::<[u8; 6]>::into(value))
  }

  pub fn split(&mut self, other: &mut Self) -> Bytes {
    debug_assert!(self.header().is_full());
    debug_assert_eq!(self.header().key_size(), other.header().key_size());
    debug_assert_eq!(self.header().capacity(), other.header().capacity());
    let t = (self.header().capacity() + 1) / 2;
    let capacity = self.header().capacity();
    let value_capacity = self.header().value_capacity();
    other.set_key_slice(0, t - 1, &self.key_slice_mut(t, capacity));
    other.set_value_slice(0, t, &self.value_slice_mut(t, value_capacity));
    other.header_mut().set_len(t - 1);
    other.set_updated(true);
    let key = self.raw_key(t - 1);
    let key = Bytes::copy_from_slice(key);
    self.header_mut().set_len(t - 1);
    self.set_updated(true);
    key
  }
}

#[derive(Debug, PartialEq)]
#[repr(C)]
pub struct MetadataBody {
  _marker: [u8; 4],
  major_version: u16,
  minor_version: u16,
  next_table_id: u16,
  next_index_id: u16,
  page_ids: [PageId; 4],
}

impl MetadataBody {
  #[inline] pub fn data_root(&self)          -> PageId { self.page_ids[0] }
  #[inline] pub fn columns_root(&self)       -> PageId { self.page_ids[1] }
  #[inline] pub fn indices_root(&self)       -> PageId { self.page_ids[2] }
  #[inline] pub fn index_columns_root(&self) -> PageId { self.page_ids[3] }

  #[inline] pub fn set_data_root(&mut self, id: PageId)          { self.page_ids[0] = id }
  #[inline] pub fn set_columns_root(&mut self, id: PageId)       { self.page_ids[1] = id }
  #[inline] pub fn set_indices_root(&mut self, id: PageId)       { self.page_ids[2] = id }
  #[inline] pub fn set_index_columns_root(&mut self, id: PageId) { self.page_ids[3] = id }

  #[inline]
  pub fn next_table_id(&mut self) -> u16 {
    let id = self.next_table_id;
    self.next_table_id += 1;
    id
  }

  #[inline]
  pub fn next_index_id(&mut self) -> u16 {
    let id = self.next_index_id;
    self.next_index_id += 1;
    id
  }
}

pub struct Metadata(Bytes);
pub struct MetadataMut(BytesMut);

impl MetadataMut {
  fn new() -> MetadataMut {
    let mut bytes = BytesMut::with_capacity(PAGE_SIZE);
    unsafe { bytes.set_len(PAGE_SIZE) }
    bytes.fill(0);
    (&mut bytes[0..4]).copy_from_slice(&"Atto".as_bytes());
    MetadataMut(bytes)
  }
}

impl Borrow<MetadataBody> for Metadata    { #[inline] fn borrow(&self) -> &MetadataBody { unsafe { deref_helper(&self.0) } } }
impl Borrow<MetadataBody> for MetadataMut { #[inline] fn borrow(&self) -> &MetadataBody { unsafe { deref_helper(&self.0) } } }

impl BorrowMut<MetadataBody> for MetadataMut { #[inline] fn borrow_mut(&mut self) -> &mut MetadataBody { unsafe { deref_mut_helper(&mut self.0) } } }

impl From<Bytes>    for Metadata    { #[inline] fn from(bytes: Bytes)    -> Metadata    { Metadata(bytes) } }
impl From<BytesMut> for MetadataMut { #[inline] fn from(bytes: BytesMut) -> MetadataMut { MetadataMut(bytes) } }

impl From<MetadataMut> for BytesMut { #[inline] fn from(meta: MetadataMut) -> BytesMut { meta.0 } }

pub trait PageVendor {
  fn new_page<T, N>(&self, key_size: usize, value_size: usize) -> N
    where
      T: Header,
      N: NodeMut<T>
  {
    debug_assert!(0 < key_size);
    debug_assert!(0 < value_size);
    N::new(key_size, value_size)
  }

  fn fetch<T, N>(&self, id: PageId) -> Result<N>
    where
      T: Header,
      N: Node<T>;

  fn save<T, N>(&mut self, node: N) -> Result<PageId>
    where
      T: Header,
      N: NodeMut<T>;

  fn copy<T, N>(&self, id: PageId) -> Result<N>
    where
      T: Header,
      N: NodeMut<T>;

  fn fetch_header(&self, id: PageId) -> Result<&dyn Header>;
  fn fetch_key_size(&self, id: PageId) -> Result<usize>;
  fn fetch_len(&self, id: PageId) -> Result<usize>;
  fn is_full(&self, id: PageId) -> Result<bool>;
  fn is_branch(&self, id: PageId) -> Result<bool>;
  fn is_leaf(&self, id: PageId) -> Result<bool>;
  fn fetch_meta(&self) -> Result<Metadata>;
  fn copy_meta(&self) -> Result<MetadataMut>;
  fn save_meta(&mut self, meta: MetadataMut) -> Result<()>;
}

#[derive(Debug)]
pub struct DummyPageVendor {
  pages: Vec<Bytes>,
}

impl DummyPageVendor {
  pub fn new() -> DummyPageVendor {
    let meta = MetadataMut::new();
    Self { pages: vec![meta.0.freeze()] }
  }
}

impl PageVendor for DummyPageVendor {
  fn fetch<T, N>(&self, id: PageId) -> Result<N>
    where
      T: Header,
      N: Node<T>,
  {
    let id = Into::<usize>::into(id);
    debug_assert!(id < self.pages.len());
    Ok(self.pages[id].clone().into())
  }

  fn fetch_header(&self, id: PageId) -> Result<&dyn Header> {
    let id = Into::<usize>::into(id);
    debug_assert!(id < self.pages.len());
    Ok(self.pages[id].clone().into())
  }

  fn save<T, N>(&mut self, node: N) -> Result<PageId>
    where
      T: Header,
      N: NodeMut<T>
  {
    let id = self.pages.len();
    self.pages.push(node.into().freeze());
    Ok(Into::<PageId>::into(id))
  }

  fn copy<T, N>(&self, id: PageId) -> Result<N>
    where
      T: Header,
      N: NodeMut<T>,
  {
    let id = Into::<usize>::into(id);
    debug_assert!(id < self.pages.len());
    let node = &self.pages[id];
    let mut bytes = BytesMut::with_capacity(node.len());
    unsafe { bytes.set_len(node.len()); }
    bytes.copy_from_slice(&node);
    Ok(bytes.into())
  }

  #[inline]
  fn fetch_key_size(&self, id: PageId) -> Result<usize> {
    debug_assert!(Into::<usize>::into(id) < self.pages.len());
    Ok(self.fetch_header(id)?.key_size())
  }

  #[inline]
  fn fetch_len(&self, id: PageId) -> Result<usize> {
    debug_assert!(Into::<usize>::into(id) < self.pages.len());
    Ok(self.fetch_header(id)?.len())
  }

  fn is_full(&self, id: PageId) -> Result<bool> {
    let header = self.fetch_header(id)?;
    Ok(header.len() == header.capacity())
  }

  fn is_branch(&self, id: PageId) -> Result<bool> {
    debug_assert!(Into::<usize>::into(id) < self.pages.len());
    Ok(self.pages[Into::<usize>::into(id)][0] == NodeKind::Branch.into())
  }

  fn is_leaf(&self, id: PageId) -> Result<bool> {
    debug_assert!(Into::<usize>::into(id) < self.pages.len());
    Ok(self.pages[Into::<usize>::into(id)][0] == NodeKind::Leaf.into())
  }

  fn fetch_meta(&self) -> Result<Metadata> {
    Ok(self.pages[0].clone().into())
  }

  fn copy_meta(&self) -> Result<MetadataMut> {
    let node = &self.pages[0];
    let mut bytes = BytesMut::with_capacity(node.len());
    unsafe { bytes.set_len(node.len()); }
    bytes.copy_from_slice(&node);
    Ok(bytes.into())
  }

  fn save_meta(&mut self, meta: MetadataMut) -> Result<()> {
    self.pages[0] = meta.0.freeze();
    Ok(())
  }
}

#[cfg(test)]
mod tests {
  use std::sync::Arc;
  use bytes::{Buf, BufMut};
  use super::*;

  fn lt_u64(mut lhs: &[u8], mut rhs: &[u8]) -> bool {
    lhs.get_u64_le() < rhs.get_u64_le()
  }

  fn to_u64_le(value: u64) -> Bytes {
    let mut bytes = BytesMut::with_capacity(size_of::<u64>());
    bytes.put_u64_le(value);
    bytes.freeze()
  }

  fn to_u48_le(value: u64) -> Bytes {
    let mut bytes = [0u8; 6];
    unsafe { bytes.copy_from_slice(&cast_to_byte_slice(&value)[..6]) }
    Bytes::copy_from_slice(&bytes)
  }

  #[test]
  fn empty_leaf() {
    type Key = u64;
    type Value = i32;
    let mut vendor = DummyPageVendor::new();

    let leaf: LeafMut = vendor.new_page(size_of::<Key>(), size_of::<Value>());
    let id = vendor.save(leaf).unwrap();
    let leaf: Leaf = vendor.fetch(id).unwrap();
    let key = u64::to_le_bytes(42);

    assert!(!leaf.header().is_full());
    assert_eq!(0, leaf.find_upper_bound(&key, lt_u64));
    assert_eq!(0, leaf.find_lower_bound(&key, lt_u64));
  }

  #[test]
  fn branch_with_1_elem() {
    let id1 = to_u48_le(41);
    let id2 = to_u48_le(42);
    let key1 = to_u64_le(11);
    let key2 = to_u64_le(12);
    let key3 = to_u64_le(13);
    let mut vendor = DummyPageVendor::new();
    let mut branch: BranchMut = vendor.new_page(size_of::<u64>(), size_of::<PageId>());
    branch.push(&id1);
    branch.insert(0, &key2, &id2);
    let branch_id = vendor.save(branch).unwrap();
    let branch: Branch = vendor.fetch(branch_id).unwrap();

    assert!(!branch.header().is_full());
    assert_eq!(1, branch.header().len);
    assert_eq!(key2.clone(), branch.key(0));
    assert_eq!(id1, branch.value(0));
    assert_eq!(id2, branch.value(1));
    assert_eq!(0, branch.find_lower_bound(&key1, lt_u64));
    assert_eq!(0, branch.find_lower_bound(&key2, lt_u64));
    assert_eq!(1, branch.find_lower_bound(&key3, lt_u64));
    assert_eq!(0, branch.find_upper_bound(&key1, lt_u64));
    assert_eq!(1, branch.find_upper_bound(&key2, lt_u64));
    assert_eq!(1, branch.find_upper_bound(&key3, lt_u64));
  }

  #[test]
  fn branch_with_full_elems() {
    let count: usize = 291;
    type Key = u64;
    let mut vendor = Arc::new(DummyPageVendor::new());
    let v1 = to_u48_le(41);

    let id = {
      let mut branch: BranchMut = Arc::get_mut(&mut vendor).unwrap().new_page(size_of::<Key>(), size_of::<PageId>());
      branch.push(&v1);
      // let mut id = vec![];
      // id.put_u64_le(0);
      let id = to_u48_le(0);
      branch.push(&id);
      for i in 0..count {
        let key = to_u64_le(i as u64);
        let value = to_u48_le(i as u64 + 1);
        branch.insert(i, &key, &value)
      }
      Arc::get_mut(&mut vendor).unwrap().save(branch).unwrap()
    };

    let branch1: BranchMut = Arc::get_mut(&mut vendor).unwrap().copy(id).unwrap();
    let mut branch2: BranchMut = Arc::get_mut(&mut vendor).unwrap().new_page(size_of::<Key>(), size_of::<PageId>());
    let branch_id1 = Arc::get_mut(&mut vendor).unwrap().save(branch1).unwrap();
    let branch1: Branch = Arc::get_mut(&mut vendor).unwrap().fetch(branch_id1).unwrap();

    assert_eq!(count, branch1.header().len());
    for i in 0..count {
      let key = to_u64_le(i as u64);
      assert_eq!(key, branch1.key(i));
      assert_eq!(i, branch1.find_lower_bound(&key, lt_u64));
      assert_eq!(i + 1, branch1.find_upper_bound(&key, lt_u64));
    }

    let mut branch1: BranchMut = Arc::get_mut(&mut vendor).unwrap().copy(branch_id1).unwrap();
    let key = branch1.split(&mut branch2);
    let branch_id1 = Arc::get_mut(&mut vendor).unwrap().save(branch1).unwrap();
    let branch_id2 = Arc::get_mut(&mut vendor).unwrap().save(branch2).unwrap();
    let branch1: Branch = Arc::get_mut(&mut vendor).unwrap().fetch(branch_id1).unwrap();
    let branch2: Branch = Arc::get_mut(&mut vendor).unwrap().fetch(branch_id2).unwrap();

    let v = to_u64_le(count as u64 / 2);
    let half = count / 2;
    assert_eq!(v, key);
    assert_eq!(half, branch1.header().len());
    assert_eq!(half, branch2.header().len());
    for i in 0..half {
      let mut k = vec![];
      k.put_u64_le(i as u64);
      assert_eq!(k, branch1.key(i as usize));
    }
    for i in 0..half {
      let mut k = vec![];
      k.put_u64_le(i as u64 + half as u64 + 1);
      assert_eq!(k, branch2.key(i as usize));
    }
    for i in 0..half {
      let value = to_u48_le(i as u64);
      assert_eq!(value, branch1.value(i));
    }
    for i in 0..half {
      let value = to_u48_le(i as u64 + half as u64 + 1);
      assert_eq!(value, branch2.value(i));
    }
  }
}