use std::collections::BTreeMap;

use bytes::Bytes;
use nix::time::{ClockId, clock_gettime};
use nix::sys::time::TimeSpec;

#[cfg(target_os = "linux")]
use nix::unistd::getpid;

use crate::schema::TableId;
use crate::sort::topological_sort;
use attosql_commons::iter::GroupBy;

#[derive(Copy, Clone, Debug, Eq, Ord, PartialEq, PartialOrd)]
#[repr(packed)]
pub struct TransId(u64);

impl TransId {
  pub fn zero() -> TransId {
    TransId(0)
  }

  pub fn succ(&self) -> TransId {
    TransId(self.0 + 1)
  }
}

impl From<u64> for TransId {
  fn from(id: u64) -> TransId {
    TransId(id)
  }
}

impl From<TransId> for u64 {
  fn from(TransId(id): TransId) -> u64 {
    id
  }
}

#[derive(Clone, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub enum Action {
  Read,
  Write,
}

#[derive(Clone, Debug, Eq, Ord, PartialEq, PartialOrd)]
struct LogEntry {
  time: TimeSpec,
  trans_id: TransId,
  action: Action,
  table_id: TableId,
  key: Bytes,
  value: Option<Bytes>,
}

#[derive(Clone, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub struct WriteEvent {
  pub table_id: TableId,
  pub key: Bytes,
  pub value: Bytes,
}

pub struct TransactionScheduler {
  log: BTreeMap<TransId, Vec<LogEntry>>,
  related_trans: BTreeMap<TransId, Vec<TransId>>,
  ref_counts: BTreeMap<TransId, usize>,
  next_trans_id: TransId,
}

impl TransactionScheduler {
  pub fn new() -> TransactionScheduler {
    let log = BTreeMap::new();
    let related_trans = BTreeMap::new();
    let ref_counts = BTreeMap::new();
    let next_trans_id = TransId::zero();
    TransactionScheduler { log, related_trans, ref_counts, next_trans_id }
  }

  fn increase_count(&mut self, related_trans: &[TransId]) {
    for t in related_trans {
      if self.ref_counts.contains_key(t) {
        self.ref_counts.insert(t.clone(), self.ref_counts[t] + 1);
      } else {
        self.ref_counts.insert(t.clone(), 1);
      }
    }
  }

  fn decrease_count(&mut self, id: &TransId) {
    debug_assert!(self.related_trans.contains_key(id));
    for t in &self.related_trans[id] {
      debug_assert!(self.ref_counts.contains_key(t));
      if self.ref_counts[t] <= 1 {
        self.ref_counts.remove(t);
        self.log.remove(t);
      } else {
        self.ref_counts.insert(t.clone(), self.ref_counts[t] - 1);
      }
    }
  }

  pub fn begin(&mut self) -> TransId {
    let trans_id = self.next_trans_id.clone();
    self.next_trans_id = self.next_trans_id.succ();
    let mut related_trans: Vec<_> = self.log.keys().cloned().collect();
    related_trans.push(trans_id.clone());
    self.increase_count(&related_trans);
    self.related_trans.insert(trans_id.clone(), related_trans);
    self.log.insert(trans_id.clone(), vec![]);
    trans_id
  }

  pub fn commit(&mut self, trans_id: &TransId) {
    self.log.remove(trans_id);
    self.decrease_count(trans_id);
    self.related_trans.remove(trans_id);
  }

  pub fn rollback(&mut self, trans_id: &TransId) {
    self.log.remove(trans_id);
    self.decrease_count(trans_id);
    self.related_trans.remove(trans_id);
  }

  #[cfg(target_os = "linux")]
  fn get_clock(&self) -> TimeSpec {
    clock_gettime(ClockId::pid_cpu_clock_id(getpid()).unwrap()).unwrap()
  }

  #[cfg(target_os = "macos")]
  fn get_clock(&self) -> TimeSpec {
    clock_gettime(ClockId::CLOCK_PROCESS_CPUTIME_ID).unwrap()
  }

  pub fn read(&mut self, trans_id: &TransId, table_id: &TableId, key: &Bytes) {
    debug_assert!(self.log.contains_key(trans_id));
    let entry = LogEntry {
      time: self.get_clock(),
      trans_id: trans_id.clone(),
      action: Action::Read,
      table_id: table_id.clone(),
      key: key.clone(),
      value: None,
    };
    self.log.get_mut(trans_id).map(|vec| vec.push(entry));
  }

  pub fn write(&mut self, trans_id: &TransId, table_id: &TableId, key: &Bytes, value: &Bytes) {
    debug_assert!(self.log.contains_key(trans_id));
    let entry = LogEntry {
      time: self.get_clock(),
      trans_id: trans_id.clone(),
      action: Action::Write,
      table_id: table_id.clone(),
      key: key.clone(),
      value: Some(value.clone()),
    };
    self.log.get_mut(trans_id).map(|vec| vec.push(entry));
  }

  pub fn schedule_to_commit(&mut self, trans_id: &TransId) -> Option<Vec<WriteEvent>> {
    debug_assert!(self.log.contains_key(trans_id));
    debug_assert!(self.related_trans.contains_key(trans_id));

    // gather all related LogEntries.
    let mut vertices: Vec<LogEntry> = self.related_trans[trans_id].iter()
      .filter(|t| self.log.contains_key(t))
      .map(|t| self.log[t].clone())
      .flatten()
      .collect();

    // map (table_id, key, prev_value) to a LogEntry.
    let mut key_to_entry: Vec<((TableId, Bytes), usize)> = vertices.iter()
      .enumerate()
      .filter(|(_, e)| e.action == Action::Write)
      .map(|(i, e)| ((e.table_id.clone(), e.key.clone()), i))
      .collect();
    key_to_entry.sort_by(|a, b| a.0.cmp(&b.0));
    let key_to_entry: BTreeMap<(TableId, Bytes), Vec<usize>> = GroupBy::new(Box::new(key_to_entry.into_iter())).collect();

    // check write-write conflict.
    let ok = key_to_entry.values()
      .all(|vs| {
        (0..(vs.len() - 1))
          .zip(1..vs.len())
          .all(|(i, j)|
            vertices[vs[i]].action == Action::Read ||
            vertices[vs[j]].action == Action::Read ||
            vertices[vs[i]].trans_id == vertices[vs[j]].trans_id )});
    if !ok {
      return None
    }

    // get a Vec of all dependencies.
    let edges: Vec<(usize, usize)> = key_to_entry.values()
      .flat_map(|vs| {
        let mut result = vec![];
        for i in 0..(vs.len() - 1) {
          for j in 1..vs.len() {
            let t1 = vertices[vs[i]].time;
            let t2 = vertices[vs[j]].time;
            if t1 < t2 {
              result.push((i, j))
            } else {
              result.push((j, i))
            }
          }
        }
        result
      }).collect();

    // sort
    let ok = topological_sort(&mut vertices, &edges);

    if ok {
      let write_event = vertices.into_iter()
        .filter(|v| v.action == Action::Write && &v.trans_id == trans_id)
        .map(|v| WriteEvent{table_id: v.table_id, key: v.key, value: v.value.unwrap()})
        .collect();
      Some(write_event)
    } else {
      None
    }
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use bytes::{Bytes, BytesMut, BufMut};

  fn from_u64(value: u64) -> Bytes {
    let mut bytes = BytesMut::new();
    bytes.put_u64_le(value);
    bytes.freeze()
  }

  #[test]
  fn schedule_to_commit_empty() {
    let mut trans = TransactionScheduler::new();
    let id = trans.begin();
    let result = trans.schedule_to_commit(&id);
    assert_eq!(Some(vec![]), result);
  }

  #[test]
  fn schedule_to_commit_1_read() {
    let mut trans = TransactionScheduler::new();
    let t1: TableId = 1u32.into();
    let k1 = from_u64(1);

    let id = trans.begin();
    trans.read(&id, &t1, &k1);
    let result = trans.schedule_to_commit(&id);

    assert_eq!(Some(vec![]), result);
  }

  #[test]
  fn schedule_to_commit_1_write() {
    let mut trans = TransactionScheduler::new();
    let t1: TableId = 1u32.into();
    let k1 = from_u64(1);
    let v1 = from_u64(41);

    let id = trans.begin();
    trans.write(&id, &t1, &k1, &v1);
    let result = trans.schedule_to_commit(&id);

    assert_eq!(Some(vec![WriteEvent{table_id: t1.clone(), key: k1.clone(), value: v1.clone()}]), result);
  }

  #[test]
  fn schedule_to_commit_2_read() {
    let mut trans = TransactionScheduler::new();
    let t1: TableId = 1u32.into();
    let k1 = from_u64(1);

    let id = trans.begin();
    trans.read(&id, &t1, &k1);
    trans.read(&id, &t1, &k1);
    let result = trans.schedule_to_commit(&id);

    assert_eq!(Some(vec![]), result);
  }

  #[test]
  fn schedule_to_commit_2_write() {
    let mut trans = TransactionScheduler::new();
    let t1: TableId = 1u32.into();
    let k1 = from_u64(1);
    let v1 = from_u64(41);

    let id = trans.begin();
    trans.write(&id, &t1, &k1, &v1);
    trans.write(&id, &t1, &k1, &v1);
    let result = trans.schedule_to_commit(&id);

    assert!(result.is_some());
    assert_eq!(2, result.unwrap().len());
  }

  #[test]
  fn schedule_to_commit_2_write_diff() {
    let mut trans = TransactionScheduler::new();
    let t1: TableId = 1u32.into();
    let k1 = from_u64(1);
    let k2 = from_u64(2);
    let v1 = from_u64(41);

    let id = trans.begin();
    trans.write(&id, &t1, &k1, &v1);
    trans.write(&id, &t1, &k2, &v1);
    let result = trans.schedule_to_commit(&id);

    assert!(result.is_some());
    assert_eq!(2, result.unwrap().len());
  }

  #[test]
  fn schedule_to_commit_1_write_1_read_dep() {
    let mut trans = TransactionScheduler::new();
    let t1: TableId = 1u32.into();
    let k1 = from_u64(1);
    let v1 = from_u64(41);

    let id = trans.begin();
    trans.write(&id, &t1, &k1, &v1);
    trans.read(&id, &t1, &k1);
    let result = trans.schedule_to_commit(&id);

    assert_eq!(Some(vec![WriteEvent{table_id: t1.clone(), key: k1.clone(), value: v1.clone()}]), result);
  }

  #[test]
  fn schedule_to_commit_1_read_1_write_dep() {
    let mut trans = TransactionScheduler::new();
    let t1: TableId = 1u32.into();
    let k1 = from_u64(1);
    let v1 = from_u64(41);

    let id = trans.begin();
    trans.read(&id, &t1, &k1);
    trans.write(&id, &t1, &k1, &v1);
    let result = trans.schedule_to_commit(&id);

    assert_eq!(Some(vec![WriteEvent{table_id: t1.clone(), key: k1.clone(), value: v1.clone()}]), result);
  }

  #[test]
  fn schedule_to_commit_2_trans_2_read_2_write_dep() {
    let mut trans = TransactionScheduler::new();
    let t1: TableId = 1u32.into();
    let k1 = from_u64(1);
    let k2 = from_u64(2);
    let v1 = from_u64(41);
    let v2 = from_u64(42);

    let id1 = trans.begin();
    let id2 = trans.begin();
    trans.read(&id1, &t1, &k1);
    trans.write(&id2, &t1, &k1, &v1);
    trans.write(&id1, &t1, &k2, &v2);
    trans.read(&id2, &t1, &k2);
    let result = trans.schedule_to_commit(&id1);

    assert_eq!(Some(vec![WriteEvent{table_id: t1.clone(), key: k2.clone(), value: v2.clone()}]), result);
  }

  #[test]
  fn schedule_to_commit_2_trans_2_read_2_write_diff() {
    let mut trans = TransactionScheduler::new();
    let t1: TableId = 1u32.into();
    let k1 = from_u64(1);
    let k2 = from_u64(2);
    let v1 = from_u64(41);
    let v2 = from_u64(42);

    let id1 = trans.begin();
    trans.read(&id1, &t1, &k1);
    trans.write(&id1, &t1, &k2, &v2);
    let result1 = trans.schedule_to_commit(&id1);
    trans.commit(&id1);
    let id2 = trans.begin();
    trans.write(&id2, &t1, &k2, &v1);
    trans.read(&id2, &t1, &k1);
    let result2 = trans.schedule_to_commit(&id2);
    trans.commit(&id2);

    assert_eq!(Some(vec![WriteEvent{table_id: t1.clone(), key: k2.clone(), value: v2.clone()}]), result1);
    assert_eq!(Some(vec![WriteEvent{table_id: t1.clone(), key: k2.clone(), value: v1.clone()}]), result2);
  }

  #[test]
  fn schedule_to_commit_2_trans_2_read_2_write_error() {
    let mut trans = TransactionScheduler::new();
    let t1: TableId = 1u32.into();
    let k1 = from_u64(1);
    let k2 = from_u64(2);
    let v1 = from_u64(41);
    let v2 = from_u64(42);

    let id1 = trans.begin();
    let id2 = trans.begin();
    trans.read(&id1, &t1, &k1);
    trans.write(&id1, &t1, &k1, &v1);
    trans.write(&id2, &t1, &k1, &v2);
    trans.read(&id2, &t1, &k2);
    let result1 = trans.schedule_to_commit(&id1);
    let result2 = trans.schedule_to_commit(&id2);

    assert_eq!(Some(vec![WriteEvent{table_id: t1.clone(), key: k1.clone(), value: v1.clone()}]), result1);
    assert_eq!(None, result2);
  }

  #[test]
  fn schedule_to_commit_2_trans_2_read_2_write_2_commit() {
    let mut trans = TransactionScheduler::new();
    let t1: TableId = 1u32.into();
    let k1 = from_u64(1);
    let k2 = from_u64(2);
    let v1 = from_u64(41);
    let v2 = from_u64(42);

    let id1 = trans.begin();
    let id2 = trans.begin();
    trans.read(&id1, &t1, &k1);
    trans.write(&id1, &t1, &k1, &v1);
    trans.write(&id2, &t1, &k2, &v2);
    trans.read(&id2, &t1, &k2);
    let result1 = trans.schedule_to_commit(&id1);
    let result2 = trans.schedule_to_commit(&id2);

    assert_eq!(Some(vec![WriteEvent{table_id: t1.clone(), key: k1.clone(), value: v1.clone()}]), result1);
    assert_eq!(Some(vec![WriteEvent{table_id: t1.clone(), key: k2.clone(), value: v2.clone()}]), result2);
  }

  #[test]
  fn schedule_to_commit_3_trans_3_read_3_write() {
    let mut trans = TransactionScheduler::new();
    let t1: TableId = 1u32.into();
    let k1 = from_u64(1);
    let k2 = from_u64(2);
    let k3 = from_u64(3);
    let v1 = from_u64(41);
    let v2 = from_u64(42);
    let v3 = from_u64(43);
    let id1 = trans.begin();
    let id2 = trans.begin();
    let id3 = trans.begin();

    trans.read(&id1, &t1, &k1);
    trans.write(&id1, &t1, &k2, &v2);
    trans.read(&id2, &t1, &k2);
    trans.write(&id2, &t1, &k3, &v3);
    trans.read(&id3, &t1, &k3);
    trans.write(&id3, &t1, &k1, &v1);

    let result1 = trans.schedule_to_commit(&id1);
    let result2 = trans.schedule_to_commit(&id2);
    let result3 = trans.schedule_to_commit(&id3);

    assert_eq!(Some(vec![WriteEvent{table_id: t1.clone(), key: k2.clone(), value: v2.clone()}]), result1);
    assert_eq!(Some(vec![WriteEvent{table_id: t1.clone(), key: k3.clone(), value: v3.clone()}]), result2);
    assert_eq!(Some(vec![WriteEvent{table_id: t1.clone(), key: k1.clone(), value: v1.clone()}]), result3);
  }

  #[test]
  fn schedule_to_commit_3_trans_3_read_3_write_2() {
    let mut trans = TransactionScheduler::new();
    let t1: TableId = 1u32.into();
    let k1 = from_u64(1);
    let k2 = from_u64(2);
    let k3 = from_u64(3);
    let v2 = from_u64(42);
    let v3 = from_u64(43);

    let id1 = trans.begin();
    trans.read(&id1, &t1, &k1);
    let id2 = trans.begin();
    trans.write(&id1, &t1, &k2, &v2);
    trans.read(&id2, &t1, &k2);
    trans.write(&id2, &t1, &k3, &v3);
    let result2 = trans.schedule_to_commit(&id2);
    trans.commit(&id2);
    let id3 = trans.begin();
    trans.read(&id3, &t1, &k2);
    trans.write(&id3, &t1, &k2, &v2);
    let result3 = trans.schedule_to_commit(&id3);
    trans.commit(&id3);
    let result1 = trans.schedule_to_commit(&id1);
    trans.commit(&id1);

    assert_eq!(Some(vec![WriteEvent{table_id: t1.clone(), key: k2.clone(), value: v2.clone()}]), result1);
    assert_eq!(Some(vec![WriteEvent{table_id: t1.clone(), key: k3.clone(), value: v3.clone()}]), result2);
    assert_eq!(None, result3);
  }

  #[test]
  fn schedule_to_commit_3_trans_3_write() {
    let mut trans = TransactionScheduler::new();
    let t1: TableId = 1u32.into();
    let k1 = from_u64(1);
    let v2 = from_u64(42);
    let v3 = from_u64(43);

    let id1 = trans.begin();
    trans.write(&id1, &t1, &k1, &v2);
    let result1 = trans.schedule_to_commit(&id1);
    trans.commit(&id1);
    let id2 = trans.begin();
    trans.write(&id2, &t1, &k1, &v3);
    let result2 = trans.schedule_to_commit(&id2);
    trans.commit(&id2);
    let id3 = trans.begin();
    trans.write(&id3, &t1, &k1, &v2);
    let result3 = trans.schedule_to_commit(&id3);
    trans.commit(&id3);

    assert_eq!(Some(vec![WriteEvent{table_id: t1.clone(), key: k1.clone(), value: v2.clone()}]), result1);
    assert_eq!(Some(vec![WriteEvent{table_id: t1.clone(), key: k1.clone(), value: v3.clone()}]), result2);
    assert_eq!(Some(vec![WriteEvent{table_id: t1.clone(), key: k1.clone(), value: v2.clone()}]), result3);
  }

  #[test]
  fn schedule_to_commit_3_trans_3_write_2() {
    let mut trans = TransactionScheduler::new();
    let t1: TableId = 1u32.into();
    let k1 = from_u64(1);
    let v2 = from_u64(42);
    let v3 = from_u64(43);

    let id2 = trans.begin();
    trans.read(&id2, &t1, &k1);
    let id1 = trans.begin();
    let id3 = trans.begin();
    trans.write(&id1, &t1, &k1, &v2);
    trans.write(&id2, &t1, &k1, &v3);
    trans.write(&id3, &t1, &k1, &v2);
    let result1 = trans.schedule_to_commit(&id1);
    let result2 = trans.schedule_to_commit(&id2);
    let result3 = trans.schedule_to_commit(&id3);
    trans.commit(&id1);
    trans.commit(&id2);
    trans.commit(&id3);

    assert_eq!(None, result1);
    assert_eq!(Some(vec![WriteEvent{table_id: t1.clone(), key: k1.clone(), value: v3.clone()}]), result2);
    assert_eq!(None, result3);
  }

  #[test]
  fn schedule_to_commit_4_trans() {
    let mut trans = TransactionScheduler::new();
    let t1: TableId = 1u32.into();
    let k1 = from_u64(1);
    let k2 = from_u64(1);
    let v1 = from_u64(41);
    let v2 = from_u64(42);
    let v3 = from_u64(43);

    let id1 = trans.begin();
    let id2 = trans.begin();
    let id3 = trans.begin();
    let id4 = trans.begin();
    trans.write(&id1, &t1, &k1, &v1);
    trans.read(&id3, &t1, &k1);
    trans.write(&id4, &t1, &k1, &v2);
    trans.read(&id3, &t1, &k2);
    trans.write(&id2, &t1, &k2, &v3);
    trans.read(&id4, &t1, &k2);
    let result4 = trans.schedule_to_commit(&id4);
    trans.commit(&id4);
    let result3 = trans.schedule_to_commit(&id3);
    trans.commit(&id3);
    let result2 = trans.schedule_to_commit(&id2);
    trans.commit(&id2);
    let result1 = trans.schedule_to_commit(&id1);
    trans.commit(&id1);

    assert_eq!(Some(vec![WriteEvent{table_id: t1.clone(), key: k1.clone(), value: v1.clone()}]), result1);
    assert_eq!(None, result2);
    assert_eq!(None, result3);
    assert_eq!(None, result4);
  }
}