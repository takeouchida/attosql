use std::collections::BTreeSet;

pub fn topological_sort<V>(vertices: &mut Vec<V>, edges: &Vec<(usize, usize)>) -> bool {
  let len = vertices.len();
  debug_assert!(edges.iter().all(|(a, b)| *a < len && *b < len));
  let mut es = vec![vec![]; len];
  for (a, b) in edges {
    es[*a].push(*b);
  }
  let mut visited = BTreeSet::new();
  let mut inserted = BTreeSet::new();
  let mut result = vec![];
  for i in 0..len {
    if visited.contains(&i) {
      continue
    }
    let ok = dfs(i, &es, &mut visited, &mut inserted, &mut result);
    if !ok {
      return false
    }
  }
  result.reverse();
  let mut indices = vec![0; len];
  for i in 0..len {
    indices[result[i]] = i;
  }
  for i in 0..len {
    let a = indices[i];
    if a == i {
      continue
    }
    let b = result[i];
    vertices.swap(i, b);
    indices.swap(i, b);
    result.swap(i, a);
  }
  true
}

fn dfs(u: usize, es: &Vec<Vec<usize>>, visited: &mut BTreeSet<usize>, inserted: &mut BTreeSet<usize>, result: &mut Vec<usize>) -> bool {
  if visited.contains(&u) {
    return false
  }
  visited.insert(u);
  for &v in &es[u] {
    if !dfs(v, es, visited, inserted, result) {
      return false
    }
  }
  visited.remove(&u);
  if !inserted.contains(&u) {
    result.push(u);
    inserted.insert(u);
  }
  true
}

#[cfg(test)]
mod tests {
  use super::*;

  fn ordered(vertices: &Vec<i32>, a: i32, b: i32) -> bool {
    vertices.iter().position(|&x| x == a).unwrap() < vertices.iter().position(|&x| x == b).unwrap()
  }

  #[test]
  fn topological_sort_empty() {
    let mut vertices: Vec::<i32> = vec![];
    let edges = vec![];

    let ok = topological_sort(&mut vertices, &edges);

    assert!(ok);
    assert_eq!(0, vertices.len());
  }

  #[test]
  fn topological_sort_1_elem() {
    let mut vertices: Vec::<i32> = vec![41];
    let edges = vec![];

    let ok = topological_sort(&mut vertices, &edges);

    assert!(ok);
    assert_eq!(1, vertices.len());
  }

  #[test]
  fn topological_sort_2_elems() {
    let mut vertices: Vec::<i32> = vec![41, 42];
    let edges = vec![];

    let ok = topological_sort(&mut vertices, &edges);

    assert!(ok);
    assert_eq!(2, vertices.len());
  }

  #[test]
  fn topological_sort_2_elems_inv() {
    let mut vertices: Vec::<i32> = vec![41, 42];
    let edges = vec![(1, 0)];

    let ok = topological_sort(&mut vertices, &edges);

    assert!(ok);
    assert_eq!(2, vertices.len());
    assert!(ordered(&vertices, 42, 41));
  }

  #[test]
  fn topological_sort_2_elems_error() {
    let mut vertices: Vec::<i32> = vec![41, 42];
    let edges = vec![(0, 1), (1, 0)];

    let ok = topological_sort(&mut vertices, &edges);

    assert!(!ok);
  }

  #[test]
  fn topological_sort_3_elems_no_edges() {
    let mut vertices: Vec::<i32> = vec![41, 42, 43];
    let edges = vec![];

    let ok = topological_sort(&mut vertices, &edges);

    assert!(ok);
    assert_eq!(3, vertices.len());
  }

  #[test]
  fn topological_sort_3_elems_random() {
    let mut vertices: Vec::<i32> = vec![41, 42, 43];
    let edges = vec![(1, 0), (1, 2)];

    let ok = topological_sort(&mut vertices, &edges);

    assert!(ok);
    assert_eq!(3, vertices.len());
    assert!(ordered(&vertices, 42, 41));
    assert!(ordered(&vertices, 42, 43));
  }

  #[test]
  fn topological_sort_3_elems_random_2() {
    let mut vertices: Vec::<i32> = vec![41, 42, 43];
    let edges = vec![(0, 1), (2, 1)];

    let ok = topological_sort(&mut vertices, &edges);

    assert!(ok);
    assert_eq!(3, vertices.len());
    assert!(ordered(&vertices, 41, 42));
    assert!(ordered(&vertices, 43, 42));
  }

  #[test]
  fn topological_sort_3_elems_random_3() {
    let mut vertices: Vec::<i32> = vec![41, 42, 43];
    let edges = vec![(2, 1), (1, 0)];

    let ok = topological_sort(&mut vertices, &edges);

    assert!(ok);
    assert_eq!(3, vertices.len());
    assert!(ordered(&vertices, 43, 42));
    assert!(ordered(&vertices, 42, 41));
  }

  #[test]
  fn topological_sort_3_elems_cycle() {
    let mut vertices: Vec::<i32> = vec![41, 42, 43];
    let edges = vec![(0, 1), (1, 2), (2, 0)];

    let ok = topological_sort(&mut vertices, &edges);

    assert!(!ok);
  }

  #[test]
  fn topological_sort_3_elems_random_4() {
    let mut vertices: Vec::<i32> = vec![41, 42, 43];
    let edges = vec![(0, 1), (1, 2), (0, 2)];

    let ok = topological_sort(&mut vertices, &edges);

    assert!(ok);
    assert!(ordered(&vertices, 41, 42));
    assert!(ordered(&vertices, 42, 43));
  }

  #[test]
  fn topological_sort_4_elems_random() {
    let mut vertices: Vec::<i32> = vec![41, 42, 43, 44];
    let edges = vec![(0, 1), (1, 3), (0, 2), (2, 3)];

    let ok = topological_sort(&mut vertices, &edges);

    assert!(ok);
    assert!(ordered(&vertices, 41, 42));
    assert!(ordered(&vertices, 42, 44));
    assert!(ordered(&vertices, 41, 43));
    assert!(ordered(&vertices, 43, 44));
  }
}