use std::io::Result;
use std::mem::size_of;

use bytes::{Bytes, BytesMut, Buf, BufMut};
use tempfile::NamedTempFile;

use attosql_server::{
  accessor::FileAccessor,
  btree::BTreeRoot,
  database::Database,
  schema::{Query, Table, Type, Value},
};
use attosql_commons::testutil::FakeGen;

const SEED: u64 = 1;
const VALUE_SIZE: usize = 15;

fn to_u64_le(value: u64) -> Bytes {
  let mut bytes = BytesMut::with_capacity(size_of::<u64>());
  bytes.put_u64_le(value);
  bytes.freeze()
}

fn prepare_db_sequential<const N: usize>() -> Result<Database<FileAccessor>> {
  let (_, path) = NamedTempFile::new()?.into_parts();
  let vendor = FileAccessor::new(path)?;
  let root = BTreeRoot::new(vendor)?;
  let mut db = Database::new(root)?;
  let types = vec![Type::U64, Type::U64];
  let table = Table::new(types);
  let table_id = db.create_table(&table)?;

  let data: Vec<_> = (0..N).map(|i| (to_u64_le(i as u64), to_u64_le(i as u64 + 10))).collect();
  let mut iter = data.into_iter();
  db.batch_insert(table_id, &mut iter)?;

  Ok(db)
}

fn main() {
  let db = prepare_db_sequential::<100000>().unwrap();
  let table_id = db.fetch_table_ids().unwrap()[0];

  for _ in 0..100000 {
    let mut fake = FakeGen::new(SEED);
    let pos = fake.choose_u64(100000);
    let key = Value::U64(pos).into();
    let value: Bytes = Value::U64(pos + 10).into();
    let query = Query::FetchByPK { table_id, key };

    let result: Vec<_> = db.plan(query, None).unwrap().iter.collect();

    assert_eq!(1, result.len());
    assert_eq!(2, result[0].len());
    assert_eq!(value, result[0][1]);
  }
}