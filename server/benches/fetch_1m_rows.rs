extern crate criterion;
use criterion::{criterion_group, criterion_main, Criterion};

use std::io::Result;
use std::mem::size_of;

use bytes::{Bytes, BytesMut, Buf, BufMut};
use tempfile::NamedTempFile;

use attosql_server::{
  accessor::FileAccessor,
  database::Database,
  table::{Query, Table, Type, Value},
};
use attosql_commons::testutil::FakeGen;

const SEED: u64 = 1;
const VALUE_SIZE: usize = 15;

fn to_u64_le(value: u64) -> Bytes {
  let mut bytes = BytesMut::with_capacity(size_of::<u64>());
  bytes.put_u64_le(value);
  bytes.freeze()
}

fn prepare_db() -> Result<(Database<FileAccessor>, Vec<(Bytes, Bytes)>, FakeGen)> {
  let count = 10000;
  let round = 100;

  let (_, path) = NamedTempFile::new()?.into_parts();
  let vendor = FileAccessor::new(path)?;
  let mut db = Database::new(vendor)?;
  let types = vec![Type::U32, Type::Char(VALUE_SIZE)];
  let table = Table::new(types);
  let table_id = db.create_table(&table)?;
  let mut dataset = Vec::new();
  let mut fake = FakeGen::new(SEED);

  for _ in 0..round {
    let mut params: Vec<_> = (0..count).map(|_| {
      let key = u32::to_le_bytes(fake.next_u32());
      let key = Bytes::copy_from_slice(&key);
      let value = fake.next_text::<16>();
      let value = Bytes::copy_from_slice(&value);
      (key, value)
    }).collect();
    params.sort_by(|a, b| a.0.cmp(&b.0));
    for (k, v) in &params {
      dataset.push((k.clone(), v.clone()))
    }

    let mut iter = params.into_iter();
    db.batch_insert(table_id, &mut iter)?;
  }
  db.vendor().borrow_mut().sync()?;

  Ok((db, dataset, fake))
}

fn prepare_db_sequential<const N: usize>() -> Result<Database<FileAccessor>> {
  let (_, path) = NamedTempFile::new()?.into_parts();
  let vendor = FileAccessor::new(path)?;
  let mut db = Database::new(vendor)?;
  let types = vec![Type::U64, Type::U64];
  let table = Table::new(types);
  let table_id = db.create_table(&table)?;

  let data: Vec<_> = (0..N).map(|i| (to_u64_le(i as u64), to_u64_le(i as u64 + 10))).collect();
  let mut iter = data.into_iter();
  db.batch_insert(table_id, &mut iter)?;

  Ok(db)
}

fn fetch_1(c: &mut Criterion) {
  let (db, dataset, mut fake) = prepare_db().unwrap();
  let table_id = db.fetch_table_ids().unwrap()[0];

  c.bench_function("fetch 1", |b| b.iter(|| {
    let pos = fake.choose_usize(dataset.len());
    let key = Value::U32(dataset[pos].0.clone().get_u32_le());
    let value = dataset[pos].1.clone();
    let query = Query::FetchByPK{ table_id, key };
    let result: Vec<Vec<Bytes>> = db.plan(query).unwrap().iter.collect();

    assert_eq!(1, result.len());
    assert_eq!(2, result[0].len());
    assert_eq!(value, result[0][1]);
  }));
}

fn fetch_2(c: &mut Criterion) {
  let db = prepare_db_sequential::<100000>().unwrap();
  let table_id = db.fetch_table_ids().unwrap()[0];

  c.bench_function("fetch 1", |b| b.iter(|| {
    let mut fake = FakeGen::new(SEED);
    let pos = fake.choose_u64(100000);
    let key = Value::U64(pos).into();
    let value: Bytes = Value::U64(pos + 10).into();
    let query = Query::FetchByPK { table_id, key };
    let result: Vec<Vec<Bytes>> = db.plan(query).unwrap().iter.collect();

    // assert_eq!(1, result.len());
    // assert_eq!(2, result[0].len());
    // assert_eq!(value, result[0][1]);
  }));
}

criterion_group!(benches, fetch_2);
criterion_main!(benches);