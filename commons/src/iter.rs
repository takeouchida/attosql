use std::borrow::Borrow;
use std::collections::HashMap;
use std::cmp::Ordering;
use std::iter::FromIterator;
use std::hash::Hash;
use std::ops::Index;
use std::mem::swap;

pub struct GroupBy<'i, T, U> where T: PartialEq {
  key: Option<T>,
  values: Vec<U>,
  iter: Box<dyn Iterator<Item=(T, U)> + 'i>,
}

impl<'i, T, U> GroupBy<'i, T, U> where T: PartialEq, {
  pub fn new(iter: Box<dyn Iterator<Item=(T, U)> + 'i>) -> Self {
    GroupBy { key: None, values: vec![], iter }
  }
}

impl<'i, T, U> Iterator for GroupBy<'i, T, U> where T: PartialEq + Clone, U: Clone {
  type Item = (T, Vec<U>);

  #[inline]
  fn next(&mut self) -> Option<Self::Item> {
    loop {
      debug_assert!((self.values.is_empty() && self.key.is_none()) || (!self.values.is_empty() && self.key.is_some()));
      match self.iter.next() {
        None =>
          return if self.values.is_empty() {
            None
          } else {
            let mut key = None;
            let mut values = vec![];
            swap(&mut key, &mut self.key);
            swap(&mut values, &mut self.values);
            Some((key.unwrap(), values))
          },
        Some((k, v)) => {
          let is_some = self.key.is_some();
          if is_some {
            let matched = self.key.as_ref().filter(|&l| k == *l).is_some();
            if matched {
              self.values.push(v.clone());
            } else {
              let mut key = Some(k.clone());
              let mut values = vec![v.clone()];
              swap(&mut key, &mut self.key);
              swap(&mut values, &mut self.values);
              return Some((key.unwrap(), values))
            }
          } else {
            self.key = Some(k.clone());
            self.values.push(v.clone());
          }
        }
      }
    }
  }
}

pub struct MergeInnerJoin<'i, 'j, T, U, V> where T: Ord {
  left_iter: Box<dyn Iterator<Item=(T, U)> + 'i>,
  right_iter: Box<dyn Iterator<Item=(T, V)> + 'j>,
  left: Option<(T, U)>,
  right: Option<(T, V)>,
}

impl<'i, 'j, T, U, V> MergeInnerJoin<'i, 'j, T, U, V> where T: Ord {
  pub fn new(
    left_iter: Box<dyn Iterator<Item=(T, U)> + 'i>,
    right_iter: Box<dyn Iterator<Item=(T, V)> + 'j>,
  ) -> MergeInnerJoin<'i, 'j, T, U, V> {
    MergeInnerJoin { left_iter, right_iter, left: None, right: None }
  }
}

impl<'i, 'j, T, U, V> Iterator for MergeInnerJoin<'i, 'j, T, U, V> where T: Ord, {
  type Item = (T, U, V);

  fn next(&mut self) -> Option<Self::Item> {
    loop {
      if self.left.is_none() {
        self.left = self.left_iter.next();
      }
      if self.right.is_none() {
        self.right = self.right_iter.next();
      }
      match (&self.left, &self.right) {
        (Some((k1, _)), Some((k2, _))) => match k1.cmp(&k2) {
          Ordering::Less    => self.left = None,
          Ordering::Equal   => {
            let mut l = None;
            let mut r = None;
            swap(&mut self.left, &mut l);
            swap(&mut self.right, &mut r);
            let (k1, v1) = l.unwrap();
            let (_ , v2) = r.unwrap();
            return Some((k1, v1, v2));
          },
          Ordering::Greater => self.right = None,
        },
        _ => return None,
      }
    }
  }
}

pub struct Cross<'i, T, U, V> {
  iter: Box<dyn Iterator<Item=(T, Vec<U>, Vec<V>)> + 'i>,
  item: Option<(T, Vec<U>, Vec<V>)>,
  left: usize,
  right: usize,
}

impl<'i, T, U, V> Cross<'i, T, U, V> {
  pub fn new(iter: Box<dyn Iterator<Item=(T, Vec<U>, Vec<V>)> + 'i>) -> Cross<'i, T, U, V> {
    Cross { iter, item: None, left: 0, right: 0 }
  }
}

impl<'i, T, U, V> Iterator for Cross<'i, T, U, V> where T: Clone, U: Clone, V: Clone {
  type Item = (T, U, V);

  fn next(&mut self) -> Option<Self::Item> {
    loop {
      match &self.item {
        None => {
          self.item = self.iter.next();
          if self.item.is_none() {
            return None
          }
        },
        Some((p, l, r)) => {
          if l.len() <= self.left {
            self.item = self.iter.next();
            self.left = 0;
            self.right = 0;
          } else if r.len() <= self.right {
            self.left += 1;
            self.right = 0;
          } else {
            let l = l[self.left].clone();
            let r = r[self.right].clone();
            self.right += 1;
            return Some((p.clone(), l, r))
          }
        },
      }
    }
  }
}

pub struct MultiHashMap<K, V> {
  pub map: HashMap<K, Vec<V>>
}

impl<K, V> MultiHashMap<K, V> {
  pub fn contains_key<Q: ?Sized>(&self, k: &Q) -> bool where K: Eq + Hash + Borrow<Q>, Q: Eq + Hash, {
    self.map.contains_key(k)
  }
}

impl<K, Q, V> Index<&Q> for MultiHashMap<K, V>
  where
    K: Eq + Hash + Borrow<Q>,
    Q: Eq + Hash + ?Sized,
{
  type Output = Vec<V>;

  #[inline]
  fn index(&self, key: &Q) -> &Vec<V> {
    &self.map[key]
  }
}

impl<K, V> FromIterator<(K, V)> for MultiHashMap<K, V> where K: Eq + Hash {
  fn from_iter<I>(iter: I) -> Self where I: IntoIterator<Item=(K, V)> {
    let mut map = HashMap::<K, Vec<V>>::new();
    for (k, v) in iter {
      if map.contains_key(&k) {
        let mut vs = map.remove(&k).unwrap();
        vs.push(v);
        map.insert(k, vs);
      } else {
        map.insert(k, vec![v]);
      }
    }
    MultiHashMap { map }
  }
}

impl<K, V> IntoIterator for MultiHashMap<K, V> {
  type Item = (K, Vec<V>);
  type IntoIter = <HashMap<K, Vec<V>> as IntoIterator>::IntoIter;
  #[inline]
  fn into_iter(self) -> Self::IntoIter {
    self.map.into_iter()
  }
}

pub struct HashInnerJoin<'i, 'j, T, U, V> where T: Ord + Eq + Hash {
  left_iter: Box<dyn Iterator<Item=(T, U)> + 'i>,
  right_iter: Box<dyn Iterator<Item=(T, V)> + 'j>,
  left: usize,
  right: Option<(T, V)>,
  map: Option<MultiHashMap<T, U>>,
}

impl<'i, 'j, T, U, V> HashInnerJoin<'i, 'j, T, U, V> where T: Ord + Eq + Hash {
  pub fn new(
    left_iter: Box<dyn Iterator<Item=(T, U)> + 'i>,
    right_iter: Box<dyn Iterator<Item=(T, V)> + 'j>,
  ) -> HashInnerJoin<'i, 'j, T, U, V> {
    HashInnerJoin { left_iter, right_iter, left: 0, right: None, map: None }
  }
}

impl<'i, 'j, T, U, V> Iterator for HashInnerJoin<'i, 'j, T, U, V> where T: Ord + Eq + Hash + Clone + 'i, U: Clone + 'i, V: Clone {
  type Item = (T, U, V);

  fn next(&mut self) -> Option<Self::Item> {
    if self.map.is_none() {
      let mut temp: Box<dyn Iterator<Item=(T, U)>> = Box::new(std::iter::empty());
      swap(&mut self.left_iter, &mut temp);
      self.map = Some(temp.into_iter().collect());
    }
    loop {
      if self.right.is_none() {
        self.right = self.right_iter.next();
      }
      let (k, v) = self.right.as_ref()?;
      let map = self.map.as_ref().unwrap();
      if map.contains_key(&k) {
        let us = &map[&k];
        if self.left < us.len() {
          let pos = self.left;
          self.left += 1;
          return Some((k.clone(), (us[pos]).clone(), v.clone()));
        } else {
          self.left = 0;
          self.right = None;
        }
      } else {
        self.left = 0;
        self.right = None;
      }
    }
  }
}

#[cfg(test)]
mod tests {
  use std::mem::size_of;
  use bytes::{Bytes, BytesMut, BufMut};
  use super::*;

  fn to_u64_le(value: u64) -> Bytes {
    let mut bytes = BytesMut::with_capacity(size_of::<u64>());
    bytes.put_u64_le(value);
    bytes.freeze()
  }

  #[test]
  fn group_by_empty() {
    let source = Vec::<(i32, u64)>::new();
    let iter = source.into_iter();
    let result = GroupBy::new(Box::new(iter));

    assert_eq!(0, result.count());
  }

  #[test]
  fn group_by_1_elem() {
    let source: Vec<(i32, u64)> = vec![(1, 100)];
    let iter = source.into_iter();
    let mut result = GroupBy::new(Box::new(iter));

    assert_eq!(Some((1, vec![100])), result.next());
    assert_eq!(None, result.next());
  }

  #[test]
  fn group_by_2_ident_elems() {
    let source: Vec<(i32, u64)> = vec![(1, 100), (1, 100)];
    let iter = source.into_iter();
    let mut result = GroupBy::new(Box::new(iter));

    assert_eq!(Some((1, vec![100; 2])), result.next());
    assert_eq!(None, result.next());
  }

  #[test]
  fn group_by_2_diff_elems() {
    let source: Vec<(i32, u64)> = vec![(1, 100), (2, 200)];
    let iter = source.into_iter();
    let mut result = GroupBy::new(Box::new(iter));

    assert_eq!(Some((1, vec![100])), result.next());
    assert_eq!(Some((2, vec![200])), result.next());
    assert_eq!(None, result.next());
  }

  #[test]
  fn group_by_2_1_elems() {
    let source: Vec<(i32, u64)> = vec![(1, 100), (1, 101), (2, 200)];
    let iter = source.into_iter();
    let mut result = GroupBy::new(Box::new(iter));

    assert_eq!(Some((1, vec![100, 101])), result.next());
    assert_eq!(Some((2, vec![200])), result.next());
    assert_eq!(None, result.next());
  }

  #[test]
  fn group_by_1_2_elems() {
    let source: Vec<(i32, u64)> = vec![(1, 100), (2, 200), (2, 201)];
    let iter = source.into_iter();
    let mut result = GroupBy::new(Box::new(iter));

    assert_eq!(Some((1, vec![100])), result.next());
    assert_eq!(Some((2, vec![200, 201])), result.next());
    assert_eq!(None, result.next());
  }

  #[test]
  fn group_by_3_diff_elems() {
    let source: Vec<(i32, u64)> = vec![(1, 100), (2, 200), (3, 300)];
    let iter = source.into_iter();
    let mut result = GroupBy::new(Box::new(iter));

    assert_eq!(Some((1, vec![100])), result.next());
    assert_eq!(Some((2, vec![200])), result.next());
    assert_eq!(Some((3, vec![300])), result.next());
    assert_eq!(None, result.next());
  }

  #[test]
  fn group_by_4_diff_tuples() {
    let source: Vec<(i32, u64)> = vec![(1, 100), (1, 101), (2, 200), (2, 201)];
    let iter = source.into_iter();
    let mut result = GroupBy::new(Box::new(iter));

    assert_eq!(Some((1, vec![100, 101])), result.next());
    assert_eq!(Some((2, vec![200, 201])), result.next());
    assert_eq!(None, result.next());
  }

  #[test]
  fn merge_join_empty() {
    let left = Vec::<(Bytes, Bytes)>::new();
    let right = Vec::<(Bytes, Bytes)>::new();
    let left_iter = left.into_iter();
    let right_iter = right.into_iter();
    let left_iter = GroupBy::new(Box::new(left_iter));
    let right_iter = GroupBy::new(Box::new(right_iter));
    let iter = MergeInnerJoin::new(Box::new(left_iter), Box::new(right_iter));

    assert_eq!(0, iter.count());
  }

  #[test]
  fn merge_join_1_elem() {
    let k1 = to_u64_le(41);
    let k2 = to_u64_le(42);
    let v1 = to_u64_le(1);
    let v2 = to_u64_le(2);
    let left: Vec<(Bytes, Bytes)> = vec![(k1.clone(), v1.clone()), (k2.clone(), v2.clone())];
    let right: Vec<(Bytes, Bytes)> = vec![(k1.clone(), v2.clone()), (k2.clone(), v1.clone())];
    let left_iter = left.into_iter();
    let right_iter = right.into_iter();
    let left_iter = GroupBy::new(Box::new(left_iter));
    let right_iter = GroupBy::new(Box::new(right_iter));
    let mut iter = MergeInnerJoin::new(Box::new(left_iter), Box::new(right_iter));

    assert_eq!(Some((k1, vec![v1.clone()], vec![v2.clone()])), iter.next());
    assert_eq!(Some((k2, vec![v2.clone()], vec![v1.clone()])), iter.next());
    assert_eq!(None, iter.next());
  }

  #[test]
  fn merge_join_cross_1_elem() {
    let k1 = to_u64_le(41);
    let k2 = to_u64_le(42);
    let v1 = to_u64_le(1);
    let v2 = to_u64_le(2);
    let left: Vec<(Bytes, Bytes)> = vec![(k1.clone(), v1.clone()), (k2.clone(), v2.clone())];
    let right: Vec<(Bytes, Bytes)> = vec![(k1.clone(), v2.clone()), (k2.clone(), v1.clone())];
    let left_iter = left.into_iter();
    let right_iter = right.into_iter();
    let left_iter = GroupBy::new(Box::new(left_iter));
    let right_iter = GroupBy::new(Box::new(right_iter));
    let iter = MergeInnerJoin::new(Box::new(left_iter), Box::new(right_iter));
    let mut cross_iter = Cross::new(Box::new(iter));

    assert_eq!(Some((k1, v1.clone(), v2.clone())), cross_iter.next());
    assert_eq!(Some((k2, v2.clone(), v1.clone())), cross_iter.next());
    assert_eq!(None, cross_iter.next());
  }

  #[test]
  fn group_by_merge_join() {
    let v1: Vec<(i32, u64)> = vec![(1, 100), (3, 300), (5, 500), (6, 600)];
    let v2: Vec<(i32, u64)> = vec![(2, 200), (3, 301), (4, 400), (6, 601)];
    let left = v1.into_iter();
    let right = v2.into_iter();
    let left = GroupBy::new(Box::new(left));
    let right = GroupBy::new(Box::new(right));
    let iter = MergeInnerJoin::new(Box::new(left), Box::new(right));
    let mut iter = Cross::new(Box::new(iter));

    assert_eq!(Some((3, 300, 301)), iter.next());
    assert_eq!(Some((6, 600, 601)), iter.next());
    assert_eq!(None, iter.next());
  }
}