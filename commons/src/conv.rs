use std::mem::{size_of, transmute};
use std::slice;

#[inline]
pub unsafe fn deref_helper<'a, T>(v: &[u8]) -> &'a T {
  &*(v.as_ptr() as *const T)
}

#[inline]
pub unsafe fn deref_mut_helper<'a, T>(v: &mut [u8]) -> &'a mut T {
  &mut *(v.as_ptr() as *mut T)
}

#[inline]
pub unsafe fn deref_slice_helper<'a, T>(v: &[u8]) -> &'a [T] {
  let len = v.len();
  let size = size_of::<T>();
  debug_assert_ne!(size, 0);
  debug_assert_eq!(len % size, 0);
  let elems = len / size;
  slice::from_raw_parts(v.as_ptr() as *const T, elems)
}

#[inline]
pub unsafe fn deref_slice_mut_helper<'a, T>(v: &mut [u8]) -> &'a mut [T] {
  let len = v.len();
  let size = size_of::<T>();
  debug_assert_ne!(size, 0);
  debug_assert_eq!(len % size, 0);
  let elems = len / size;
  slice::from_raw_parts_mut(v.as_ptr() as *mut T, elems)
}

#[inline]
pub unsafe fn cast_to_byte_slice<T>(v: &T) -> &[u8] {
  let data = transmute::<&T, *const u8>(v);
  let len = size_of::<T>();
  slice::from_raw_parts(data, len)
}

#[inline]
pub unsafe fn cast_to_byte_slice_mut<T>(v: &mut T) -> &mut [u8] {
  let data = transmute::<&mut T, *mut u8>(v);
  let len = size_of::<T>();
  slice::from_raw_parts_mut(data, len)
}