use rand::prelude::*;
use rand_pcg::Pcg64;

pub struct FakeGen {
  rng: Pcg64,
}

impl FakeGen {
  const CHARS: &'static [u8] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-".as_bytes();

  pub fn new(seed: u64) -> FakeGen {
    let rng = Pcg64::seed_from_u64(seed);
    FakeGen { rng }
  }

  pub fn next_u32(&mut self) -> u32 {
    self.rng.next_u32()
  }

  pub fn next_text<const N: usize>(&mut self) -> [u8; N] {
    debug_assert!(N < 256);
    let mut bytes = [0u8; N];
    bytes[0] = N as u8;
    self.rng.fill_bytes(&mut bytes);
    (1..N).for_each(|i| bytes[i] = Self::CHARS[bytes[i] as usize & 0x3fusize]);
    bytes
  }

  pub fn choose_usize(&mut self, size: usize) -> usize {
    let e = self.rng.next_u32() as f64 / (u32::max_value() as f64 + 1.0);
    (size as f64 * e) as usize
  }

  pub fn choose_u64(&mut self, size: usize) -> u64 {
    let e = self.rng.next_u32() as f64 / (u32::max_value() as f64 + 1.0);
    (size as f64 * e) as u64
  }
}