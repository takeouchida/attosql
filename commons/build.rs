use std::path::Path;

fn main() -> Result<(), std::io::Error> {
  let proto: &dyn AsRef<Path> = &"proto/attosql.proto";
  let proto_path: &Path = proto.as_ref();
  let proto_dir = proto_path.parent().expect("proto file should reside in a directory");
  tonic_build::configure().protoc_arg("--experimental_allow_proto3_optional").compile(&[proto_path], &[proto_dir])
}