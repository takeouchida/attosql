#[macro_use] extern crate lalrpop_util;
extern crate attosql_commons;

lalrpop_mod!(pub sql);

pub mod ast;

use rustyline::Editor;
use rustyline::error::ReadlineError;
use lalrpop_util::ParseError;

use attosql_commons::attosql::database_client::DatabaseClient;
use attosql_commons::attosql::{CreateTableRequest, InsertRequest, RunRequest, BeginRequest, CommitRequest, RollbackRequest};
use attosql_commons::attosql::{FullScan, Query, query};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
  let mut client = DatabaseClient::connect("http://[::1]:5000").await?;
  let mut rl = Editor::<()>::new();
  if rl.load_history("./.attosql").is_err() {
    println!("No history file was found. will create a new one.")
  }
  let mut trans_id: Option<u64> = None;

  loop {
    let line = rl.readline("> ");
    match line {
      Ok(line) => {
        rl.add_history_entry(line.as_str());
        if line == "quit" || line == "q" || line == "Q" {
          break
        }
        let q: Result<ast::Query, ParseError<_, _, _>> = sql::queryParser::new().parse(&line);
        match q {
          Ok(ast::Query::CreateTable(ast::CreateTable{columns})) => {
            let columns = columns.into_iter().map(|c| c.into()).collect();
            let req = tonic::Request::new(CreateTableRequest{columns});
            let res = client.create_table(req).await?;
            println!("recv {:?}", res);
          },
          Ok(ast::Query::Insert(ast::Insert{table_id, columns, values})) => {
            let values = values.into_iter().map(|v| v.into()).collect();
            let req = tonic::Request::new(InsertRequest{table_id: table_id as u32, values, trans_id});
            let res = client.insert(req).await?;
            println!("recv {:?}", res);
          },
          Ok(ast::Query::Select(ast::Select{columns, table, joins, where_, group_by, pos})) => {
            let query = Some(Query{query: Some(query::Query::FullScan(FullScan{table_id: table as u32}))});
            let req = tonic::Request::new(RunRequest { query: query });
            let res = client.run(req).await?;
            println!("recv {:?}", res);
          },
          Ok(ast::Query::Update) => {
            unimplemented!()
          },
          Ok(ast::Query::Delete) => {
            unimplemented!()
          },
          Ok(ast::Query::Begin) => {
            match trans_id {
              Some(_) => { println!("A transaction has already been begun."); },
              None => {
                let req = tonic::Request::new(BeginRequest{});
                let res = client.begin(req).await?;
                println!("recv {:?}", res);
                trans_id = Some(res.into_inner().trans_id);
              }
            }
          },
          Ok(ast::Query::Commit) => {
            match trans_id {
              None => { println!("A transaction has not been begun yet."); }
              Some(tid) => {
                let req = tonic::Request::new(CommitRequest{ trans_id: tid });
                client.commit(req).await?;
                trans_id = None;
              }
            }
          },
          Ok(ast::Query::Rollback) => {
            match trans_id {
              None => { println!("A transaction has not been begun yet."); }
              Some(tid) => {
                let req = tonic::Request::new(RollbackRequest{ trans_id: tid });
                client.rollback(req).await?;
                trans_id = None;
              }
            }
          }
          Err(err) => println!("err: {:?}", err),
        }
      },
      Err(ReadlineError::Interrupted) => {
        println!("CTRL-C");
        break
      },
      Err(ReadlineError::Eof) => {
        println!("CTRL-D");
        break
      },
      Err(err) => {
        println!("Error: {:?}", err);
        break
      }
    }
  }
  rl.save_history("./.attosql")?;
  Ok(())
}