use attosql_commons::attosql::{ColumnSpec, InValue, Type, in_value};

pub type Pos = (usize, usize);

#[derive(Debug)]
pub enum Lit<'a> {
  Int(i64),
  Str(&'a str),
  Null,
}

#[derive(Debug)]
pub struct Ident<'a>(pub &'a str);

#[derive(Debug)]
pub enum Term<'a> {
  Lit(Lit<'a>, Pos),
  Ident(Ident<'a>),
}

#[derive(Debug)]
pub enum BinOp {
  Eq,
  And,
}

#[derive(Debug)]
pub enum AExp<'a> {
  Lit(Lit<'a>, Pos),
  Bin(BinOp, Box<AExp<'a>>, Box<AExp<'a>>, Pos),
  In(bool, Box<AExp<'a>>, Vec<Box<AExp<'a>>>, Pos),
}

#[derive(Debug)]
pub struct Column(pub Option<i64>, pub i64);

#[derive(Debug)]
pub struct Table(pub u16);

#[derive(Debug)]
pub struct JoinConst<'a>(pub Vec<Box<AExp<'a>>>);

#[derive(Debug)]
pub struct Join<'a>(pub Table, pub JoinConst<'a>);

#[derive(Debug)]
pub struct Where<'a>(pub Box<AExp<'a>>, pub Pos);

#[derive(Debug)]
pub struct Having<'a>(pub Box<AExp<'a>>, pub Pos);

#[derive(Debug)]
pub struct GroupBy<'a>(pub Column, pub Option<Having<'a>>, pub Pos);

#[derive(Debug)]
pub struct Select<'a> {
  pub columns: Vec<Column>,
  pub table: i64,
  pub joins: Vec<Join<'a>>,
  pub where_: Option<Where<'a>>,
  pub group_by: Option<GroupBy<'a>>,
  pub pos: Pos,
}

#[derive(Debug)]
pub struct Update;

#[derive(Debug)]
pub struct Delete;

#[derive(Debug)]
pub enum Typ {
  U8,
  U16,
  I32,
  U32,
  U64,
  // Date,
  // DateTime,
  String(usize),
}

#[derive(Debug)]
pub struct ColumnDef(pub Typ);

#[derive(Debug)]
pub struct CreateTable {
  pub columns: Vec<ColumnDef>,
}

#[derive(Debug)]
pub struct Insert<'a> {
  pub table_id: u64,
  pub columns: Vec<u64>,
  pub values: Vec<AExp<'a>>
}

#[derive(Debug)]
pub enum Query<'a> {
  Select(Select<'a>),
  Insert(Insert<'a>),
  Update,
  Delete,
  CreateTable(CreateTable),
  Begin,
  Commit,
  Rollback,
}

impl From<ColumnDef> for ColumnSpec {
  fn from(value: ColumnDef) -> ColumnSpec {
    let (ty, len): (Type, u32) = match value.0 {
      Typ::U8 => (Type::U8, 0),
      Typ::U16 => (Type::U16, 0),
      Typ::I32 => (Type::I32, 0),
      Typ::U32 => (Type::U32, 0),
      Typ::U64 => (Type::U64, 0),
      Typ::String(n) => (Type::Char, n as u32),
    };
    ColumnSpec { r#type: ty as i32, len }
  }
}

impl<'a> From<AExp<'a>> for InValue {
  fn from(value: AExp<'a>) -> InValue {
    let in_value = match value {
      AExp::Lit(Lit::Int(n), _) => in_value::InValue::Number(n),
      AExp::Lit(Lit::Str(s), _) => in_value::InValue::Str(s.to_owned()),
      _ => unimplemented!(),
    };
    InValue { in_value: Some(in_value) }
  }
}